RUN=$1

LIST_OF_SAMPLES="/mnt/disk4/labs/salipante/nithisha/MH_file_lists/$RUN.txt"
RUN_DIR=/mnt/labs/swisher/Swisher_MH_data/$RUN

chmod -R 777 $RUN_DIR

while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')

                #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
			mkdir $RUN_DIR/$TUMOR_PFX

			mv $RUN_DIR/${TUMOR_PFX}_R1.fastq.gz $RUN_DIR/$TUMOR_PFX
			mv $RUN_DIR/${TUMOR_PFX}_R2.fastq.gz $RUN_DIR/$TUMOR_PFX

			if [ ! -d $RUN_DIR/$GERMLINE_PFX ];then
				mkdir $RUN_DIR/$GERMLINE_PFX

				mv $RUN_DIR/${GERMLINE_PFX}_R1.fastq.gz $RUN_DIR/$GERMLINE_PFX
				mv $RUN_DIR/${GERMLINE_PFX}_R2.fastq.gz $RUN_DIR/$GERMLINE_PFX
			fi
                #####################SINGLE SAMPLE##########################
                else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
			
			mkdir $RUN_DIR/$SAMPLE_PFX

			mv $RUN_DIR/${SAMPLE_PFX}_R1.fastq.gz $RUN_DIR/$SAMPLE_PFX
                        mv $RUN_DIR/${SAMPLE_PFX}_R2.fastq.gz $RUN_DIR/$SAMPLE_PFX
		fi
        

	done< $LIST_OF_SAMPLES                                                                               
