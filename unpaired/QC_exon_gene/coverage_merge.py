#The purpose of this script is to pretty much just rearrange cols. The name of file was kept as merge.py to be consistant with paired CP pipeline (in which T and G results are merged)
import sys
import pandas as pd
import numpy as np
import os #just for dir check

#Read in args
SAMPLE_EXON_QC_FILE = sys.argv[1]

SAMPLE_GENE_QC_FILE = sys.argv[2]

SAMPLE_PFX = sys.argv[3]
OUTPUT_DIR_PATH = sys.argv[4] #CP/results/TUMOR_PFX/results/

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[SAMPLE_EXON_QC_FILE,SAMPLE_GENE_QC_FILE]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)

#Dir check
        if os.path.exists(OUTPUT_DIR_PATH) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#------------------------------------------------------------------------------------------------------------------------------------------------------------
#Main code
def qc_exon (SAMPLE_EXON_QC_FILE):
	#For QC per exon
	sample_exon_qc = pd.read_csv(SAMPLE_EXON_QC_FILE, sep = "\t", header = 0,index_col= None, dtype = "str")

	#Let us clean up a little, make the avg_read_deptth an interger and combine the chromosome, start and stop positions
	sample_exon_qc["Position"] = "chr" + sample_exon_qc["Chromosome"] + ":" + sample_exon_qc["Start"] + "-" + sample_exon_qc["Stop"]
	sample_exon_qc["Segment_Position"] = "chr" + sample_exon_qc["Chromosome"] + ":" + sample_exon_qc["Segment_Start"] + "-" + sample_exon_qc["Segment_End"]
	sample_exon_qc["Avg_Read_Depth"] = sample_exon_qc["Avg_Read_Depth"].astype(float).round().astype(int).astype(str)

	sample_exon_qc_rearranged = sample_exon_qc[["Position","Segment_Position","Gene","Exon_Number","Avg_Read_Depth"]]
	
	#Write to file
	output_file_name = OUTPUT_DIR_PATH + SAMPLE_PFX + "_UN_" + "exon_qc" + ".txt"
	sample_exon_qc_rearranged.to_csv(output_file_name, sep = "\t", index=None)


def qc_gene (SAMPLE_GENE_QC_FILE):
	#For QC per gene
	sample_gene_qc = pd.read_csv(SAMPLE_GENE_QC_FILE, sep = "\t", header= 0, index_col= None, dtype = "str")
	
	#Let us clean up a little, make the avg_read_deptth an interger and combine the chromosome, start and stop positions
	sample_gene_qc["Position"] = "chr" + sample_gene_qc["Chromosome"] + ":" + sample_gene_qc["Start"] + "-" + sample_gene_qc["Stop"]
	sample_gene_qc["Segment_Position"] = "chr" + sample_gene_qc["Chromosome"] + ":" + sample_gene_qc["Segment_Start"] + "-" + sample_gene_qc["Segment_End"]
	sample_gene_qc["Avg_Read_Depth"] = sample_gene_qc["Avg_Read_Depth"].astype(float).round().astype(int).astype(str)

	sample_gene_qc_rearranged = sample_gene_qc[["Position","Segment_Position","Gene","Avg_Read_Depth"]]

	#Write to file
        output_file_name = OUTPUT_DIR_PATH + SAMPLE_PFX + "_UN_" + "gene_qc" + ".txt"
        sample_gene_qc_rearranged.to_csv(output_file_name, sep = "\t", index=None)

#---------------------------------------------------------------------------------------------------------------
qc_exon(SAMPLE_EXON_QC_FILE)
qc_gene(SAMPLE_GENE_QC_FILE)
