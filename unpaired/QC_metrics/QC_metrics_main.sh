set -x -e
#The purpose of this script is to generate qc metrics from 2 documents per tumor/germline paired sample - savepath/picard/output_t_hs_metrics.txt and savepath/picard/marked_dup_metrics_tumor_pfx_tumor.txt
#the 2 files above are generated from CP box, specifically by the commands CollectHSMetrics and Duplication metrics respectively
#We extract line 8 of both files - this is the only line we need

#Read in args
SAVEPATH=$1
SAMPLE_PFX=$2
SAMPLE_CollectHsMetricsFile=$3
SAMPLE_DuplicationMetricsFile=$4
SAMPLE_InsertionMetricsFile=$5

QC_METRICS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_metrics/qc_metrics_postprocess.py"

QC_METRICS_DIR_PATH=$SAVEPATH/QC_metrics
mkdir -p $QC_METRICS_DIR_PATH

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 5 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$QC_METRICS_POSTPROCESS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$QC_METRICS_DIR_PATH"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

amend_files () {
	#CollectHsMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
	sed -n "7,9p" $SAMPLE_CollectHsMetricsFile > $QC_METRICS_DIR_PATH/${SAMPLE_PFX}_UN_hs_metrics.txt

	#DuplicationMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
	sed -n "7,9p" $SAMPLE_DuplicationMetricsFile > $QC_METRICS_DIR_PATH/dup_metrics_${SAMPLE_PFX}_UN.txt

	#InsertMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
        sed -n "7,8p" $SAMPLE_InsertionMetricsFile > $QC_METRICS_DIR_PATH/ins_metrics_${SAMPLE_PFX}_UN.txt
}


postprocess () {
	#Use a python script to combine these files and delete rows you do not need
	python $QC_METRICS_POSTPROCESS_SCRIPT $QC_METRICS_DIR_PATH $SAMPLE_PFX $QC_METRICS_DIR_PATH/${SAMPLE_PFX}_UN_hs_metrics.txt $QC_METRICS_DIR_PATH/dup_metrics_${SAMPLE_PFX}_UN.txt $QC_METRICS_DIR_PATH/ins_metrics_${SAMPLE_PFX}_UN.txt $SAVEPATH/results/${SAMPLE_PFX}_UN_qc_metrics.txt 

}

remove_files () {
	rm -r $QC_METRICS_DIR_PATH
}


#Main code
amend_files
postprocess
#remove_files
