#used in Pindel post process script
import re
import sys
import os

#REFSEQ  #this was obtained from UCSC's Genome Browser, Tools, Table Browser
#BROCA_GENES
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


################################
###########FUNCTIONS############
################################
broca_gene_transcripts = {}
def BROCA_gene_trancript_dict(BROCA_GENES):
		with open (BROCA_GENES, "r+") as broca:
			header = broca.readline()
			for bline in broca:
				broca_line = bline.strip("\n").split("\t")
				broca_gene = broca_line[0]
				broca_transcript = broca_line[4].split(".")[0]
				broca_gene_transcripts[broca_gene] = broca_transcript
genes = ""
transcripts = ""
def openRefSeqFile(REFSEQ,SV_START,SV_END,CHROM):
	global region1
	global region1State
	global region2
	global region2State
	
	with open (REFSEQ, "r+") as refseq:
                                header = refseq.readline() #skip header line
                                for rline in refseq:
					#Let us clean up each refseq line before processing it downstream
                                        rs_line = rline.split("\t")
					rs_line[9] = rs_line[9].strip("\"") #HAVE TO DO THIS FOR EXON STARTS AND STOPS AS THAT CELL GETS READ AS "5000234,5000678," with " also
					rs_line[10] = rs_line[10].strip("\"")
					rs_line[9] = rs_line[9].rstrip(",")
                                        rs_line[10] = rs_line[10].rstrip(",")
					rs_line[2] = rs_line[2].replace("chr","")

					#We need these variables to regenerate for EVER line of refseq file for EVERY line of pindel file.
					#Initialize global variables
					region1 = ""
					region1State = ""
					region2 = ""
					region2State = ""

					#Deploy functions here
					considerSVStart(rs_line,SV_START,SV_END,CHROM)
					considerSVEnd(rs_line,SV_START,SV_END,CHROM)
					cleanUpGenes(rs_line)


#INNER FUNC 1
def considerSVStart(rs_line,SV_START,SV_END,CHROM):
	#This step is to find region1 and region1State
        #if this event is within the range of a gene length, note it down, only once, even for different isoforms of the same gene.
        #if chr no matches and pindel start position is within the range of refseq's txStart and txStop which are transcription start and stop sites) 
	#dont int(chromosome) in case of "chrM", this cannot be  int ed.
	global region1
	global region1State
	global genes
	if rs_line[12] in broca_gene_transcripts.keys() and CHROM == rs_line[2] and int(SV_START) >= int(rs_line[4]) and int(SV_START) <= int(rs_line[5]): #rs_line is refseq file line [4] [5] are tx start and tx ends
		if rs_line[1].split(".")[0] == broca_gene_transcripts[rs_line[12]]: #if refseq line transcript without version matches the transcript which is the value of the dict key-refseq gene
			#if chromosome matches and pindel event is within preferred transcript
                	#Try to find which exon/intron this events occurs in
			exonStarts = rs_line[9].split(",")
                	exonEnds = rs_line[10].split(",")
                	#no of exons
                	noOfExons = len(exonStarts)
		
			#go through each exon and see if event occurs in exon/5' or 3' UTR/intron if event is contained in the range of the given exonStart to exonStop
                	counter = 0
                	while counter < noOfExons:

                		#Check if the event occurs in an exon - this means that event is contained in the range of the given exonStart to exonStop
                       		if int(exonStarts[counter]) <= int(SV_START) and int(exonEnds[counter]) >= int(SV_START):
                       			if rs_line[3] == '+':
                               			region1 = counter + 1 #WHY- This is because counter starts from 0! which is technically the first exon
                                       		region1State = "exon"
                               		if rs_line[3] == '-':
                               			region1 = noOfExons - counter
                                       		region1State = "exon"
                               		#Check for special case of 5'/3' UTR - this means that the event is still contained within the range of txStart and txStop but outside cdsStart and csdEnd (coding start and stop)
					if int(SV_START) < int(rs_line[6]) or int(SV_START) > int(rs_line[7]): #[6] and [7] are coding start and ends from refseq
                               			region1State = "UTR" #at this point, region1State is already "exon", it will be replaced by "UTR" if this is true in addition to satifying exon requirement
						region1= ""

	               		#Check if the event occurs in an intron - this means that the event start is greater than the exonEnd of the current exon but lesser than the next exonStart
                		if counter != (noOfExons - 1):
					if int(exonStarts[counter+1]) > int(SV_START) and int(exonEnds[counter]) <= int(SV_START):
                        			if rs_line[3] == "+":
                               				region1 = counter + 1
                                       			region1State = "intron"
                               			if rs_line[3] == "-":
                               				region1 = noOfExons - (counter + 1)
                                       			region1State = "intron"

                        	#increase counter
                        	counter = counter + 1

			#Update the genes this event is related to
                	if rs_line[12] not in genes:
				genes = genes + rs_line[12] + ","

			
#INNER FUNC2.2
def considerSVEnd(rs_line,SV_START,SV_END,CHROM):
	global region2
        global region2State
        global genes
	#if chr no matches and pindel end position is within the range of refseq's txStart and txStop which are transcription start and stop sites)
	if  rs_line[12] in broca_gene_transcripts.keys() and CHROM == rs_line[2] and int(SV_END) >= int(rs_line[4]) and int(SV_END) <= int(rs_line[5]):
		if rs_line[1].split(".")[0] == broca_gene_transcripts[rs_line[12]]:
       			#Check if the event occurs in an exon - this means that event is contained in the range of the given exonStart to exonStop
			exonStarts = rs_line[9].split(",")
               		exonEnds = rs_line[10].split(",")
               		#no of exons
               		noOfExons = len(exonStarts)

               		#go through each exon and see if event occurs in exon/5' or 3' UTR/intron if event is contained in the range of the given exonStart to exonStop
               		counter = 0
               		while counter < noOfExons:
               			#Check if the event occurs in an exon - this means that event is contained in the range of the given exonStart to exonStop
				if int(exonStarts[counter]) <= int(SV_END) and int(exonEnds[counter]) >= int(SV_END):
                       			if rs_line[3] == "+":
						region2 = counter + 1
                                       		region2State = "exon"

					if rs_line[3] == "-":
						region2 = noOfExons - counter
						region2State = "exon"

					#Check for special case of 5'/3' UTR - this means that the event is still contained within the range of txStart and txStop but outside cdsStart and csdEnd (coding start and stop)
					if int(SV_END) < int(rs_line[6]) or int(SV_END) > int(rs_line[7]):
						region2State = "UTR"
						region2= ""
					
				#Check if the event occurs in an intron - this means that the event start is greater than the exonEnd of the current exon but lesser than the next exonStart
                       		if counter != (noOfExons - 1):
					if int(exonStarts[counter + 1]) >= int(SV_END) and int(exonEnds[counter]) < int(SV_END):
                       				if rs_line[3] == "+":
                               				region2 = counter + 1
                                       			region2State = "intron"
						if rs_line[3] == "-":
							region2 = noOfExons - (counter + 1)
                                       			region2State = "intron"

				#increase counter
                      		counter = counter + 1


			#Update the genes this event is related to
               		if rs_line[12] not in genes:
				genes = genes + rs_line[12] + ","





#INNER FUNC2.3
def cleanUpGenes(rs_line):
	global transcripts
	#If gene is found(meaning region1State/region2State equals something intron/exon/UTR), note down the involved transcripts #Step 1 : Gene_name : NM...
	if region1State != "" and region2State != "":
		transcripts = transcripts + rs_line[12] + ":" + rs_line[1] #rs_line[1] is transcripts and rs_line[12] is gene names

		if region1 == region2 and region1State == region2State : #Step 2: Gene_name : NM...(Exon 1) OR Gene_name: NM... (Exon 2 - Exon 3)
			transcripts = transcripts + "(" + region1State + " " + str(region1) + "),"
		else:
        		transcripts = transcripts + "(" + region1State + " " + str(region1) + "-" + region2State + " " + str(region2) + "),"
	if region1State == "" and region2State != "":
		transcripts = transcripts +  rs_line[12] + ":" + rs_line[1] + "(" + region1State + " " + str(region1) + "-" + region2State + " " + str(region2) + "),"
	if region1State != "" and region2State == "":
		transcripts = transcripts + rs_line[12] + ":" + rs_line[1] + "(" + region1State + " " + str(region1) + "-" + region2State + " " + str(region2) + "),"





def printToOutput(genes,transcripts):
	#Tidy up gene and transcript lists by removing trailing commas
	genes = genes.rstrip (",")
        transcripts = transcripts.rstrip(",")

	#if no genes are found, report as intergenic
        if genes == "":
        	genes = "Intergenic"
        #Find out if this is exonic or intronic
        exonSummary = "Intergenic"
        if "intron" in transcripts:
        	exonSummary = "Intronic"
	if "UTR" in transcripts:
        	exonSummary = "UTR"
	if "exon" in transcripts:
        	exonSummary = "Exonic"
	if "-" in transcripts and "UTR" not in transcripts:
        	exonSummary = "Exonic" #if we transition from one state to another, then we are including an exon, unless we are going into an intron.

	#Format output. Format is Gene  Intronic/Exonic Type    Length  Position        No.of supporting reads  Entry Index     Transcripts
        output = genes + "\t" + exonSummary + "\t" + transcripts + "\n"
	return (output)







#Main code
def annotate_region(CHROM,SV_START,SV_END,BROCA_GENES,REFSEQ):
	global genes
	global transcripts
	genes = ""
	transcripts = ""
	BROCA_gene_trancript_dict(BROCA_GENES)
	openRefSeqFile(REFSEQ,SV_START,SV_END,CHROM) #-->calls a.considerPindelStart b.considerPindelEnd c.cleanUpGenes
	output = printToOutput(genes,transcripts)
	return output
