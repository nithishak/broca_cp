set -x -e
#For indel analysis, varscan to study germline mutations and Vardict to study single sample analysis.

#Read in args
SAVEPATH=$1 #this will end in CP/results/$TUMOR_PFX/....
SAMPLE_PFX=$2

#programs specific to this script only
#REF_GENOME_b37
#SAMTOOLS
#GATK
#VARSCAN
#BEDFILE_0B
#VARDICT
#VARDICT_SCRIPTS_DIR

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 2 ]; then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$VARSCAN|$BEDFILE_0B|$VARDICT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done


#Dir check
DIRS="$VARDICT_SCRIPTS_DIR"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done

unset IFS

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
varscan () {
        #1. VARSCAN
        #concluded that somatic and somatic Filter(somatic variant pipeline) gave inconclusive results, dont use, use old pipeline - mpileup2snp etc. (germline variant pipeline)
        #http://varscan.sourceforge.net/somatic-calling.html
        #http://varscan.sourceforge.net/using-varscan.html#v2.3_somatic
        #target sites contained in bed file and sent as -L arg to samtools mpileup command
        #[UN.pileup file created]

        mkdir -p $SAVEPATH/varscan
        $SAMTOOLS mpileup -f $REF_GENOME_b37 -d 100000 -A -B -l $BEDFILE_0B $SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN.bam > $SAVEPATH/varscan/${SAMPLE_PFX}_UN.pileup
        #SNPS
        #set strand filter to be 0 as if strand filter fails, instead of showing it under FITLER varscan is discarding variant altogether.
        java -jar $VARSCAN mpileup2snp $SAVEPATH/varscan/${SAMPLE_PFX}_UN.pileup --strand-filter 0 --min-var-freq 0.03 --min-reads2 5 --output-vcf 1 > $SAVEPATH/varscan/${SAMPLE_PFX}_UN_snp.vcf
        #INDELS #set strand filter to be 0 so that variants that do not pass do not get thrown out by Varscan.
        java -jar $VARSCAN mpileup2indel $SAVEPATH/varscan/${SAMPLE_PFX}_UN.pileup --strand-filter 0 --min-var-freq 0.01 --min-reads2 4 --output-vcf 1 > $SAVEPATH/varscan/${SAMPLE_PFX}_UN_indel.vcf
}

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vardict () {

        #2. VARDICT
	#https://github.com/AstraZeneca-NGS/VarDictJava
        #[f-threshold for AF][b-indexed bam file][r-minimum reads][z-bed file 0/1 index based, 1 means 0 based][F-hexical to filter reads. Default: 0x504][c-col for chrom][S-col for region start][g-col for gene name]
        #currently, min reads set to 5. Previous criteria was 5 reads for SNPs and 4 for indels
        #currently, min var freq set to 0.03 (f arg), previous criteria was 0.03 for SNPs and 0.01 for indels
        #need a bed file (this was modified from original bed file to contain 4 cols - chrom, start pos, end pos, gene name) and is ZERO based.
        #had to modify command to remove a line from intermediate file for full command to work
        #target sites contained in bed file and sent as in as an argument to this command
        #[vardict.vcf file created]
	#-F Default: 0x504 (filter unmapped reads, 2nd alignments and duplicates)
	#--deldupvar is used to remove duplicate variants (this is different from -t which removes duplicate reads (PCR) and since dedup was done previously by Picard, not specifying it again. Aslo -F 0x504 default setting removes dup reads)
	#COMMAND WAS CHANGED FOR SINGLE SAMPLE take note! <teststrandbias.R> <var2vcf_valid.pl>

	mkdir -p $SAVEPATH/vardict
	$VARDICT -G $REF_GENOME_b37 -f 0.03 -N "my_sample" -b "$SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN.bam" -r 5 -z 1 -c 1 -S 2 -E 3 -g 4 $BEDFILE_0B|sed '1d'|$VARDICT_SCRIPTS_DIR/teststrandbias.R|$VARDICT_SCRIPTS_DIR/var2vcf_valid.pl -N "unpaired" -f 0.03 -v 5 > $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict_dups.vcf

	#Remove duplicate variants from vcf (6/16/21 - realize that --dedupvar/-t/-F 0x504 do not remove duplicate variants)
	$PROGRAM_PATH/tabix-0.2.6//bgzip -c $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict_dups.vcf > $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict_dups.vcf.gz
	$PROGRAM_PATH/tabix-0.2.6/tabix -p vcf $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict_dups.vcf.gz
	$BCFTOOLS norm -d none $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict_dups.vcf.gz > $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict.vcf
}

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

indel_analysis () {
	varscan
	vardict
}

indel_analysis
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
