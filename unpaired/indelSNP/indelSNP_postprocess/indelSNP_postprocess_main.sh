set -x -e
#This script is my own snpindel analyis using Python
#Notes: So in step 1, we use bcftools to merge outputs from varscan (2 indel files for G and T, 2 SNP files for G and T respectively) and vardict and annotate using annovar
#In step 2, we use our own python script to merge these outputs using pythn and parse out useful fields
#In step 3, we merge output from setp 2 with step 3 and post-process the annotated output!


#Read in args
SAMPLE_PFX=$1
SAVEPATH=$2 


#Form a new dir for indel post_analysis for each sample in a run (this is to merge vcfs from varscan and vardict using Bcftools and annotate using Annovar)
rm -rf $SAVEPATH/indelSNP_vcfs
SNP_INDEL_VCFS=$SAVEPATH/indelSNP_vcfs
mkdir -p $SNP_INDEL_VCFS
#Form a new dir within the above dir for python analysis (results from my own python script for post analysis on vcfs from vardict and varscan)
SNP_INDEL_PYTHON=$SNP_INDEL_VCFS/python
mkdir -p $SNP_INDEL_PYTHON

#variables used
#PROGRAM_PATH
#BROCA_GENES
#PYTHON_MERGE
#INDEL_FINAL_POST_PROCESSING
#INDEL_FILTER_EXPLANATION_SCRIPT
#BRCA1_STARITA_PRED
#MISSENSES_FAVES 
#SGE_RAD51C 

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 2 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BROCA_GENES|$PYTHON_MERGE|$INDEL_FINAL_POST_PROCESSING"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$SNP_INDEL_VCFS|$SNP_INDEL_PYTHON|$PROGRAM_PATH"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\




#---------------------------------------------------------PART1:Merge Varscan and Vardict outputs and annotate using Annovar---------------------------------------------------------------------------------------------------------------------------------------------
copy_files_for_annovar () {
        #For each tumor-germline paired sample within a run
        #copy over files for INDELS and SNPS - results from 1)varscan (germline variant caller) 2)vardict (somatic and germline variant caller)
        cp $SAVEPATH/varscan/${SAMPLE_PFX}_UN_snp.vcf $SNP_INDEL_VCFS
        cp $SAVEPATH/varscan/${SAMPLE_PFX}_UN_indel.vcf $SNP_INDEL_VCFS
        cp $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict.vcf $SNP_INDEL_VCFS

}

zip_merge_files () {
	#Compress files using bgzip
        #$PROGRAM_PATH/tabix-0.2.6/bgzip $NEW_PATH/*vcf
        ls $SNP_INDEL_VCFS/*.vcf | xargs -n1 $PROGRAM_PATH/tabix-0.2.6/bgzip 

        #Index compressed vcf files using tabix
        ls $SNP_INDEL_VCFS/*vcf.gz | xargs -n1 $PROGRAM_PATH/tabix-0.2.6/tabix -p vcf

	shopt -s nullglob
        file_str=""
        for VCF in $SNP_INDEL_VCFS/*.vcf.gz
        do
                file_str="$file_str $VCF"
        done
        new_file_str=$(echo $file_str|sed -e 's/^[ \t]*//')
        $PROGRAM_PATH/bcftools-1.9/bcftools merge $new_file_str --force-samples -m none -o $SNP_INDEL_VCFS/${SAMPLE_PFX}_UN_merged_indelSNP.vcf
}

annotate_annovar () {
        $PROGRAM_PATH/annovar/table_annovar.pl $SNP_INDEL_VCFS/${SAMPLE_PFX}_UN_merged_indelSNP.vcf $PROGRAM_PATH/annovar/humandb/ -buildver hg19 -out $SNP_INDEL_VCFS/${SAMPLE_PFX}_UN_merged_indelSNP.vcf -remove -arg '-splicing 10,,,,,,,,,,,,,,,,,,,,,' -protocol refGene,genomicSuperDups,clinvar_20240917,dbnsfp47a,dbscsnv11,snp138,cosmic70,mcap13,cadd13gt20,gnomad211_genome,exac03nontcga,1000g2015aug_eas,1000g2015aug_eur,1000g2015aug_sas,1000g2015aug_amr,1000g2015aug_afr,1000g2015aug_all,esp6500siv2_ea,esp6500siv2_aa,esp6500siv2_all,revel,gerp++gt2 -operation g,r,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f -nastring . -vcfinput
}

#---------------------------------------------------------PART2:Merge varscan and vardict vcfs using custom python script-------------------------------------------------------------------------------------------------------------------
copy_files_for_python () {
        #For each tumor-germline paired sample within a run
        #copy over files for INDELS and SNPS - results from 1)varscan (germline variant caller) 2)vardict (somatic and germline variant caller)
        cp $SAVEPATH/varscan/${SAMPLE_PFX}_UN_snp.vcf $SNP_INDEL_PYTHON
        cp $SAVEPATH/varscan/${SAMPLE_PFX}_UN_indel.vcf $SNP_INDEL_PYTHON
        cp $SAVEPATH/vardict/${SAMPLE_PFX}_UN_vardict.vcf $SNP_INDEL_PYTHON
}


remove_vcf_headers_for_python () {
        #Remove headers till line #CHROM POS REF ALT in each vcf
        for VCF in $SNP_INDEL_PYTHON/*.vcf
        do
                sed -n -i '/#CHROM/,$p' $VCF
        done
}

merge_vardict_varscan_for_python () {
	#Run indel analysis part 1 - output will be TUMOR_PFX_TG_snp_indel.vcf in results/$TUMOR_PFX/indel_vcfs/indel_post_analysis_py/
	#Merging snps and indels from Varscan and Vardict
	python $PYTHON_MERGE $SNP_INDEL_PYTHON/${SAMPLE_PFX}_UN_snp.vcf $SNP_INDEL_PYTHON/${SAMPLE_PFX}_UN_indel.vcf $SNP_INDEL_PYTHON/${SAMPLE_PFX}_UN_vardict.vcf $SNP_INDEL_PYTHON/${SAMPLE_PFX}_UN_pythonmerge.txt #last file is outputfile
}

#---------------------------------------------------------PART3:Merge outputs from annovar and custom_python_script_ merged_output for final output and postprocess that--------------------------------------------------------------------
merge_annovar_python_postprocess () {
	#Run indel analysis part 2 - output will be TUMOR_PFX_TG_snp_indel_analysis.txt in results/$TUMOR_PFX/results/
	python $INDEL_FINAL_POST_PROCESSING $BROCA_GENES $SNP_INDEL_VCFS/${SAMPLE_PFX}_UN_merged_indelSNP.vcf.hg19_multianno.txt $SNP_INDEL_PYTHON/${SAMPLE_PFX}_UN_pythonmerge.txt $BRCA1_STARITA_PRED $MISSENSES_FAVES $SGE_RAD51C $SAVEPATH/results/${SAMPLE_PFX}_UN_snp_indel_analysis.txt $SAVEPATH/results/${SAMPLE_PFX}_UN_snp_indel_analysis_BROCA.txt #last 2 files are outputfiles, BROCA meaning only for BROCA genes

}

vardict_filter_explanation () {
        #Create an output in final results folder that explains filters that one may see in single sample analysis in Vardict
        python $INDEL_FILTER_EXPLANATION_SCRIPT $SAVEPATH/results/vardict_filters.txt
}

del_intermediate_files () {
	rm -r $SNP_INDEL_VCFS
}

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
####SUB FUNCS#####

annovar () {
	copy_files_for_annovar
	zip_merge_files
	annotate_annovar
}

python_processing () {
	copy_files_for_python
	remove_vcf_headers_for_python
	merge_vardict_varscan_for_python
}

final_postprocessing () {
	merge_annovar_python_postprocess
	vardict_filter_explanation
	del_intermediate_files
}
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

####MAIN CODE#####

annovar
python_processing
final_postprocessing

