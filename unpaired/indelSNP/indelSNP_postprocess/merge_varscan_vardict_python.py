import pandas as pd
import sys

VARSCAN_SAMPLE_SNP=sys.argv[1]
VARSCAN_SAMPLE_INDEL=sys.argv[2]
VARDICT=sys.argv[3]
OUTFILE=sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[VARSCAN_SAMPLE_SNP,VARSCAN_SAMPLE_INDEL,VARDICT]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

##############################VARSCAN SNPS##########################################################################################
#let us load sample varscan snp - only pos is read in as int64, rest all object type which means str
varscan_sample_snp = pd.read_csv(VARSCAN_SAMPLE_SNP, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan sample snp
varscan_sample_snp['ref_reads'] = varscan_sample_snp['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_sample_snp['var_reads'] = varscan_sample_snp['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_sample_snp['Reads_R|V'] = varscan_sample_snp['ref_reads'] + "|" + varscan_sample_snp['var_reads']
varscan_sample_snp['ref_reads'] = varscan_sample_snp['ref_reads'].astype("int64")
varscan_sample_snp['var_reads'] = varscan_sample_snp['var_reads'].astype("int64")
varscan_sample_snp['VAF'] = (varscan_sample_snp['var_reads']/(varscan_sample_snp['var_reads']+varscan_sample_snp['ref_reads'])).round(2)

varscan_SNP = varscan_sample_snp[["#CHROM","POS","REF","ALT","VAF","Reads_R|V"]]

##############################VARSCAN INDELS##########################################################################################
#let us load varscan sample indel- only pos is read in as int64, rest all object type which means str
varscan_sample_indel = pd.read_csv(VARSCAN_SAMPLE_INDEL, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan sample indel
varscan_sample_indel['ref_reads'] = varscan_sample_indel['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_sample_indel['var_reads'] = varscan_sample_indel['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_sample_indel['Reads_R|V'] = varscan_sample_indel['ref_reads'] + "|" + varscan_sample_indel['var_reads']
varscan_sample_indel['ref_reads'] = varscan_sample_indel['ref_reads'].astype("int64")
varscan_sample_indel['var_reads'] = varscan_sample_indel['var_reads'].astype("int64")
varscan_sample_indel['VAF'] = (varscan_sample_indel['var_reads']/(varscan_sample_indel['var_reads']+varscan_sample_indel['ref_reads'])).round(2)

varscan_INDEL = varscan_sample_indel[["#CHROM","POS","REF","ALT","VAF","Reads_R|V"]]

##############################VARSCAN###################################################################################
########################################################################################################################
#concat results from SNP and INDEL from varscan vcfs
varscan = pd.concat([varscan_SNP,varscan_INDEL])

##############################VARDICT##########################################################################################
###############################################################################################################################
#Let us load vardict output
vardict = pd.read_csv(VARDICT, sep= "\t", header = 0, dtype = "str")
#Let us post-process vardict vcf
#For sample
vardict["ref_reads"] = vardict["unpaired"].apply(lambda x: x.split(":")[3]).apply(lambda x: x.split(",")[0]) #AD[0] = ADref
vardict["var_reads"] = vardict["unpaired"].apply(lambda x: x.split(":")[3]).apply(lambda x: x.split(",")[1]) ##AD[1] = ADvar
#Combine info
vardict["Reads_R|V"] = vardict["ref_reads"] + "|" + vardict["var_reads"]
#Add the VAF col
vardict["ref_reads"] = vardict["ref_reads"].astype("int64")
vardict["var_reads"] = vardict["var_reads"].astype("int64")
vardict["VAF"] = (vardict["var_reads"]/(vardict["var_reads"]+vardict["ref_reads"])).round(2)

#Let us parse out the filter, quality, Type of event, Event Status and MSI status too
vardict ["Filter(Vardict)"] = vardict["FILTER"] #Specifying (Vardict) in col name because I am not taking Varscan's Filter col, too many filter cols will cause confusion
vardict["TypeOfEvent"] = vardict["INFO"].apply(lambda x: x.split(";")[1]).apply(lambda x: x.split("=")[1]) #parsing out TYPE= from "INFO" col #[] no differs in paired vs unpaired analysis!
vardict["MSI"] = vardict["INFO"].apply(lambda x: x.split(";")[20]).apply(lambda x: x.split("=")[1]) # parsing out MSI= from "INFO" col #[] no differs in paired vs unpaired analysis!

#################################################################################################################################
##############################SNP_INDEL##########################################################################################
#################################################################################################################################
indels = pd.merge(varscan.loc[:,["#CHROM","POS","REF","ALT","VAF","Reads_R|V"]],vardict.loc[:,["#CHROM","POS","REF","ALT","VAF","Reads_R|V","VAF","QUAL","TypeOfEvent","MSI","Filter(Vardict)"]], on =["#CHROM","POS","REF","ALT"], how="outer", suffixes=["(Varscan)","(Vardict)"])
#write output to file
indels.to_csv(OUTFILE,sep = "\t", index= False)
