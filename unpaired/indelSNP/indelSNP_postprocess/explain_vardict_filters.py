#Small script to create the vardict filters that may be generated 

import sys
import pandas as pd

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 2 :
    print "CHECK: Not all python args provided "
    sys.exit (1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

OUTPUT_FILE = sys.argv[1]

#LONGMSI explanation has the word "somatic variant" in it. I beleive that is an error and removed it here #CHECKKK?.
with open (OUTPUT_FILE, "w+") as output:
	output.write ("Vardict_Filter\tFilter_Explanation\n")
	output.write("q22.5\tMean Base Quality Below 22.5\nQ10\tMean Mapping Quality Below 10\np8\tMean Position in Reads Less than 8\nSN1.5\tSignal to Noise Less than 1.5\nBias\tStrand Bias\npSTD\tPosition in Reads has STD of 0\nd3\tTotal Depth < 3\nv5\tVar Depth < 5\nf0.03\tAllele frequency < 0.03\nMSI12\tVariant in MSI region with 12 non-monomer MSI or 13 monomer MSI\nNM5.25\tMean mismatches in reads = 5.25, thus likely false positive\nInGap\tThe variant is in the deletion gap, thus likely false positive\nInIns\tThe variant is adjacent to an insertion variant\nCluster0bp\tTwo variants are within 0 bp\nLongMSI\tThe variant is flanked by long A/T (=14)\nAMPBIAS\tIndicate the variant has amplicon bias\n")
