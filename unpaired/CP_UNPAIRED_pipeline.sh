#!/bin/bash 
#Always make sure this is first line or else line 150 will error out saying bad substitution 

SAMPLE_PFX=$1  
RUN=$2
RUN_TYPE=$3
source paths/unpaired_paths.sh
source paths/common_paths.sh $RUN $RUN_TYPE

set -x -e

#create subdirectory for this sample
SAVEPATH=$SAVEROOT/$SAMPLE_PFX
mkdir -p $SAVEPATH

#create a dir within SAVEPATH for RESULTS from each program that runs PER tumor sample pair (dir of FINAL OUTPUTS)
mkdir -p $SAVEPATH/results        

####################################################################################CHECK THAT ALL FILES NEEDED ABOVE EXIST###########################################################################
#Parameter check
if [ "$#" -ne 3 ]; then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BBMERGE|$CUTADAPT|$BWA|$REF_GENOME_b37|$SAMTOOLS|$PICARD|$GATK|$BEDFILE_0B|$BEDFILE_0B_genes|$BROCA_GENES|$REFSEQ|$QC_METRICS_SCRIPT|$QC_EXON_GENE_SCRIPT|$INDEL_SCRIPT|$INDEL_POST_PROCESSING_SCRIPT|$GRIDSS_SCRIPT|$PINDEL_SCRIPT|$MSINGS_SCRIPT|$COMBINE_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        #exit 1
		fi
done

#Dir check
DIRS="$VARIANT_FILES_b37|$SAVEROOT|$SAVEPATH/results"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        #exit 1
                fi
done
unset IFS
########################################################################################################################################################################################################

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#FOR EACH UNPAIRED SAMPLE OF A RUN:
#########
#PART1#
#########

find_fastqs () {
	#Dr.Ming provides .txt.gz files while MedGenome provides us with fastq.gz files 
	#Find sample sequences
	SAMPLE_R1_old=$(find $DATAPATH/$SAMPLE_PFX/ | grep -E '*_1_sequence.txt.gz|*_R1.fastq.gz')
	SAMPLE_R2_old=$(find $DATAPATH/$SAMPLE_PFX/ | grep -E '*_2_sequence.txt.gz|*_R2.fastq.gz')
}

bbmerge () {
	mkdir -p $SAVEPATH/bbmap
	$BBMERGE -Xmx1g -ignorejunk in1=$SAMPLE_R1_old  in2=$SAMPLE_R2_old outa=$SAVEPATH/bbmap/${SAMPLE_PFX}_UN_adapters.fa itn 
}

obtain_adapters () {
        #Obtain forward and reverse adapters from tumor and germline sample to use for CUTADAPT. Obtain this from previous BBMERGE output!
        FP_SAMPLE=`sed '2q;d' $SAVEPATH/bbmap/${SAMPLE_PFX}_UN_adapters.fa`
        RP_SAMPLE=`sed '4q;d' $SAVEPATH/bbmap/${SAMPLE_PFX}_UN_adapters.fa`
        echo "OBTAINING PRIMERS done...."
}

renaming_files () {
	if [[ "$SAMPLE_R1_old" == *"txt.gz"* ]];then
		#Rename file names (.fastq prefix needed for cutadapt)
		replace="fastq.gz"

		SAMPLE_R1=${SAMPLE_R1_old/txt.gz/$replace}
		cp $SAMPLE_R1_old $SAMPLE_R1
		SAMPLE_R2=${SAMPLE_R2_old/txt.gz/$replace}
		cp $SAMPLE_R2_old $SAMPLE_R2
	else
		SAMPLE_R1=$SAMPLE_R1_old
		SAMPLE_R2=$SAMPLE_R2_old
	fi
	echo "RENAMING FILES done....."
}

cutadapt () {
	$CUTADAPT -a $FP_SAMPLE -A $RP_SAMPLE -o $SAVEPATH/bbmap/s_2_1_sequence_trimmed_UN.fastq.gz -p $SAVEPATH/bbmap/s_2_2_sequence_trimmed_UN.fastq.gz $SAMPLE_R1 $SAMPLE_R2
	echo "CUTADAPT done...."
}

bwa_mem () {	
	#BWA ALIGNMENT
	mkdir -p $SAVEPATH/bwa
	echo "BWA MEM starting...."
	#Try to replace "unpaired" in string with sample pfx
	$BWA mem -t $CORES -M -R "@RG\tID:G\tPL:ILLUMINA\tPU:NA\tLB:null\tSM:${SAMPLE_PFX}" $REF_GENOME_b37 $SAVEPATH/bbmap/s_2_1_sequence_trimmed_UN.fastq.gz $SAVEPATH/bbmap/s_2_2_sequence_trimmed_UN.fastq.gz > $SAVEPATH/bwa/${SAMPLE_PFX}_UN.sam
	echo "BWA MEM done...."
}


samtools () {
	#version 1.9 versions -(http://www.htslib.org/doc/) specific link-(http://www.htslib.org/doc/samtools.html)
	#SAMTOOLS to view (change to bam file) and sort
	mkdir -p $SAVEPATH/samtools 
	$SAMTOOLS view -bSu -F 4 -q 20 $SAVEPATH/bwa/${SAMPLE_PFX}_UN.sam | $SAMTOOLS sort - -o $SAVEPATH/samtools/${SAMPLE_PFX}_UN_sorted.bam
}

picard_index_bam () {
        java -jar $PICARD BuildBamIndex I=$SAVEPATH/samtools/${SAMPLE_PFX}_UN_sorted.bam
        echo "PICARD BUILDBAMINDEX done...."
}

picard_metrics () {
	mkdir -p $SAVEPATH/picard
	java -jar $PICARD CollectInsertSizeMetrics I=$SAVEPATH/samtools/${SAMPLE_PFX}_UN_sorted.bam O=$SAVEPATH/picard/${SAMPLE_PFX}_UN_insert_metrics.txt H= $SAVEPATH/picard/${SAMPLE_PFX}_UN_insert_metrics_histogram.pdf
	echo "PICARD COLLECTINSERTMETRICS done...."
}

############# LAB ANALYSIS FILE ########################
picard_deduplicates_mark () {
	#This is the version the lab wants for analysis - dups marked but not removed
	java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${SAMPLE_PFX}_UN_sorted.bam O=$SAVEPATH/picard/${SAMPLE_PFX}_UN_marked_duplicates.bam REMOVE_DUPLICATES=false M=$SAVEPATH/picard/marked_dup_metrics_${SAMPLE_PFX}_UN.txt CREATE_INDEX=true
	echo "PICARD MARKDUPLICATES done...."
}

gatk_recalibration_no_bedfile () {
	#This is the version the lab wants for IGV analysis - recalibration wihthout filtering with bedfile
	mkdir -p $SAVEPATH/recal
	echo "GATK BASE RECALIBRATOR starting without bedfile...."
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${SAMPLE_PFX}_UN_marked_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -O $SAVEPATH/recal/${SAMPLE_PFX}_UN_recal_labv.table
	echo "GATK BASE RECALIBRATOR done without bedfile...."

	echo "CREATION OF FINAL BAMS starting without bedfile...."
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${SAMPLE_PFX}_UN_marked_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${SAMPLE_PFX}_UN_recal_labv.table -O $SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN_labv.bam
	echo "FINAL BAMS creation done without bedfile...."
}

QC_metrics () {
	#Twist's QC is based on a bam that is sorted but not filtered using a bedfile or duplicated reads (these are marked but not removed)
        #To match their analysis, we are doing the same here

        #this is to create interval list for baits used by QC_metrics
        java -jar $PICARD BedToIntervalList \
        I=$BEDFILE_0B_BAITS \
        O=$SAVEPATH/recal/baits.interval_list \
        SD=$REF_GENOME_b37

        #this is to create interval list for targets used by QC_metrics
        java -jar $PICARD BedToIntervalList \
        I=$BEDFILE_0B \
        O=$SAVEPATH/recal/targets.interval_list \
        SD=$REF_GENOME_b37

        #For SAMPLE
        #This command requires an .interval_list file as target and bait files.
        java -jar $PICARD CollectHsMetrics \
        I=$SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN_labv.bam \
        O=$SAVEPATH/picard/${SAMPLE_PFX}_UN_hs_metrics.txt \
        R=$REF_GENOME_b37 \
        BAIT_INTERVALS=$SAVEPATH/recal/baits.interval_list \
        TARGET_INTERVALS=$SAVEPATH/recal/targets.interval_list
}

############# LAB ANALYSIS FILE ########################

#We are re-running picard and recalibration here to remove duplicates and filter using bedfiles so that we can give clean bams to the downstream processing tools

picard_deduplicates_remove () {
        java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${SAMPLE_PFX}_UN_sorted.bam O=$SAVEPATH/picard/${SAMPLE_PFX}_UN_no_duplicates.bam REMOVE_DUPLICATES=true M=$SAVEPATH/picard/removed_dup_metrics_${SAMPLE_PFX}_UN.txt CREATE_INDEX=true
        echo "PICARD REMOVEDUPLICATES done...."
}

gatk_recalibration_bedfile () {
	#Version GATK 4.8.0.1 (https://software.broadinstitute.org/gatk/documentation/tooldocs/4.0.8.1/org_broadinstitute_hellbender_tools_walkers_bqsr_BaseRecalibrator.php)
	#Explanation of what this does -https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_bqsr_BaseRecalibrator.php
	#Various positions for -L tried, -L 1 works not -L chr1. Placement in command crucial. Not enough documentation
        #https://software.broadinstitute.org/gatk/documentation/article?id=11009 (L arg GATK for interval list) (should be 0 based bed file) (.bed in itself means 0 based)
        #https://gatkforums.broadinstitute.org/gatk/discussion/4133/when-should-i-use-l-to-pass-in-a-list-of-intervals
	echo "GATK BASE RECALIBRATOR starting with bedfile...."
	mkdir -p $SAVEPATH/recal
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${SAMPLE_PFX}_UN_no_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -L $BEDFILE_0B -O $SAVEPATH/recal/${SAMPLE_PFX}_UN_recal.table
	echo "GATK BASE RECALIBRATOR done...."

	#GATK to apply recalibration results to bam files
	#FINAL BAM FILES
	#LOOKS LIKE THIS CREATES AN UPDATED BAI FILE AS WELL!! #https://github.com/broadinstitute/gatk/issues/4219, because optional common argument --create-output-bam-index is by default true!!
	echo "CREATION OF FINAL BAMS starting...."
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${SAMPLE_PFX}_UN_no_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${SAMPLE_PFX}_UN_recal.table -L $BEDFILE_0B -O $SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN.bam
	echo "FINAL BAMS creation done...."
}


#########
#PART2#
#########

qc_metrics() {
	#For 1.QC metrics
	$QC_METRICS_SCRIPT $SAVEPATH $SAMPLE_PFX $SAVEPATH/picard/${SAMPLE_PFX}_UN_hs_metrics.txt $SAVEPATH/picard/removed_dup_metrics_${SAMPLE_PFX}_UN.txt $SAVEPATH/picard/${SAMPLE_PFX}_UN_insert_metrics.txt
}

qc_exon_gene () {
	#For 2.QC per exon or per gene
	$QC_EXON_GENE_SCRIPT $SAMPLE_PFX $SAVEROOT $SAVEPATH
}

indelSNP () {
	#For 3.Varscan and 2.Vardict #uses ANNORVAR for annotation
	$INDEL_SCRIPT $SAVEPATH $SAMPLE_PFX
	$INDEL_POST_PROCESSING_SCRIPT $SAMPLE_PFX $SAVEPATH
}

gridss () {
	#For 4.Gridss #uses ANNOTSV for annotation
	$GRIDSS_SCRIPT $SAVEPATH $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt $SAMPLE_PFX 
}

pindel () {
	#For 5.Pindel #uses ANNOTSV and custom script for annotation
	$PINDEL_SCRIPT $SAVEPATH $SAMPLE_PFX $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt 
}

msings () {
	#For 6.Msings
	$MSINGS_SCRIPT $SAVEPATH $SAMPLE_PFX
}

#For 7.Cnvkit
#takes in all normal bams at once to create pooled reference, so run as standalone script


#Combine all outputs
produce_final_output () {
	python $COMBINE_SCRIPT $SAVEPATH $SAMPLE_PFX $SAVEPATH/${SAMPLE_PFX}.xlsx
}

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#####Sub-functions######
CP_read_alignment () {

        #Functions from top till bwa mem need to run TOGETHER
        find_fastqs
	bbmerge
	obtain_adapters
        renaming_files
        cutadapt
        bwa_mem
        samtools
	picard_index_bam
        picard_metrics
	#=========labv_files==========
        picard_deduplicates_mark #new bai formed
	gatk_recalibration_no_bedfile #new bai formed
	QC_metrics
	#=========labv_files==========
	picard_deduplicates_remove #new bai formed
	gatk_recalibration_bedfile #new bai formed
}

run_analysis () {
        qc_metrics
        qc_exon_gene
        indelSNP
        gridss
        pindel
        msings
        produce_final_output
}

####Main-functions######
CP_read_alignment
run_analysis
