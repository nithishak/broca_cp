#This script doesn't do much other than adding col names, just kept it to be consistant with paired CP pipeline (in which T and G outputs are merged).
import sys
import pandas as pd 

SAMPLE_PFX = sys.argv[1]
SAMPLE_OUTPUT= sys.argv[2]
OUTPUT_FILE= sys.argv[3]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 4 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[SAMPLE_OUTPUT]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#Let us merge the 2 files together
msings_sample = pd.read_csv(SAMPLE_OUTPUT, sep= "\t", header = 0)

#Rename the cols from bqsr_SAMPLE_PFX to SAMPLE_PFX
msings_sample.columns = ["Position",SAMPLE_PFX]

#Deal with: if no of unstable loci and pasing loci is 0, then msings output will NOT have a msing_score row!
if msings_sample.iloc[2,0] != "msing_score":
        #add in the msing_score for it
	msings_upper = msings_sample.iloc[0:2,:]
	msings_lower = msings_sample.iloc[2:,:]
	
	new_line = [{"Position": "msing_score" , SAMPLE_PFX : ""}]
	msings_upper = msings_upper.append(new_line, ignore_index=True, sort=False)

	msings_sample = pd.concat([msings_upper, msings_lower], ignore_index= True)
	
	#change the msi status from neg to undetermined
        msings_sample.iloc[3,1] = "UNDETERMINED" 

#Add chr prefix to positions
for i in range(4,msings_sample.shape[0]):
	msings_sample.loc[i,"Position"] = "chr" + msings_sample.loc[i,"Position"]
		

msings_sample.to_csv(OUTPUT_FILE, sep="\t", index= False)
