set -x #did not put -xe because code below has if else loop, we dont want this to exit if sample has no pindel results 
SAVEPATH=$1
SAMPLE_PFX=$2
TX_FILE=$3
CANGENES_FILE=$4

#variables used
#PINDEL
#BEDTOOLS
#PINDEL_POST_ANALYIS_SCRIPT
#PINDEL_MERGE_SCRIPT
#BEDFILE_0B
#BROCA_GENES
#REFSEQ

SAMPLE_BAM_FILE=$SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN.bam
SAMPLE_INSERT_METRICS_FILE=$SAVEPATH/picard/${SAMPLE_PFX}_UN_insert_metrics.txt

#Make a dir for Pindel
PINDEL_DIR=$SAVEPATH/pindel
mkdir -p $PINDEL_DIR

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 4 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BEDTOOLS|$SAMPLE_BAM_FILE|$SAMPLE_INSERT_METRICS_FILE|$PINDEL_MERGE_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$PINDEL|$PINDEL_DIR"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

pindel_create_config () {

        #4. PINDEL
        # From GO-029 and MY-002 onwards, the target fragment insert sizes have changed from 150-200bp to 200-250 bp take note.
        #manually filter out results (indels/tds >10bp)
	echo -n "$SAMPLE_BAM_FILE " > $PINDEL_DIR/pindel_config.txt
        #echo -n `sed -n '8p' $SAMPLE_INSERT_METRICS_FILE | awk -F"\t" '{print int($6)}'` >> $PINDEL_DIR/pindel_config.txt #round average insert size to nearest info or the name of the sample will not be right
	UN_INSERT_SIZE=`sed -n '8p' $SAMPLE_INSERT_METRICS_FILE | awk -F"\t" '{print int($6)}'`
	echo "Insert size for unpaired sample is $UN_INSERT_SIZE"
	if [ $UN_INSERT_SIZE -ge 102 ];then
		echo -n $UN_INSERT_SIZE >> $PINDEL_DIR/pindel_config.txt #round average insert size to nearest info or the name of the sample will not be right
	else
		echo "INSERT SIZE FOR UNPAIRED SAMPLE IS SMALLER THAN READ LENGTH, TAKE NOTE!"
                echo -n "102" >> $PINDEL_DIR/pindel_config.txt
        fi
        echo " unpaired_sample" >> $PINDEL_DIR/pindel_config.txt

}

run_pindel () {
	#Run Pindel - separately for tumor and germline samples
	#$PINDEL/pindel -f $REF_GENOME_b37 -i $SAVEPATH/pindel/pindel_config.txt -c ALL -o $SAVEPATH/pindel/pindel -r false -k false  ####SHOULD I USE THESE OPTIONS? r is for inversions and k is for breakpoints
	#https://github.com/genome/pindel/blob/master/FAQ (for pindel running too slowly)
	##Take out -T 4 option, giving core dumped error when running in parallel with other samples in batch
	
	#$PINDEL/pindel -f $REF_GENOME_b37 -i $PINDEL_DIR/pindel_config.txt -c ALL -o $PINDEL_DIR/pindel -j $BR_BED_FILE -T 4
	$PINDEL/pindel -f $REF_GENOME_b37 -i $PINDEL_DIR/pindel_config.txt -c ALL -o $PINDEL_DIR/pindel -j $BEDFILE_0B
}


#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
annotate_CUSTOM () {
	
	#Annotate using custom script (our own annotation script)
	#create outputfile and create header
	echo -e "SV_chrom\tSV_start\tSV_end\tGene\tEvent_Size\tGene_region\tEvent_Type\tTranscripts\t$SAMPLE_PFX" >$SAVEPATH/pindel/pindel_custom_annotated.txt 
	#Format for sample is: total no of read- [no of reads on + strand , - strand that match alt allele]
	files="$PINDEL_DIR/pindel_D $PINDEL_DIR/pindel_SI $PINDEL_DIR/pindel_INV $PINDEL_DIR/pindel_TD"
	shopt -s nullglob
	for file in $files
        do
        python $PINDEL_POST_ANALYIS_SCRIPT $file $PINDEL_DIR/pindel_custom_annotated.txt $BROCA_GENES $REFSEQ
        done

	#individual pindel output files may contain dups so make sure to remove dups in final output
}
#-----------------------------------------------------------------------------------------------------------------------
create_pindel_vcf () {
        #Make one vcf from all pindel outputs
	#pindel2vcf -p <pindel_output_file> -r <reference_file> -R <name_and_version_of_reference_genome> -d <date_of_reference_genome_version> [-v <vcf_output_file>]
	#there is no webpage for this, type pindel/pindel2vcf --help for options in linux
	#-P option takes all pindel outputs with similar prefixes and produces 1 big vcf
	#we can specify the min no of supporting reads but in the interest of increased sensitivity (we want tumors with very few reads - rare variants), and not specificity, we use conservative no of 3
	#we can also choose to specify --both_strands_supported true but lab would like the info for each strand to not be filtered out here and available to analyze in the final output as to how many reads forward/reverse strand has
	#LI and BP files (long insertion and break point files) have a different type of header andare not supported yet.
	#pindel_RP file is an intermediate output for developers to look into discordant read pairs. It will not be used by us anywhere. All other files (D,INV,SI and TD files have same format)
	#Note that cutoff to call heterozygous is 0.2 to 0.8. CHECKKKKKK if this affects annotsv/author says annotsv does NOT annotate GT 0/0 for G and T/but I have seen AnnotSV annotate this. NEEDS CLARIFICATION.
	#Min supporting reads 3 means that for any event where no of unique reads for +and- strand for ALL samples in pindel config file is equal to 3 or above.
	#This may include G 2 reads and T 1 reads but what we want in a min supporting read of 3 PER sample. So filter this result AGAIN in post-processing step!
	#We want any events bigger than 10 bps only as such events should be picked up by indelSNP analysis. Also <100bp events but this we shall decide to act on later. For now, just filter for more than 10 bp events.
	 #--compact_output_limit  Puts all structural variations of which either the ref allele or the alt allele exceeds the specified size (say 10 in '-co 10') in the format 'chrom pos first_base <SVType>' (we are doing this here to avoid errors in AnnotSV for large events of size 1 mil and more)
	$PINDEL/pindel2vcf -P $PINDEL_DIR/pindel -r $REF_GENOME_b37 -R b37 -d 00000000 -v $PINDEL_DIR/pindel_ALL.vcf --min_supporting_reads 3 --min_size 10 --compact_output_limit 200
}


annotate_ANNOTSV () {
	#Let us run Annotsv using vcf
	#we want events between 10 to 100bp only. Lesser than 10bp should be found by indelsnp analysis and more than 100bp is probably false. So filter out less than 10 bp here. We shall filter out more than 100bp if we decide to later on
	#pindel_ALL.vcf that is made from individual pindel outputs also contain dup rows but somwhow after annotsv annotations, there are no dups present. Remove dups in final output..
	export "ANNOTSV=/mnt/disk4/labs/salipante/nithisha/programs/AnnotSV"
	$ANNOTSV/bin/AnnotSV -SVinputFile $PINDEL_DIR/pindel_ALL.vcf -outputFile $PINDEL_DIR/pindel_annotsv_annotated.vcf -bedtools $BEDTOOLS -SVminSize 0 -candidateGenesFile $CANGENES_FILE -candidateGenesFiltering yes -txFile $TX_FILE -annotationMode split -bcftools $BCFTOOLS
	
	#This file may contain non-ASCII characters and causes problems when merging so remove them here inplace
	LANG=C sed -i 's/[\d128-\d255]//g' $PINDEL_DIR/pindel_annotsv_annotated.vcf.tsv	

}

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
merge_custom_annotsv () {
	python $PINDEL_MERGE_SCRIPT $SAMPLE_PFX $PINDEL_DIR/pindel_custom_annotated.txt $PINDEL_DIR/pindel_annotsv_annotated.vcf.tsv $PINDEL_DIR/pindel_${SAMPLE_PFX}_UN_merged.txt
	#remove dup lines here based on first 5 cols (SV position (chr:start-stop), SV length, SV type, REF and ALT)
	awk '!x[$1,$2,$3,$4,$5]++' $PINDEL_DIR/pindel_${SAMPLE_PFX}_UN_merged.txt > $SAVEPATH/results/pindel_${SAMPLE_PFX}_UN.txt
}
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
rm_files () {
	rm -r $PINDEL_DIR
}
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code
pindel_create_config

if run_pindel;then
	echo "PINDEL was successful"
	annotate_CUSTOM
	create_pindel_vcf
	annotate_ANNOTSV
	merge_custom_annotsv
else
	echo "PINDEL failed!!!"
	echo -e "SV Position\tSV length\tSV type\tREF\tALT\t${SAMPLE_PFX}_Var[Fwd,Rev]\tFILTER\tGene name\tNM\tCDS length\ttx length\tlocation\tlocationType\tintersectStart\tintersectEnd\tDGV_GAIN_IDs\tDGV_GAIN_found|tested\tDGV_GAIN_Frequency\tDGV_LOSS_IDs\tDGV_LOSS_found|tested\tDGV_LOSS_Frequency\tGD_ID\tGD_AN\tGD_N_HET\tGD_N_HOMALT\tGD_AF\tGD_POPMAX_AF\tGD_ID_others\t1000g_event\t1000g_AF\t1000g_max_AF\tpromoters\tdbVar_event\tdbVar_variant\tdbVar_status\tAnnotSV ranking" >> $SAVEPATH/results/pindel_${SAMPLE_PFX}_UN.txt
fi
#rm_files
