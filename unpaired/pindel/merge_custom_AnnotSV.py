import sys
import pandas as pd

SAMPLE_PFX = sys.argv[1]
CUSTOM_OUTPUT= sys.argv[2]
ANNOTSV_OUTPUT = sys.argv[3]
OUTPUT_FILE = sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[CUSTOM_OUTPUT,ANNOTSV_OUTPUT]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


custom_output = pd.read_csv(CUSTOM_OUTPUT, sep= "\t", header=0, index_col = False, dtype = "str")
annotsv_output = pd.read_csv(ANNOTSV_OUTPUT, sep= "\t", header=0, index_col = False, dtype = "str")

#Let us rename some cols
custom_output = custom_output.rename(columns = {"Event_Size":"SV_length"})
annotsv_output = annotsv_output.rename(columns= {"Location2" : "Location_Type"}) 
#Let us modify the "location" col such that if it says "intronX - intronX" (the same intron), col "location2" would say "intron" instead of "CDS".
#Note that if col "location" says "intronA-intronC", it includes exonB so it is ok for  "location2" to say "CDS"
def indicate_introns (location,location2) :
        if (pd.notnull(location) and "intron" in location and location.split("-")[0] == location.split("-")[1]):
                return "intron"
        else:
                return location2
annotsv_output['Location_Type'] = annotsv_output.apply(lambda x: indicate_introns (x['Location'], x['Location_Type']), axis=1)

#Merge these 2 dfs 
#06/17/21 - had diff getting ref and alt information from pindel files into pindel_custom_annotated.txt so using SV_length param to improve merge specificity for same event & positions sharing events
#monitor for cases where different events have same sv chrom, sv start, sv end & sv length, causing dups in final results for diff events
merged = pd.merge(custom_output, annotsv_output, how = "inner", on = ["SV_chrom", "SV_start", "SV_end","SV_length"])

#Sort acc to Gene names and then SV start
merged = merged.sort_values(["Gene_name", "SV_start"])

#Create new col called POS that combines SV chrom, SV start and SV end
merged["SV_Position"] = "chr" + merged["SV_chrom"] + ":" + merged["SV_start"] + "-" + merged["SV_end"]

#Only pick cols we need
new_df = merged[["SV_Position" ,"SV_length","SV_type","REF","ALT",SAMPLE_PFX,"FILTER","Gene_name","Gene_count","Tx","Tx_start","Tx_end","Overlapped_tx_length","Overlapped_CDS_length","Overlapped_CDS_percent","Frameshift","Exon_count","Location","Location_Type","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]]

#Let us rename the sample pfx col to make the annotation clearer
new_df = new_df.rename(columns = {SAMPLE_PFX : SAMPLE_PFX + "_Var[Fwd,Rev]"})

new_df.to_csv(OUTPUT_FILE,sep="\t",index=None)
