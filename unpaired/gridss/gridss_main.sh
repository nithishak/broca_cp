set -x
SAVEPATH=$1
TX_FILE=$2
BROCA_GENES_FILE=$3
SAMPLE_PFX=$4

#variables used
#GRIDSS
#BEDTOOLS
#GRIDSS_POSTPROCESS_SCRIPT

GRIDSS_DIR=$SAVEPATH/gridss
mkdir $GRIDSS_DIR


#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 4 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$REF_GENOME_b37|$TX_FILE|$CANGENES_FILE|$GRIDSS|$BEDTOOLS|$GRIDSS_POSTPROCESS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$SAVEPATH|$GRIDSS_DIR"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#single sample analysis
gridss_single_sample () {
        java -ea -Xmx31g -Dsamjdk.create_index=true -Dsamjdk.use_async_io_read_samtools=true -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=true -Dsamjdk.compression_level=1 -Dgridss.gridss.output_to_temp_file=true -cp $GRIDSS gridss.CallVariants REFERENCE_SEQUENCE=$REF_GENOME_b37 INPUT=$SAVEPATH/recal/bqsr_${SAMPLE_PFX}_UN.bam OUTPUT=$GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.sv.vcf ASSEMBLY=$GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.gridss.assembly.bam

        #Annotate using AnnotSV
	export "ANNOTSV=/mnt/disk4/labs/salipante/nithisha/programs/AnnotSV"
        $ANNOTSV/bin/AnnotSV -SVinputFile $GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.sv.vcf -outputFile $GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.sv.annotated.vcf -bedtools $BEDTOOLS -SVminSize 0 -txFile $TX_FILE -annotationMode split -bcftools $BCFTOOLS

        #This file may contain non-ASCII characters and causes problems when merging so remove them here inplace
        LANG=C sed -i 's/[\d128-\d255]//g' $GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.sv.annotated.vcf.tsv
}



#postprocess outputs
gridss_postprocess () {
	python $GRIDSS_POSTPROCESS_SCRIPT $GRIDSS_DIR/gridss_${SAMPLE_PFX}_UN_output.sv.annotated.vcf.tsv $SAVEPATH/results/gridss_${SAMPLE_PFX}_UN_output.sv.annotated.vcf $BROCA_GENES_FILE #last to last arg is outputfile
}


rm_files () {
	rm -r $GRIDSS_DIR
}


#Main code
gridss_single_sample
gridss_postprocess
rm_files
