#paths needed only for scripts from unpaired pipeline

#unpaired/CP_UNPAIRED_pipeline.sh
#-----------------------------------------------------------------
#For QC metrics - general information regarding each sample
export QC_METRICS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_metrics/QC_metrics_main.sh"
#For QC per exon and per gene per sample
export QC_EXON_GENE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_exon_gene/QC_exon_gene_main.sh"
#For indel analysis
export INDEL_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/indelSNP/indelSNP_main.sh"
export INDEL_POST_PROCESSING_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh"
#For structural variant analysis
#GRIDSS identifies the breakpoints for SV events but does not specify explicitly what is is (might be able to refer from ref and alt columns)
export GRIDSS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/gridss/gridss_main.sh"
#For tandem dup information
export PINDEL_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/pindel/pindel_main.sh"
#For Msings
export MSINGS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/msings/msings_main.sh"
#To COMBINE all outputs - Remember, if there are any changes in outputs from above programs, changes need to be made in this script too!
export COMBINE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/combine_results_per_sample/combine_results_per_sample_main.py"

#unpaired/QC_metrics/QC_metrics_main.sh
#-------------------------------------------------------------------------------
export QC_METRICS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_metrics/qc_metrics_postprocess.py"

#unpaired/QC_exon_gene/QC_exon_gene_main.sh
#---------------------------------------------------------------------------------------
export QC_PER_EXON_GENE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_exon_gene/fit_per_exon_gene.py"
export MERGE_FILE="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/QC_exon_gene/coverage_merge.py"      

#unpaired/indelSNP/indelSNP_main.sh
#-----------------------------------------------------------------------
export VARDICT_SCRIPTS_DIR="/mnt/disk4/labs/salipante/nithisha/programs/VarDictJava/build/install/VarDict/bin"
#unpaired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh
#-----------------------------------------------------------------------------------------------------------------------------------------
export PYTHON_MERGE="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/indelSNP/indelSNP_postprocess/merge_varscan_vardict_python.py"
export INDEL_FINAL_POST_PROCESSING="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/indelSNP/indelSNP_postprocess/merge_annovar_python.py"
export INDEL_FILTER_EXPLANATION_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/indelSNP/indelSNP_postprocess/explain_vardict_filters.py"

#unpaired/gridss/gridss_main.sh
#----------------------------------------------------------------
export GRIDSS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/gridss/gridss_postprocess.py"

#unpaired/pindel/pindel_main.sh
#--------------------------------------------------------------
export PINDEL_POST_ANALYIS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/pindel/pindel_custom_annotate.py"
export PINDEL_MERGE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/pindel/merge_custom_AnnotSV.py" 
#unpaired/pindel/pindel_custom_annotate.py
#-------------------------------------------------------------------------------------
#sys.path.append("/mnt/disk4/labs/salipante/nithisha/CP/unpaired/annotation")

#unpaired/msings/msings_main.sh
#----------------------------------------------------------------
export MSINGS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/msings/msings_postprocess.py"  

#unpaired/combine_results_per_sample/combine_results_per_sample_main.py 
#-----------------------------------------------------------------------------------------------------------------------------------------------
#ensure that columns for indelsnp and gridss to be formatted are right here 
