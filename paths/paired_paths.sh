#paths needed only for scripts from paired pipeline

#paired/CP_PAIRED_pipeline.sh
#-----------------------------------------------------------------
#For QC metrics - general information regarding each sample
export QC_METRICS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/QC_metrics/QC_metrics_main.sh"
#For QC per exon and per gene per sample
export QC_EXON_GENE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/QC_exon_gene/QC_exon_gene_main.sh"
#For indel analysis
export INDEL_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/indelSNP/indelSNP_main.sh"
export INDEL_POST_PROCESSING_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh"
#For structural variant analysis
#GRIDSS identifies the breakpoints for SV events but does not specify explicitly what is is (might be able to refer from ref and alt columns)
export GRIDSS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/gridss/gridss_main.sh"
#For tandem dup information
export PINDEL_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/pindel/pindel_main.sh"
#For Msings
export MSINGS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/msings/msings_main.sh"
#To COMBINE all outputs - Remember, if there are any changes in outputs from above programs, changes need to be made in this script too!
export COMBINE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/combine_results_per_sample/combine_results_per_sample_main.py"

#paired/QC_metrics/QC_metrics_main.sh 
#-------------------------------------------------------------------------------
export QC_METRICS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/QC_metrics/qc_metrics_postprocess.py"

#paired/QC_exon_gene/QC_exon_gene_main.sh 
#---------------------------------------------------------------------------------------
export QC_PER_EXON_GENE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/QC_exon_gene/fit_per_exon_gene.py"
export MERGE_FILE="/mnt/disk4/labs/salipante/nithisha/CP/paired/QC_exon_gene/coverage_merge.py"      

#paired/indelSNP/indelSNP_main.sh 
#-----------------------------------------------------------------------
export VARSCAN="/mnt/disk4/labs/salipante/nithisha/programs/VarScan.v2.3.9.jar"
export VARDICT="/mnt/disk4/labs/salipante/nithisha/programs/VarDictJava/build/install/VarDict/bin/VarDict"
export VARDICT_SCRIPTS_DIR="/mnt/disk4/labs/salipante/nithisha/programs/VarDictJava/build/install/VarDict/bin"
#paired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh 
#-----------------------------------------------------------------------------------------------------------------------------------------
export PYTHON_MERGE="/mnt/disk4/labs/salipante/nithisha/CP/paired/indelSNP/indelSNP_postprocess/merge_varscan_vardict_python.py"
export INDEL_FINAL_POST_PROCESSING="/mnt/disk4/labs/salipante/nithisha/CP/paired/indelSNP/indelSNP_postprocess/merge_annovar_python.py"
export INDEL_FILTER_EXPLANATION_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/indelSNP/indelSNP_postprocess/explain_vardict_filters.py"

#paired/gridss/gridss_main.sh 
#----------------------------------------------------------------
export GRIDSS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/gridss/gridss_postprocess.py"

#paired/pindel/pindel_main.sh 
#--------------------------------------------------------------
export PINDEL_POST_ANALYIS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/pindel/pindel_custom_annotate.py"
export PINDEL_MERGE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/pindel/merge_custom_AnnotSV.py" 
#paired/pindel/pindel_custom_annotate.py AND unpaired/pindel/pindel_custom_annotate.py
#-------------------------------------------------------------------------------------
#sys.path.append("/mnt/disk4/labs/salipante/nithisha/CP/paired/annotation")

#paired/msings/msings_main.sh 
#----------------------------------------------------------------
export MSINGS_POSTPROCESS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/paired/msings/msings_postprocess.py"  

#paired/combine_results_per_sample/combine_results_per_sample_main.py 
#-----------------------------------------------------------------------------------------------------------------------------------------------
#ensure that columns for indelsnp and gridss to be formatted are right here 

