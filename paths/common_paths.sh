set -e -x
#paths needed for CP_wrapper, paired pipeline and unpaired pipeline
RUN=$1
RUN_TYPE=$2

#CP_WRAPPER
#-----------
####assay specific input files needed for pipeline to run####
if [ $RUN_TYPE == "BROCA_MY" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_MY_v2.txt"
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/BROCA_MY_targets_v2.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/BROCA_MY_baits_v2.bed"

elif [ $RUN_TYPE == "BROCA_GO" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_GO.txt" #contains genes in our assay, format for tab-delimited file is Gene|Chr|Partition start|partition end|partition refseq, chr has no prefix
        #the entire pipeline including cnvkit uses 0 based bed file
        #export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/BROCA_GO_targets.bed" #NO CHR PREFIX #ZERO BASED #target regions
	export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/BROCA_GO_targets_QC-version.bed" #this includes exons and introns for some genes like BRCA1
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/BROCA_GO_baits.bed" #NO CHR PREFIX #ZERO BASED #genes only for QC #probe regions

elif [ $RUN_TYPE == "HRV1" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_HRv1.txt"
	#lab could not find target or bait file for HRV1 so using files for HRV3
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv3_baits.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv3_baits.bed"


elif [ $RUN_TYPE == "HRV3" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_HRv3.txt"
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv3_baits.bed"
	#lab does not have targets file for HRV3 so using baits file as target bedfile as well
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv3_baits.bed" 

elif [ $RUN_TYPE == "HRV6" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_HRv6.txt"
	#could only find baits file so using that as target file as well
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv6_targets.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv6_baits.bed"

elif [ $RUN_TYPE == "HRV7" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_HRv7.txt"
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv7_targets.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv7_baits.bed"


elif [ $RUN_TYPE == "HRV8" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/V2_BROCA_Genes_HRv8.txt"
        #the entire pipeline including cnvkit uses 0 based bed file
        #For HR version, both the bedfile and baits file are the same, we have only the baits file available
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/V2_BROCA_HRv8_baits_0.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/V2_BROCA_HRv8_baits_0.bed"

elif [ $RUN_TYPE == "HRV9" ];then
        export BROCA_GENES="/mnt/disk4/labs/salipante/nithisha/programs/broca_genes/$RUN_TYPE/BROCA_Genes_HRv9.txt"
        export BEDFILE_0B="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv9_targets.bed"
        export BEDFILE_0B_BAITS="/mnt/disk4/labs/salipante/nithisha/programs/bed_file/$RUN_TYPE/HRv9_baits.bed"

else
        echo "Error:ARE YOU RUNNING BROCA OR HR VERSION? PLEASE CHECK"
        exit
fi


export DATAPATH="/mnt/labs/swisher/Swisher_MH_data/$RUN" #where the fastq files are found
#export DATAPATH="/mnt/disk4/labs/salipante/data/external_runs/Swisher_MH_data/PureCN_tumors/fastq/$RUN"

export LIST_OF_SAMPLES="/mnt/disk4/labs/salipante/nithisha/MH_file_lists/$RUN.txt" #list of samples in run, one unpaired/paired sample  per row, separate paired with |
export LIST_OF_ORDERED_SAMPLES="/mnt/disk4/labs/salipante/nithisha/MH_file_lists/${RUN}_col_order.txt" #order of samples we want in final output, one sample per row
export CORES=4
export RUN_ONCE_PER_RUN_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/run_once_per_run/run_once_per_run_main.sh" #run these scripts once per run
export PATH_TO_CP_PAIRED_PIPELINE="/mnt/disk4/labs/salipante/nithisha/CP/paired/CP_PAIRED_pipeline.sh"
export PATH_TO_CP_UNPAIRED_PIPELINE="/mnt/disk4/labs/salipante/nithisha/CP/unpaired/CP_UNPAIRED_pipeline.sh"
export COMBINE_FINAL_OUTPUT_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/combined_run_main.sh"  
export RUN_LOG_DIR="/mnt/disk4/labs/salipante/nithisha/CP/${RUN}_logs" #dir where logs for run are stored
export SAVEROOT="/mnt/disk4/labs/salipante/nithisha/CP/${RUN}_results" #dir where results for a run are stored
export RUN_RESULTS=$SAVEROOT/${RUN}_final_output #this is the main dir with results that has to be exported for lab analysis

#run_once_per_run/run_once_per_run_main.sh 
#--------------------------------------------
#these are the files need to produce exon and gene bedfiles as well as preferred transcripts and genes
#preferred transcripts go in as txFile and is used in Pindel and Gridss
#exon and gene bedfiles are used in QC to generate coverage stats per exon/gene
#$BROCA_GENE
export REFSEQ="/mnt/disk4/labs/salipante/nithisha/programs/refSeq_UCSC_table_browser/refseq.txt"
export GENERATE_RUN_INFO="/mnt/disk4/labs/salipante/nithisha/CP/run_once_per_run/generate_run_info.sh"
export QC_EXON_CREATE_BEDFILE="/mnt/disk4/labs/salipante/nithisha/CP/run_once_per_run/create_exon_bedfile/create_exon_bedfile_main.py"
export QC_GENE_CREATE_BEDFILE="/mnt/disk4/labs/salipante/nithisha/CP/run_once_per_run/create_gene_bedfile/create_gene_bedfile_main.py"
export CREATE_ANNOTSV_FILES="/mnt/disk4/labs/salipante/nithisha/CP/run_once_per_run/createAnnotSVfiles.py"


#paired/CP_PAIRED_pipeline.sh AND unpaired/CP_UNPAIRED_pipeline.sh
#--------------------------------------------
export BBMERGE="/mnt/disk4/labs/salipante/nithisha/programs/bbmap/bbmerge.sh"
export CUTADAPT="/home/local/AMC/nithisha/.local/bin/cutadapt"
export BWA="/mnt/disk4/labs/salipante/nithisha/programs/bwa-0.7.17/bwa"
export REF_GENOME_b37="/mnt/disk4/labs/salipante/nithisha/programs/ref_genome/b37/human_g1k_v37.fasta" #NO CHR PREFIX (hg19 has chr prefix) #versions available b37/hg19
export SAMTOOLS="/mnt/disk4/labs/salipante/nithisha/programs/samtools-1.9/samtools"
export PICARD="/mnt/disk4/labs/salipante/nithisha/programs/picard/build/libs/picard.jar"
export GATK="/mnt/disk4/labs/salipante/nithisha/programs/gatk-4.0.8.1/gatk"
#$BEDFILE_0B
export VARIANT_FILES_b37="/mnt/disk4/labs/salipante/nithisha/programs/known_variant_files/b37" #known variant files, versions available b37/hg19

#paired/QC_exon_gene/QC_exon_gene_main.sh AND unpaired/QC_exon_gene/QC_exon_gene_main.sh
#--------------------------------------------
export BEDTOOLS="/mnt/disk4/labs/salipante/nithisha/programs/bedtools2/bin/bedtools"

#paired/indelSNP/indelSNP_main.sh and unpaired/indelSNP/indelSNP_main.sh
#--------------------------------------------
export VARSCAN="/mnt/disk4/labs/salipante/nithisha/programs/VarScan.v2.3.9.jar"
export VARDICT="/mnt/disk4/labs/salipante/nithisha/programs/VarDictJava/build/install/VarDict/bin/VarDict"
#paired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh and unpaired/indelSNP/indelSNP_postprocess/indelSNP_postprocess_main.sh
#--------------------------------------------
export PROGRAM_PATH="/mnt/disk4/labs/salipante/nithisha/programs" #where to find prrograms
export BRCA1_STARITA_PRED="/mnt/disk4/labs/salipante/nithisha/programs/swisher_favs_indelsnps/CustomColumn_BRCA1_Starita_predictions.txt" #lab specific files, indelsnps
export MISSENSES_FAVES="/mnt/disk4/labs/salipante/nithisha/programs/swisher_favs_indelsnps/CustomColumn_Missenses_Faves_V2.txt" #lab specific files, indelsnps
export SGE_RAD51C="/mnt/disk4/labs/salipante/nithisha/programs/swisher_favs_indelsnps/sge_rad51c.txt" #lab specific files, indelsnps

#paired/gridss/gridss_main.sh and unpaired/gridss/gridss_main.sh
#--------------------------------------------
export GRIDSS="/mnt/disk4/labs/salipante/nithisha/programs/gridss/gridss-2.0.0-gridss-jar-with-dependencies.jar"

#paired/pindel/pindel_main.sh and unpaired/pindel/pindel_main.sh
#--------------------------------------------
export PINDEL="/mnt/disk4/labs/salipante/nithisha/programs/pindel" #this is a directory
export BCFTOOLS="/mnt/disk4/labs/salipante/nithisha/programs/bcftools-1.9/bcftools"

#paired/msings/msings_main.sh and unpaired/msings/msings_main.sh
#--------------------------------------------
if [ $RUN_TYPE == "BROCA_GO" ];then
	export MSINGS="/mnt/disk4/labs/salipante/nithisha/programs/msings/scripts/run_msings.sh"
	export MSINGS_BEDFILE="/mnt/disk4/labs/salipante/nithisha/programs/msings/msings_required_inputs/broca_go/broca_go_MSI_loci.bed"
	export MSINGS_BASELINE="/mnt/disk4/labs/salipante/nithisha/programs/msings/msings_required_inputs/broca_go/BROCA_GO_MSI_BASELINE4.txt"

elif [ $RUN_TYPE == "BROCA_MY" ];then
	export MSINGS="/mnt/disk4/labs/salipante/nithisha/programs/msings/scripts/skip_msings.sh"
	#files below are dummy files, broca_my does not need msi to run
	export MSINGS_BEDFILE="/mnt/disk4/labs/salipante/nithisha/programs/msings/msings_required_inputs/broca_my/broca_my_MSI_loci.bed"
        export MSINGS_BASELINE="/mnt/disk4/labs/salipante/nithisha/programs/msings/msings_required_inputs/broca_my/BROCA_MY_MSI_BASELINE.txt"

elif [ $RUN_TYPE == "HRV9" ];then
	export MSINGS="/mnt/disk4/labs/salipante/nithisha/programs/msings/scripts/run_msings.sh"
	export MSINGS_BASELINE="/mnt/disk4/labs/salipante/nithisha/programs/msings/msings_required_inputs/HRV9/HRV9_MSI_BASELINE.txt"
fi

##paths specific to paired/unpaired pipelines will come from paired_paths.sh or unpaired_paths.sh respectively


#combined_run/combined_run_main.sh
#----------------------------------
export COMBINE_QC_METRICS="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/QC_Metrics/QC_Metrics_main.py"
export COMBINE_QC_EXONS="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/QC_exons/QC_exons_main.py"
export COMBINE_QC_GENES="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/QC_genes/QC_genes_main.py"
export COMBINE_INDELSNPS="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/indelSNP/indelSNP_main.py"
export COMBINE_GRIDSS="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/gridss/gridss_main.py"
export COMBINE_PINDEL="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/pindel/pindel_main.py"
export COMBINE_MSINGS="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/msings/msings_main.py"
export FINAL_OUTPUT_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/combined_run/final_output.py"      

#cnvkit/CNV_MAIN.sh
#-------------------
export CNVKIT="/mnt/disk4/labs/salipante/nithisha/programs/cnvkit-0.9.8" #imp to keep local copy for other files needed for cnvkit to run (like access-5k-mappable.grch37.bed)
#export CNVKIT="/mnt/disk4/labs/salipante/nithisha/programs/cnvkit-0.9.5"
#we use $BEDFILE_0B for cnvkit
#For gene and exon breakdown
export EXON_BREAKDOWN="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/exon_breakdown/cnv_exon_breakdown_mainfunc.py"
export GENE_BREAKDOWN="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/gene_breakdown/cnv_gene_breakdown_mainfunc.py"
export QC_EXON_RANGES="$SAVEROOT/QC_bedfile_exons.bed"
export QC_GENE_RANGES="$SAVEROOT/QC_bedfile_genes.bed"
#To plot graphs
#1 is normal diploid state, .5 is loss of a copy and 1.5 is gain of a copy
export PLOT_R_SUBFUNCS="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/plot_graphs/cnvkitPlotSubFuncs.R"
export PLOT_R_MAINFUNC="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/plot_graphs/cnvkitPlotMainFunc.R"
#To calculate z scores
export CAL_Z_SCORE_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/calculate_z_scores.py"
export COMBINE_RESULTS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/combine_all.py"
#To convert all .txt files to .xlsx files so that headers can be seen via formatting
export FORMAT_OUTPUTS_SCRIPT="/mnt/disk4/labs/salipante/nithisha/CP/cnvkit/format.py"    
