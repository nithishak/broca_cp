#Format and change all .txt files in CNVKIT dir to .xlsx files such that headers can be clearly seen!
import os
import glob
import pandas as pd
import sys

CNVKIT_OUTPUT = sys.argv[1]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 2 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#Dir check
if os.path.exists(CNVKIT_OUTPUT) == False:
        print "ERROR:One of required directories not present"
        sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Point to right dir
os.chdir(CNVKIT_OUTPUT)

def get_txt_files ():
	#get all the .txt files in CNVKIT dir
	extension = 'txt'
	all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
	
	return all_filenames

#---------------------------------------------------------------------------------------------------------------------------
#Inner func
def apply_formatting(dataframe,writer,worksheet_name,workbook):
       worksheet = writer.sheets[worksheet_name]

       format1 = workbook.add_format({'align': 'left'})

       #To see headers clearly without them being condensed
       def get_col_widths(dataframe):
               # Concatenate this to the lengths of column name
                return [(len(x)+3) for x in dataframe.columns]

       for i, width in enumerate(get_col_widths(dataframe)):
               worksheet.set_column(i, i, width,format1)
#----------------------------------------------------------------------------------------------------------------------------
def format_files (all_filenames):
	for f in all_filenames:
		df = pd.read_csv(f, sep= "\t", header = 0, index_col = None)

		# Create a Pandas Excel writer using XlsxWriter as the engine.
		OUTPUT_FILE  = f.replace (".txt",".xlsx")
        	writer = pd.ExcelWriter(OUTPUT_FILE, engine='xlsxwriter',options={'strings_to_numbers': True})

		# Convert the dataframe to an XlsxWriter Excel object.
		if "exon" in f:
			worksheet_name = "cnvkit_exon_analysis"
		elif "gene" in f:
			worksheet_name = "cnvkit_gene_analysis"
		else:
			worksheet_name  = "cnvkit_analysis"
		
		print worksheet_name
		
		df.to_excel(writer, sheet_name = worksheet_name, index = False)
	
		# Get the xlsxwriter objects from the dataframe writer object.	
		workbook  = writer.book

        	apply_formatting (df,writer,worksheet_name,workbook)
	
		# Close the Pandas Excel writer and output the Excel file.
		writer.save()


#Main code
files_list  = get_txt_files ()
format_files (files_list)
