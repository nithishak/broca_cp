#This script reads cnvkit output (.cnr) and merges data from baits that are from the same exon into segments. For each exon, the average log ratio is converted to normalized cnv ratio (already divided by diploid no 2). Then, the average standard deviation (the spread of reads for a single exon) is calculated using the ave log ratios. Lastly, the weights given by cnr are summed instead of averaged (refer to link below). We do not know what the weights indicate precisely, we are only keeping it to see if it has any relation to ave log ratios/normalized cnv ratios. Might be deleted in future runs.
#Post processing will be done on this file to find a z score, and in turn, a p value for each exon. Mean of all germline samples for that exon will be used for this purpose.
#Note: If an exon does not have any reads, the last few cols will all read 0 but segment start and end have been designed to reflect actual exon starts and stops FYI.

import math
import pandas as pd

#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#write a line to the final output
segmentedoutputfile = ""
def write_to_output(prefix,savepath):
	global segmentedoutputfile
	segmentedoutputfile = segmentedoutputfile +  savepath + "/" + prefix + ".cnvkit_exon_analysis.txt"
	#print outputfile
	with open(segmentedoutputfile, "w") as output:
		output.write("Position	Segment_Position	Gene	Exon_Number	Ave_Log_Ratio	Ave_Normalized_CNV_Ratio	Ave_Stdev	Sum_weights\n")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
#We have a file with exon positions associated with each gene SORTED alphabetically AND by strand direction (from QC) for preferred transcript [chrx	100	200	gene	exon_no] - chrs may NOT be in order.
#we can now merge the baits overlapping these exon positions into single entries - one segment per exon
#make a pass through the CNV data for EACH EXON we are interested in
def main_func(exonpositions,cnr_datafile): #datafile here is cnr file
	with open(exonpositions, "r") as exonpositionsfile:
		for exon in exonpositionsfile: #exonpositions is a file that has been previously generated from refseq for genes that are in BROCAassay for preferred transcript
			#extract the chromosome, start and stop position of the exon
			chromosome = exon.split("\t")[0] #does not have chr prefix
			exonstart = exon.split("\t")[1]
			exonstop = exon.split("\t")[2]
			gene = exon.split("\t")[3]
			exonNo = exon.rstrip("\n").split("\t")[4]

			#set segment start and end ridiculously high and low respectively so that we can overwrite them with our first logical operator
			segment_start = 10000000000000000000000000
			segment_end = -10000000000000000000000000
	
			segment_log2ratio = []
			segment_weights= []

			#CALL INNER FUNCTIONS
			baits_to_segments(cnr_datafile,chromosome, exonstart,exonstop,segment_start,segment_end,gene,exonNo,segment_log2ratio,segment_weights) 
			#Calling this function gives you a)segment_log2ratio LIST b)segment_weights LIST, c)segment_start VALUE b)segment_end VALUE

#---------------------------------------------------------------------------------------------------------------------------------------	
#INNER FUNC
#opens up the cnvkit output (cnr file) and goes through line by line (Excluding header) to merge baits into segments for EACH exon
def baits_to_segments(cnr_datafile,chromosome, exonstart,exonstop,segment_start,segment_end,gene,exonNo,segment_log2ratio,segment_weights):
	with open (cnr_datafile, "r") as cnr_input: #make sure cnr file does NOT have chr prefix
		header = cnr_input.readline()
		for cnr_line in cnr_input:
			cnr_line = cnr_line.rstrip("\n")
			cnr_linearray = cnr_line.split("\t")

			cnr_segment_start = int(cnr_linearray[1]) 
			cnr_segment_stop = int(cnr_linearray[2]) 

			cnr_log2ratio = float(cnr_linearray[5])
                        cnr_weight = float(cnr_linearray[6])

			exonstart = int(exonstart) #actual exon start
			exonstop = int(exonstop) #actual exon stop
			

			#CASE1 : if start of cnr segment is within exon
			if str(chromosome) == cnr_linearray[0] and cnr_segment_start >= exonstart and cnr_segment_start <= exonstop:
				if cnr_segment_start < segment_start:
					segment_start = cnr_segment_start
				if cnr_segment_stop > segment_end:
					segment_end = cnr_segment_stop
				
				segment_log2ratio.append(cnr_log2ratio)
				segment_weights.append(cnr_weight)

			#CASE2: if stop of cnr segment is within exon
			elif str(chromosome) == cnr_linearray[0] and cnr_segment_stop >= exonstart and cnr_segment_stop <= exonstop:
				if cnr_segment_start < segment_start:
					segment_start = cnr_segment_start
				if cnr_segment_stop > segment_end:
					segment_end = cnr_segment_stop

				segment_log2ratio.append(cnr_log2ratio)
                                segment_weights.append(cnr_weight)

			#CASE3: if the entire exon in within the cnr segment
			elif str(chromosome) == cnr_linearray[0] and cnr_segment_start < exonstart and cnr_segment_stop > exonstop:
				if cnr_segment_start < segment_start:
					segment_start = cnr_segment_start
				if cnr_segment_stop > segment_end:
					segment_end = cnr_segment_stop

				segment_log2ratio.append(cnr_log2ratio)
                                segment_weights.append(cnr_weight)

			#CASE4: if entire cnr segment is within exon
				#In this case, the segment first satisfies and passes through Case 1 if and so it gets appended anyway.
			

	#CALL ANOTHER INNER FUNCTION
	segment_summary_statistics(chromosome,exonstart,exonstop,segment_start,segment_end,gene,exonNo,segment_log2ratio,segment_weights)
	
#------------------------------------------------------------------------------------------------------------------------------------------------------------------
#INNER FUNC
#Now, we have passed through the entire CNV file.
#For each exon, calulate summary statistics for merged bins
def segment_summary_statistics(chromosome,exonstart,exonstop,segment_start,segment_end,gene,exonNo,segment_log2ratio,segment_weights):
	if len(segment_log2ratio) != 0: 
		avelogratio = average(segment_log2ratio)
		aveweights = sum(segment_weights)  
		stdeva = stdev(segment_log2ratio) 

		segment_average_normalized_cnv_ratio = str(round((2**(float(avelogratio))),2)) 
		
	else: #if length of segment_log2ratio is 0 meaning that NO bins from cnr matched that exon
		segment_start = exonstart
		segment_end = exonstop
		avelogratio = str(0)
		stdeva = str(0)
		aveweights = str(0)

		segment_average_normalized_cnv_ratio = str(0)

	#create string
	position = "chr" + chromosome + ":" + str(exonstart) + "-" + str(exonstop)
	segment_position = "chr" + chromosome + ":" + str(segment_start) + "-" + str(segment_end)
	string = ("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" %(position,segment_position,gene,exonNo,str(avelogratio),str(segment_average_normalized_cnv_ratio),str(stdeva),str(aveweights)))
	
	#save to file
	with open (segmentedoutputfile, "a+") as output:
		output.write(string)	
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------------------------
#INNER FUNC
def average(segmentlog2ratioarray):
	total = 0
	for ratio in segmentlog2ratioarray:
		total = total + ratio
	average = total/len(segmentlog2ratioarray)
	return average
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#INNER FUNC
def stdev(segmentlog2ratioarray): 
	if len(segmentlog2ratioarray) == 1:
		return 0
	else:
		averagee = average(segmentlog2ratioarray)
		sqtotal = 0
		for ratio in segmentlog2ratioarray:
			sqtotal = sqtotal + (averagee - ratio) **2
		std = sqtotal/ (len(segmentlog2ratioarray)) ** 0.5
		return std
#---------------------------------------------------------------------------------------------------------------------------------------------------------------

