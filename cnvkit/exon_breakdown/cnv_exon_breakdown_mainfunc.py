import sys
import os 
import cnv_exon_breakdown_subfuncs as c 

#Read in args
prefix = sys.argv[1]
savepath = sys.argv[2]
qc_exon_positions_file = sys.argv[3]
cnr_file = sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[qc_exon_positions_file,cnr_file]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)


#Dir check
        if os.path.exists(savepath) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Main function
c.write_to_output(prefix,savepath)
exon_breakdown = c.main_func(qc_exon_positions_file,cnr_file)
