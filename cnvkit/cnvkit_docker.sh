#!/bin/bash
############################
#CNVKIT - this is to find CNV (here we use both t and g samples)
############################
#Summary
#First CNV_BAMS is filled with copied bams and bais but deleted, all intermediate outputs found in CNV_T_RESULTS and CNV_G_RESULTS while final outputs can be found in CNVKIT_OUTPUT.
#YOU CAN ONLY RUN THIS PIPELINE AFTER CP_PAIRED_pipeline.sh has run on ALL files as bam files are needed for all of the tumor-germline pairs.
#At the very least, run CP_PAIRED_pipeline box 1 on all pairs and then send output to this script
#Needed files- 1) covered sites bed files 2) an access bed file (accessible sites) - use the file provided (access-5k-mappable.hg19.bed), 5k better than 10k, window of bases, lower the no, better the granularity:#https://cnvkit.readthedocs.io/en/stable/
#Expected output- [target.bed, antitarget.bed, germline.targettcoverage.cnn, germline.antitargetcoverage.cnn, tumor.targetcoverage.cnn, tumor.antitargetcoverage.cnn, tumor.cnr,tumor.cns, tumor]
#the .cns file is then converted to .vcf file [bqsr_tumor.vcf file created]
#.cnr has several values ------> in the.cns file, it groups regions into segments- we want to look at cnr file to a)perform breakdown by exons b)perform breakdown by genes 3)create plots per gene for tumor genes AND germline genes
#cnr file may break down a capture region/row from bedfile into even smaller bins! take note.
#Looks like weight is dependent on a)length of bin, shorter, lesser weight b)read depth, lesser, lesser weight. Weights are SUMMED over a segment made from bins, not average!!
#------------------------------------------------------------------------------------------------------------------------------------------------------
whole_command_tumors () {
        #Tumors are compared against reference, results saved in CNV_T_RESULTS [target and anti-target bed file per run, .cnn, .cnr, .cns, scatter.pdf, diagram.pdf formed PER tumor]
        #Make sure Rscript points to right R which has DNAcopy loaded, if you want to use another R version, point to it using --rscript-path or else cns is not produced here and causes probs for seg step downstream for pureCN
       
        cnvkit.py batch $CNV_BAMS/*_T.bam \
        --normal $CNV_BAMS/*_G.bam \
        --targets $BEDFILE_0B_BAITS \
        --fasta $REF_GENOME_b37 \
        --access $CNVKIT/data/access-5k-mappable.grch37.bed \
        --output-reference $CNV_BAMS/my_reference.cnn \
        --output-dir $CNV_T_RESULTS \
        --diagram \
        --scatter \
        -p 8 
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
whole_command_germlines () {
        #Germlines are compared against reference, results saved in CNV_G_RESULTS [same files produded as above]
        #Make sure Rscript points to right R which has DNAcopy loaded, if you want to use another R version, point to it using --rscript-path or else cns is not produced here and causes probs for seg step downstream for pureCN

        cnvkit.py batch $CNV_BAMS/*_G.bam \
        -r  $CNV_BAMS/my_reference.cnn \
        -p 8 \
        --scatter \
        --diagram \
        -d $CNV_G_RESULTS
}

#------------------------------------------------------------------------------------------------------------------------------------------------------

#Main functions
>$CNVKIT_OUTPUT/cnvkit_version.txt
CNVKIT_VERSION=$(cnvkit.py version)
echo "CNVKIT_VERSION = $CNVKIT_VERSION" >> $CNVKIT_OUTPUT/cnvkit_version.txt 
echo "TUMOR ANALYSIS"
whole_command_tumors
echo "GERMLINE ANALYSIS"
whole_command_germlines
echo "ALL DONE"
