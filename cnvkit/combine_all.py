import sys
import os
import glob
import pandas as pd
import re #for sorting function

MH_RUN = sys.argv[1]
CNVKIT_OUTPUT =  sys.argv[2] #/results/CNVKIT_OUTPUT
ORDERED_COLS =  sys.argv[3] #file containing right order of samples in run

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 4 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[ORDERED_COLS]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)

#Dir check
if os.path.exists(CNVKIT_OUTPUT) == False:
        print "ERROR:One of required directories not present"
        sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Point to right dir
os.chdir(CNVKIT_OUTPUT)

def sample_order (ORDERED_COLS):
	with open (ORDERED_COLS, 'r+') as ordered_col_file:
		ordered_col_list = [line.rstrip("\r\n") for line in ordered_col_file]
	return ordered_col_list


def combine_exons (ordered_sample_list):
	#Create a new df with relevant information, pick ANY exon analysis file to populate final df with the cols that are common for all the samples
	template_file = ordered_sample_list[0] + ".cnvkit_exon_analysis.txt"
	combined_df_exon = pd.read_csv(template_file, sep = "\t", header = 0 , usecols = ["Position","Gene","Exon_Number"])

	#For normalized cnv ratios
	for sample in ordered_sample_list:
		file_name =  sample + ".cnvkit_exon_analysis.txt"
		sample_df = pd.read_csv(file_name, sep = "\t", header = 0)
		col_name = sample + "(_N_CNV Ratio)"
		combined_df_exon[col_name] = sample_df["Ave_Normalized_CNV_Ratio"]
	#For p values
	for sample in ordered_sample_list:
        	file_name =  sample + ".cnvkit_exon_analysis.txt"
        	sample_df = pd.read_csv(file_name, sep = "\t", header = 0)
        	col_name = sample + "(_P_VAL)"
        	combined_df_exon[col_name] = sample_df["p_value"]
	
	#Save to file
        output_name = MH_RUN + "_combined_exon.txt"
        combined_df_exon.to_csv(output_name, sep="\t", index=False)

def combine_genes (ordered_sample_list):
	#Create a new df with relevant information
        template_file = ordered_sample_list[0] + ".cnvkit_gene_analysis.txt"
        combined_df_gene = pd.read_csv(template_file, sep = "\t", header = 0 , usecols = ["Position","Gene"])

	#For normalized cnv ratios
	for sample in ordered_sample_list:
        	file_name =  sample + ".cnvkit_gene_analysis.txt"
        	sample_df = pd.read_csv(file_name, sep = "\t", header = 0)
        	col_name = sample + "(_N_CNV Ratio)"
        	combined_df_gene[col_name] = sample_df["Ave_Normalized_CNV_Ratio"]	
	#For p values
        for sample in ordered_sample_list:
                file_name =  sample + ".cnvkit_exon_analysis.txt"
                sample_df = pd.read_csv(file_name, sep = "\t", header = 0)
                col_name = sample + "(_P_VAL)"
                combined_df_gene[col_name] = sample_df["p_value"]

	#Save to file
	output_name = MH_RUN + "_combined_gene.txt"
	combined_df_gene.to_csv(output_name, sep="\t", index=False)


#Main code
ordered_sample_list = sample_order (ORDERED_COLS)
combine_exons(ordered_sample_list)
combine_genes(ordered_sample_list)
