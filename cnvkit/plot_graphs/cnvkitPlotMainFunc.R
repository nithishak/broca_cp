#Read in files and extract info PER GENE
args <- commandArgs(trailing = TRUE)

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#File check
files <- list(args[1],args[2],args[3],args[4])
for (filee in files) {
	if (!file.exists(filee)){
		print ("ERROR:Not all required files are present")
		quit()
	}
}
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#Run sub-funcs first before running this script
source (args[1])

broca.gene <- read.csv(args[2], header = TRUE, sep = "\t")
sorted.broca.gene <- broca.gene[order(broca.gene[,1]),] 
genes <- as.vector(sorted.broca.gene[['Gene']]) 
preferred.transcripts.temp <- as.vector(sorted.broca.gene[['partition.refseq']]) 
preferred.transcripts <- gsub("\\..*", "", preferred.transcripts.temp)

refseq <- read.csv(args[3], header = TRUE, sep = "\t")
cnr = read.csv(args[4], sep = "\t")
prefix.name <- gsub(pattern = "\\.cnr$", "", basename(args[4]))
savepath <- dirname(args[4])

#--------------------------------------------------------------------------------------------------------------------
#MAIN FUNC
#--------------------------------------------------------------------------------------------------------------------
file.name= paste(savepath,"/", prefix.name, ".pdf", sep="")
pdf(file= file.name, width=11, height=4)

#delete
stats.file <- "stats_without_chr_check.txt"

for (gene.index in (1:length(genes))) {
	print (genes[gene.index]) #print which gene we are working on
	#get capture region range for gene from bedfile
	x.range<- plot.x.range(genes[gene.index],broca.gene)
	#extract specific info - plot x start, plot x end
	plot.start <- as.numeric(unlist(x.range[1]))
	plot.end <- as.numeric(unlist(x.range[2]))
  
 	#get gene specific info - cd.start, cd.end, exon.starts, exon.ends, tx.start, tx.end
	gene.info <- find.gene.info(genes,gene.index, preferred.transcripts,refseq)
	#extract info
	chr <- unlist(gene.info[1]) #dont use as.numeric, chr X gives error
	tx.start <- as.numeric(unlist(gene.info[2]))
	tx.end <- as.numeric(unlist(gene.info[3]))
	cd.start <- as.numeric(unlist(gene.info[4]))
	cd.end <- as.numeric(unlist(gene.info[5]))
	exon.starts <- as.numeric(unlist(gene.info[6]))
  	exon.stops <- as.numeric(unlist(gene.info[7])) 
  
  
	#get cnr info for this gene - cnr.starts, cnr.starts, log2ratios
	gene.cnr<- find.cnr.rows(cnr,chr,plot.start,plot.end)
	#extract info
	cnr.starts <- as.numeric(unlist(gene.cnr[1])) 
	cnr.ends <-  as.numeric(unlist(gene.cnr[2])) 
	cnv.ratios <- as.numeric(unlist(gene.cnr[3]))
  
 	#find out max y value you want and round off to nearest .5 value, min y value is always 0
	max.y <- round((max(cnv.ratios))/0.5)*0.5 
 
	#-----------------------------------------------------------------------------
	#We now have all the information for a gene we need, so let us plot!
	plot_outline(gene.index,plot.start, plot.end,max.y)
 	plot.y.lines (max.y,plot.start,plot.end)
	plot.geneline.exons(max.y,exon.stops,exon.starts, cd.start, cd.end, tx.start,tx.end)
	plot.cnvs(cnr.starts,cnr.ends,cnv.ratios)
	#-----------------------------------------------------------------------------
}
dev.off()
