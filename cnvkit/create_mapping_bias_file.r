# speed-up future runtimes by pre-calculating variant mapping biases
library("PureCN")
normal.panel.vcf.file <- "/mnt/disk4/labs/salipante/nithisha/CP/LOH_test_results/cnvkit_G_results/pon.vcf.gz"
bias <- calculateMappingBiasVcf(normal.panel.vcf.file, genome = "h19")
saveRDS(bias, "/mnt/disk4/labs/salipante/nithisha/CP/LOH_test_results/cnvkit_G_results/mapping_bias.rds")
