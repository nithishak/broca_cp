set -x -e
export R_LIBS="/home/local/AMC/nithisha/R/R.3.3-library"

#Summary
#First CNV_BAMS is filled with copied bams and bais but deleted, all intermediate outputs found in CNV_T_RESULTS and CNV_G_RESULTS while final outputs can be found in CNVKIT_OUTPUT.
#YOU CAN ONLY RUN THIS PIPELINE AFTER CP_PAIRED_pipeline.sh has run on ALL files as bam files are needed for all of the tumor-germline pairs. 
#At the very least, run CP_PAIRED_pipeline box 1 on all pairs and then send output to this script
#Needed files- 1) covered sites bed files 2) an access bed file (accessible sites) - use the file provided (access-5k-mappable.hg19.bed), 5k better than 10k, window of bases, lower the no, better the granularity:#https://cnvkit.readthedocs.io/en/stable/
#Expected output- [target.bed, antitarget.bed, germline.targettcoverage.cnn, germline.antitargetcoverage.cnn, tumor.targetcoverage.cnn, tumor.antitargetcoverage.cnn, tumor.cnr,tumor.cns, tumor]
#the .cns file is then converted to .vcf file [bqsr_tumor.vcf file created]
#.cnr has several values, in the.cns file, it groups regions into segments- we want to look at cnr file to a)perform breakdown by exons b)perform breakdown by genes 3)create plots per gene for tumor genes AND germline genes
#cnr file may break down a capture region/row from bedfile into even smaller bins! take note.
#Looks like weight is dependent on a)length of bin, shorter, lesser weight b)read depth, lesser, lesser weight. Weights are SUMMED over a segment made from bins, not average!!

RUN=$1

#variables used
#SAVEROOT
#CNVKIT
#BEDFILE_1B #no chr prefix as refflat does not have chr prefix as well #USE 1BASED INDEXING: 0 based file would mean n-1 from start of bed file so that results will start from n inclusive. Cnvkit is doing n-1 to bed file provided so provide it with a 1 based bed file!
#REF_GENOME_b37
#LIST_OF_SAMPLES
#GATK

#For gene and exon breakdown
#EXON_BREAKDOWN
#GENE_BREAKDOWN
#QC_EXON_RANGES
#QC_GENE_RANGES

#To plot graphs
#1 is normal diploid state, .5 is loss of a copy and 1.5 is gain of a copy
#PLOT_R_SUBFUNCS
#PLOT_R_MAINFUNC
#BROCA_GENES
#bedfile already above
#REFSEQ
#cnr file, identify below

#To calculate z scores
#CAL_Z_SCORE_SCRIPT

#To produce combined outputs for exon and germline analysis
#COMBINE_RESULTS_SCRIPT
#LIST_OF_ORDERED_SAMPLES

#To convert all .txt files to .xlsx files so that headers can be seen via formatting
#FORMAT_OUTPUTS_SCRIPT

#------------------------------------------------------------------------------------------------------------------------------------------------------
#create subdirectory for cnvkit within results outside sample folders - bams FOR ALL samples from a run to be copied to this folder
CNV_BAMS=$SAVEROOT/CNVKIT_BAMS
mkdir -p $CNV_BAMS #"/mnt/disk4/labs/salipante/nithisha/CP/$RUN_results/CNVKIT_BAMS"

CNV_T_RESULTS=$SAVEROOT/cnvkit_T_results
mkdir -p $CNV_T_RESULTS

CNV_G_RESULTS=$SAVEROOT/cnvkit_G_results
mkdir -p $CNV_G_RESULTS

CNVKIT_OUTPUT=$SAVEROOT/${RUN}_final_output/CNVKIT_OUTPUT
mkdir -p $CNVKIT_OUTPUT

#------------------------------------------------------------------------------------------------------------------------------------------------------



#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 1 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BEDFILE_1B|$LIST_OF_SAMPLES|$GATK|$EXON_BREAKDOWN|$GENE_BREAKDOWN|$PLOT_R_SUBFUNCS|$PLOT_R_MAINFUNC|$BROCA_GENES|$REFSEQ|$CAL_Z_SCORE_SCRIPT|$COMBINE_RESULTS_SCRIPT|$FORMAT_OUTPUTS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$SAVEROOT|$CNVKIT|$CNV_BAMS|$CNV_T_RESULTS|$CNV_G_RESULTS"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#------------------------------------------------------------------------------------------------------------------------------------------------------
copy_files_cnvkit () {
	#Copy over all bam and bai files to CNV_BAMS folder
	#MAKE SURE at this point, CP wrapper checks has been run so that labv bams have been moved out of recal folders
	while IFS='' read -r line
        do
	TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line|awk -F'|' '{print $2}')

	if [ ! -z $TUMOR_PFX ];then
		#tumor from paired/unpaired sample (as long as tumor string is present)
		#For the purpose of CNVKIT, lets also make a copy of this bam in a dir called CNVKIT_BAMS and rename them

	        cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } #file may be _T or_UN # ls *<>* means the file must contain <>
	        cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}*.bai $CNV_BAMS/
	        mv  $CNV_BAMS/bqsr_${TUMOR_PFX}*.bam $CNV_BAMS/${TUMOR_PFX}_T.bam 
        	mv  $CNV_BAMS/bqsr_${TUMOR_PFX}*.bai $CNV_BAMS/${TUMOR_PFX}_T.bai 
		
	fi
	

	if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
		#germline from paired sample
		#For the purpose of CNVKIT, lets also make a copy of this bam in a dir called CNVKIT_BAMS and rename them
        	cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } ##file is paired and _G
		cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}*.bai $CNV_BAMS/
		mv $CNV_BAMS/bqsr_${GERMLINE_PFX}*.bam $CNV_BAMS/${GERMLINE_PFX}_G.bam 
		mv $CNV_BAMS/bqsr_${GERMLINE_PFX}*.bai $CNV_BAMS/${GERMLINE_PFX}_G.bai
	fi


	if [ -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
		#germline from unpaired sample
		cp $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } ##file is unpaired and _UN
		cp $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}*.bai $CNV_BAMS/
                mv $CNV_BAMS/bqsr_${GERMLINE_PFX}*.bam $CNV_BAMS/${GERMLINE_PFX}_G.bam
                mv $CNV_BAMS/bqsr_${GERMLINE_PFX}*.bai $CNV_BAMS/${GERMLINE_PFX}_G.bai
	fi
	

	done<$LIST_OF_SAMPLES

	
	#Make sure time stamps for index files are later than for bam files (when you copy over, bai files copy faster so timestamp may be earlier instead of later)
	touch -c $CNV_BAMS/*.bai
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
whole_command_tumors () {
	#Tumors are compared against reference, results saved in CNV_T_RESULTS [target and anti-target bed file per run, .cnn, .cnr, .cns, sactter.pdf, diagram.pdf formed PER tumor]
	$CNVKIT/cnvkit.py batch $CNV_BAMS/*_T.bam --normal $CNV_BAMS/*_G.bam --targets $BEDFILE_1B --fasta $REF_GENOME_b37 --access $CNVKIT/data/access-5k-mappable.grch37.bed --output-reference $CNV_BAMS/my_reference.cnn --output-dir $CNV_T_RESULTS --diagram --scatter -p 8
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
whole_command_germlines () {
	#Germlines are compared against reference, results saved in CNV_G_RESULTS
	$CNVKIT/cnvkit.py batch $CNV_BAMS/*_G.bam -r  $CNV_BAMS/my_reference.cnn -p 8 --scatter --diagram -d $CNV_G_RESULTS
}

#------------------------------------------------------------------------------------------------------------------------------------------------------
del_cnv_bams () {
	#Remember to delete all intermediate large files
	rm -r $CNV_BAMS
}
#------------------------------------------------------------------------------------------------------------------------------------------------------





#------------------------------------------------------------------------------------------------------------------------------------------------------
exon_gene_breakdown () {
	#Calulate per exon/per gene PER sample, results stored in CNV_T_RESULTS or CNV_G_RESULTS dirs

	JOBS_IN_BATCH=8
	count=0

	#Exon and gene breakdown
	while IFS='' read -r line
        do
        TUMOR_PFX=$(echo $line| awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')

        ################################For each tumor sample########################################
	if [ ! -z $TUMOR_PFX ];then
		TUMOR_CNR=$(ls $CNV_T_RESULTS/${TUMOR_PFX}_T.cnr)
	
		#breakdown by exon
		python $EXON_BREAKDOWN $TUMOR_PFX $CNV_T_RESULTS $QC_EXON_RANGES $TUMOR_CNR &
	
		#breakdown by gene
		python $GENE_BREAKDOWN $TUMOR_PFX $CNV_T_RESULTS $QC_GENE_RANGES $TUMOR_CNR &

		count=$((count+1))
	fi
	###############################For each germline sample#######################################
        if [ ! -z $GERMLINE_PFX ];then
		GERMLINE_CNR=$(ls $CNV_G_RESULTS/${GERMLINE_PFX}_G.cnr)
	
		#If statements because different tumors might have same germline sample
		if [ ! -f $CNV_G_RESULTS/$GERMLINE_PFX.cnvkit_exon_analysis.txt ];then
		#breakdown by exon
			python $EXON_BREAKDOWN $GERMLINE_PFX $CNV_G_RESULTS $QC_EXON_RANGES $GERMLINE_CNR &
		fi

		if [ ! -f $CNV_G_RESULTS/$GERMLINE_PFX.cnvkit_gene_analysis.txt ];then
			#breakdown by gene
        		python $GENE_BREAKDOWN $GERMLINE_PFX $CNV_G_RESULTS $QC_GENE_RANGES $GERMLINE_CNR &
		fi

		count=$((count+1))
	fi
	
	
	if [ $count -ge $JOBS_IN_BATCH ];then
        	wait
        	count=0
        fi
 	
	done<$LIST_OF_SAMPLES
}

#------------------------------------------------------------------------------------------------------------------------------------------------------
calculate_z_scores () {
	#Calculate z scores as a bearer of confidence for each call being made, results stored in CNVKIT_OUTPUT dir
        python $CAL_Z_SCORE_SCRIPT $SAVEROOT $CNVKIT_OUTPUT
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
plot_graphs () {
	#Plot graphs for each sample,results stored in CNV_T_RESULTS or CNV_G_RESULTS dirs
	
	JOBS_IN_BATCH=10
	count=0
	#Run R scripts to produce graphs
	Rscript $PLOT_R_SUBFUNCS 
	#Produce graphs for tumor-germline paired sample
	while IFS='' read -r line
	do
	TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')	

	################################For each tumor sample########################################
	if [ ! -z $TUMOR_PFX ];then
		TUMOR_CNR=$(ls $CNV_T_RESULTS/${TUMOR_PFX}_T.cnr)
		Rscript $PLOT_R_MAINFUNC $PLOT_R_SUBFUNCS $BROCA_GENES $BEDFILE_1B $REFSEQ $TUMOR_CNR &
		count=$((count+1))
	fi
	###############################For each germline sample#######################################
	if [ ! -z $GERMLINE_PFX ];then
		GERMLINE_CNR=$(ls $CNV_G_RESULTS/${GERMLINE_PFX}_G.cnr)
		#Dont repeat graph plot for a germline that is present for 2 or more tumor samples (TA, TB etc)
		if [ ! -f $CNV_G_RESULTS/${GERMLINE_PFX}_G.pdf ];then
			Rscript $PLOT_R_MAINFUNC $PLOT_R_SUBFUNCS $BROCA_GENES $BEDFILE_1B $REFSEQ $GERMLINE_CNR &
			count=$((count+1))
		fi
	fi	

        if [ $count -ge $JOBS_IN_BATCH ];then
        	wait
        	count=0
        fi
	
	done< $LIST_OF_SAMPLES
}

#------------------------------------------------------------------------------------------------------------------------------------------------------
compress_plots () {
	#Compress plots, results found in CNVKIT_OUTPUT DIR
	JOBS_IN_BATCH=20
        count=0

        while IFS='' read -r line
        do
	echo $line
        TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
        GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')

	################################For each tumor sample########################################
        if [ ! -z $TUMOR_PFX ];then
		echo "Compressing tumor file pdf for $TUMOR_PFX"
        	ps2pdf $CNV_T_RESULTS/${TUMOR_PFX}_T.pdf $CNVKIT_OUTPUT/${TUMOR_PFX}_T.pdf &
		count=$((count+1))
	fi
	###############################For each germline sample#######################################	
        if [ ! -z $GERMLINE_PFX ];then
		echo "Compressing germline file pdf for $GERMLINE_PFX"
		ps2pdf $CNV_G_RESULTS/${GERMLINE_PFX}_G.pdf $CNVKIT_OUTPUT/${GERMLINE_PFX}_G.pdf &
		count=$((count+1))
	fi

        if [ $count -ge $JOBS_IN_BATCH ];then
        	wait
        	count=0
        fi
        
	done< $LIST_OF_SAMPLES
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
merge_pfs_scatter_diagram () {
	#Merge scatter.pdfs for tumors and germlines respectively in CNV_T_RESULTS or CNV_G_RESULTS, but produce output in cnvkit final output dir(CNVKIT_OUTPUT) after compression
	gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_T_RESULTS/${RUN}_T_scatter.pdf -dBATCH $CNV_T_RESULTS/*-scatter.pdf
	gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_G_RESULTS/${RUN}_G_scatter.pdf -dBATCH $CNV_G_RESULTS/*-scatter.pdf
	
	ps2pdf  -dAutoRotatePages=/None $CNV_T_RESULTS/${RUN}_T_scatter.pdf $CNVKIT_OUTPUT/${RUN}_T_scatter.pdf
	ps2pdf  -dAutoRotatePages=/None $CNV_G_RESULTS/${RUN}_G_scatter.pdf $CNVKIT_OUTPUT/${RUN}_G_scatter.pdf

	#Merge diagram.pdfs for tumors and germlines respectively in CNV_T_RESULTS or CNV_G_RESULTS, but produce output in cnvkit final output dir(CNVKIT_OUTPUT) after compression
	gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_T_RESULTS/${RUN}_T_diagram.pdf -dBATCH $CNV_T_RESULTS/*-diagram.pdf
        gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_G_RESULTS/${RUN}_G_diagram.pdf -dBATCH $CNV_G_RESULTS/*-diagram.pdf

        ps2pdf  -dAutoRotatePages=/None $CNV_T_RESULTS/${RUN}_T_diagram.pdf $CNVKIT_OUTPUT/${RUN}_T_diagram.pdf
        ps2pdf  -dAutoRotatePages=/None $CNV_G_RESULTS/${RUN}_G_diagram.pdf $CNVKIT_OUTPUT/${RUN}_G_diagram.pdf
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
rm_files1 () {
	#Remove files that are not needed anymore in CNV_T_RESULTS and CNV_G_RESULTS dirs
	rm $CNV_G_RESULTS/*_G.pdf
	rm $CNV_G_RESULTS/*cnvkit_gene_analysis.txt
	rm $CNV_G_RESULTS/*.cnvkit_exon_analysis.txt

	rm $CNV_T_RESULTS/*_T.pdf
	rm $CNV_T_RESULTS/*cnvkit_gene_analysis.txt
	rm $CNV_T_RESULTS/*cnvkit_exon_analysis.txt     
	rm -r $CNV_T_RESULTS $CNV_G_RESULTS 
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
combine_results_exons_genes () {
        #produce a combined result for all samples in run for a)exon analysis b)gene analysis. result found in CNV_OUTPUT
        python $COMBINE_RESULTS_SCRIPT $RUN $CNVKIT_OUTPUT $LIST_OF_ORDERED_SAMPLES
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
format_output () {
	#Format output, result found in CNV_OUTPUT
	python $FORMAT_OUTPUTS_SCRIPT $CNVKIT_OUTPUT
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
rm_files2 () {
	#Remove unneeded files from main results dir - CNVKIT OUTPUT
	rm $CNVKIT_OUTPUT/*.txt
}
#------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code

cnvkit () {
	copy_files_cnvkit
      	whole_command_tumors
	whole_command_germlines
	#del_cnv_bams
}

cnvkit_postprocess () {
	exon_gene_breakdown
	calculate_z_scores
	plot_graphs
	compress_plots
	merge_pfs_scatter_diagram
	#rm_files1
	combine_results_exons_genes
	format_output
	#rm_files2
}

#Main code
cnvkit
cnvkit_postprocess
