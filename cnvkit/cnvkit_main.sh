set -x -e
RUN=$1
echo $RUN

#variables used
#SAVEROOT
#CNVKIT
#BEDFILE_0B_BAITS #no chr prefix as refflat does not have chr prefix as well 
#REF_GENOME_b37
#LIST_OF_SAMPLES
#GATK

#For gene and exon breakdown
#EXON_BREAKDOWN
#GENE_BREAKDOWN
#QC_EXON_RANGES
#QC_GENE_RANGES

#To plot graphs
#1 is normal diploid state, .5 is loss of a copy and 1.5 is gain of a copy
#PLOT_R_SUBFUNCS
#PLOT_R_MAINFUNC
#BROCA_GENES
#bedfile already above
#REFSEQ
#cnr file, identify below

#To calculate z scores
#CAL_Z_SCORE_SCRIPT

#To produce combined outputs for exon and germline analysis
#COMBINE_RESULTS_SCRIPT
#LIST_OF_ORDERED_SAMPLES

#To convert all .txt files to .xlsx files so that headers can be seen via formatting
#FORMAT_OUTPUTS_SCRIPT

#------------------------------------------------------------------------------------------------------------------------------------------------------
#create subdirectory for cnvkit within results outside sample folders - bams FOR ALL samples from a run to be copied to this folder
CNV_BAMS=$SAVEROOT/CNVKIT_BAMS
mkdir -p $CNV_BAMS #"/mnt/disk4/labs/salipante/nithisha/CP/$RUN_results/CNVKIT_BAMS"

CNV_T_RESULTS=$SAVEROOT/cnvkit_T_results
mkdir -p $CNV_T_RESULTS

CNV_G_RESULTS=$SAVEROOT/cnvkit_G_results
mkdir -p $CNV_G_RESULTS

CNVKIT_OUTPUT=$SAVEROOT/${RUN}_final_output/CNVKIT_OUTPUT
mkdir -p $CNVKIT_OUTPUT

#------------------------------------------------------------------------------------------------------------------------------------------------------



#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 1 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
#FILES="$BEDFILE_0B_BAITS|$LIST_OF_SAMPLES|$GATK|$EXON_BREAKDOWN|$GENE_BREAKDOWN|$PLOT_R_SUBFUNCS|$PLOT_R_MAINFUNC|$BROCA_GENES|$REFSEQ|$CAL_Z_SCORE_SCRIPT|$COMBINE_RESULTS_SCRIPT|$FORMAT_OUTPUTS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$SAVEROOT|$CNVKIT|$CNV_BAMS|$CNV_T_RESULTS|$CNV_G_RESULTS"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


############################
#COPY OVER BAMS
############################
#------------------------------------------------------------------------------------------------------------------------------------------------------
copy_and_rename_bams () {
	#Copy over all bam and bai files to CNV_BAMS folder
	#MAKE SURE at this point, CP wrapper checks has been run so that labv bams have been moved out of recal folders
	while IFS='' read -r line
        do
	TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line|awk -F'|' '{print $2}')

	if [ ! -z $TUMOR_PFX ];then
		#tumor from paired/unpaired sample (as long as tumor string is present)
		#For the purpose of CNVKIT, lets also make a copy of this bam in a dir called CNVKIT_BAMS and rename them
		#https://askubuntu.com/questions/225621/how-to-search-for-all-the-files-starting-with-the-name-abc-in-a-directory

		#first ensure that labv bam is not present (it may be if we did not run 2nd part of CP eg.)
		rm $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_*_labv.bam $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}*_labv.bai || true
		
		#now proceed to copy
	        cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } #file may be _T or_UN # ls *<>* means the file must contain <>
	        cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_*.bai $CNV_BAMS/
	        mv  $CNV_BAMS/bqsr_${TUMOR_PFX}_*.bam $CNV_BAMS/${TUMOR_PFX}_T.bam 
        	mv  $CNV_BAMS/bqsr_${TUMOR_PFX}_*.bai $CNV_BAMS/${TUMOR_PFX}_T.bai 
		
	fi
	

	if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
		#germline from paired sample
		#For the purpose of CNVKIT, lets also make a copy of this bam in a dir called CNVKIT_BAMS and rename them

		#first ensure that labv bam is not present (it may be if we did not run 2nd part of CP eg.)
                rm $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_*_labv.bam $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}*_labv.bai || true

		#now proceed to copy
        	cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } ##file is paired and _G
		cp $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_*.bai $CNV_BAMS/
		mv $CNV_BAMS/bqsr_${GERMLINE_PFX}_*.bam $CNV_BAMS/${GERMLINE_PFX}_G.bam 
		mv $CNV_BAMS/bqsr_${GERMLINE_PFX}_*.bai $CNV_BAMS/${GERMLINE_PFX}_G.bai
	fi


	if [ -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
		#germline from unpaired sample

		#first ensure that labv bam is not present (it may be if we did not run 2nd part of CP eg.)
                rm $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}_*_labv.bam $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}*_labv.bai || true

		#now proceed to copy
		cp $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}_*.bam $CNV_BAMS/ || { echo 'COPY failed' ; exit 1; } ##file is unpaired and _UN
		cp $SAVEROOT/$GERMLINE_PFX/recal/bqsr_${GERMLINE_PFX}_*.bai $CNV_BAMS/
                mv $CNV_BAMS/bqsr_${GERMLINE_PFX}_*.bam $CNV_BAMS/${GERMLINE_PFX}_G.bam
                mv $CNV_BAMS/bqsr_${GERMLINE_PFX}_*.bai $CNV_BAMS/${GERMLINE_PFX}_G.bai
	fi
	

	done<$LIST_OF_SAMPLES

	
	#Make sure time stamps for index files are later than for bam files (when you copy over, bai files copy faster so timestamp may be earlier instead of later)
	#https://buildmedia.readthedocs.org/media/pdf/cnvkit/latest/cnvkit.pdf, PG17, Note: If you’ve prebuilt the BAM index file (.bai), make sure its timestamp is later than the BAM file’s.
	#Thought this would solve "no X, is sample truncated" error in whole command t/g but sadly there was no change! - this is for .7 latest version, now using .6v
	touch -c $CNV_BAMS/*.bai
}
#------------------------------------------------------------------------------------------------------------------------------------------------------



############################
#CNVKIT - this is to find CNV (here we use both t and g samples)
############################
#Summary
#First CNV_BAMS is filled with copied bams and bais but deleted, all intermediate outputs found in CNV_T_RESULTS and CNV_G_RESULTS while final outputs can be found in CNVKIT_OUTPUT.
#YOU CAN ONLY RUN THIS PIPELINE AFTER CP_PAIRED_pipeline.sh has run on ALL files as bam files are needed for all of the tumor-germline pairs.
#At the very least, run CP_PAIRED_pipeline box 1 on all pairs and then send output to this script
#Needed files- 1) covered sites bed files 2) an access bed file (accessible sites) - use the file provided (access-5k-mappable.hg19.bed), 5k better than 10k, window of bases, lower the no, better the granularity:#https://cnvkit.readthedocs.io/en/stable/
#Expected output- [target.bed, antitarget.bed, germline.targettcoverage.cnn, germline.antitargetcoverage.cnn, tumor.targetcoverage.cnn, tumor.antitargetcoverage.cnn, tumor.cnr,tumor.cns, tumor]
#the .cns file is then converted to .vcf file [bqsr_tumor.vcf file created]
#.cnr has several values ------> in the.cns file, it groups regions into segments- we want to look at cnr file to a)perform breakdown by exons b)perform breakdown by genes 3)create plots per gene for tumor genes AND germline genes
#cnr file may break down a capture region/row from bedfile into even smaller bins! take note.
#Looks like weight is dependent on a)length of bin, shorter, lesser weight b)read depth, lesser, lesser weight. Weights are SUMMED over a segment made from bins, not average!!
#------------------------------------------------------------------------------------------------------------------------------------------------------
cnvkit_t_g () {
	touch ./cnvkit/cnvkit_env_variables.txt
	> ./cnvkit/cnvkit_env_variables.txt

	echo "CNVKIT=$CNVKIT" >> ./cnvkit/cnvkit_env_variables.txt
	echo "CNV_BAMS=$CNV_BAMS" >> ./cnvkit/cnvkit_env_variables.txt
	echo "BEDFILE_0B_BAITS=$BEDFILE_0B_BAITS" >> ./cnvkit/cnvkit_env_variables.txt
	echo "REF_GENOME_b37=$REF_GENOME_b37" >> ./cnvkit/cnvkit_env_variables.txt
	echo "CNV_T_RESULTS=$CNV_T_RESULTS" >> ./cnvkit/cnvkit_env_variables.txt
	echo "CNV_G_RESULTS=$CNV_G_RESULTS" >> ./cnvkit/cnvkit_env_variables.txt
	echo "CNVKIT_OUTPUT=$CNVKIT_OUTPUT" >> ./cnvkit/cnvkit_env_variables.txt
	
	docker run --user root \
	-v /mnt/disk4/labs/salipante/nithisha/programs:/mnt/disk4/labs/salipante/nithisha/programs \
	-v /mnt/disk4/labs/salipante/nithisha/CP:/mnt/disk4/labs/salipante/nithisha/CP \
	--env-file /mnt/disk4/labs/salipante/nithisha/CP/cnvkit/cnvkit_env_variables.txt \
	nithisha_cnvkit:096
	
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
exon_gene_breakdown () {
	#Calulate per exon/per gene PER sample, results stored in CNV_T_RESULTS or CNV_G_RESULTS dirs

	>$CNVKIT_OUTPUT/list_of_exon_samples.txt
	>$CNVKIT_OUTPUT/list_of_gene_samples.txt

	#Exon and gene breakdown
	while IFS='' read -r line
        do
        TUMOR_PFX=$(echo $line| awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')

        ################################For each tumor sample########################################
	if [ ! -z $TUMOR_PFX ];then
		TUMOR_CNR=$(ls $CNV_T_RESULTS/${TUMOR_PFX}_T.cnr)
	
		#breakdown by exon
		python $EXON_BREAKDOWN $TUMOR_PFX $CNV_T_RESULTS $QC_EXON_RANGES $TUMOR_CNR &
		echo $CNV_T_RESULTS/${TUMOR_PFX}.cnvkit_exon_analysis.txt >> $CNVKIT_OUTPUT/list_of_exon_samples.txt
	
		#breakdown by gene
		python $GENE_BREAKDOWN $TUMOR_PFX $CNV_T_RESULTS $QC_GENE_RANGES $TUMOR_CNR &
		echo $CNV_T_RESULTS/${TUMOR_PFX}.cnvkit_gene_analysis.txt >> $CNVKIT_OUTPUT/list_of_gene_samples.txt

	fi
	###############################For each germline sample#######################################
        if [ ! -z $GERMLINE_PFX ];then
		GERMLINE_CNR=$(ls $CNV_G_RESULTS/${GERMLINE_PFX}_G.cnr)
	
		#If statements because different tumors might have same germline sample
		if [ ! -f $CNV_G_RESULTS/${GERMLINE_PFX}.cnvkit_exon_analysis.txt ];then
			#breakdown by exon
			python $EXON_BREAKDOWN $GERMLINE_PFX $CNV_G_RESULTS $QC_EXON_RANGES $GERMLINE_CNR &
			echo $CNV_G_RESULTS/${GERMLINE_PFX}.cnvkit_exon_analysis.txt >> $CNVKIT_OUTPUT/list_of_exon_samples.txt
			wait

		else
			echo "Skipping exon breakdown analysis for sample: $GERMLINE_PFX"
		fi

		if [ ! -f $CNV_G_RESULTS/${GERMLINE_PFX}.cnvkit_gene_analysis.txt ];then
			#breakdown by gene
        		python $GENE_BREAKDOWN $GERMLINE_PFX $CNV_G_RESULTS $QC_GENE_RANGES $GERMLINE_CNR &
			echo $CNV_G_RESULTS/${GERMLINE_PFX}.cnvkit_gene_analysis.txt >> $CNVKIT_OUTPUT/list_of_gene_samples.txt
			wait
		fi

	fi
	
	done<$LIST_OF_SAMPLES
}

#------------------------------------------------------------------------------------------------------------------------------------------------------
calculate_z_scores () {
	#Calculate z scores as a bearer of confidence for each call being made, results stored in CNVKIT_OUTPUT dir
        python $CAL_Z_SCORE_SCRIPT $SAVEROOT $CNVKIT_OUTPUT
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
plot_graphs () {
	#Plot graphs for each sample,results stored in CNV_T_RESULTS or CNV_G_RESULTS dirs
	
	JOBS_IN_BATCH=10
	count=0
	#Run R scripts to produce graphs
	Rscript $PLOT_R_SUBFUNCS 
	#Produce graphs for tumor-germline paired sample [args needed 1)broca_genes 2)refseq file 3)cnr file from cnvkit]
	while IFS='' read -r line
	do
	TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
	GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')	

	################################For each tumor sample########################################
	if [ ! -z $TUMOR_PFX ];then
		TUMOR_CNR=$(ls $CNV_T_RESULTS/${TUMOR_PFX}_T.cnr)
		Rscript $PLOT_R_MAINFUNC $PLOT_R_SUBFUNCS $BROCA_GENES $REFSEQ $TUMOR_CNR &
		count=$((count+1))
	fi
	###############################For each germline sample#######################################
	if [ ! -z $GERMLINE_PFX ];then
		GERMLINE_CNR=$(ls $CNV_G_RESULTS/${GERMLINE_PFX}_G.cnr)
		#Dont repeat graph plot for a germline that is present for 2 or more tumor samples (TA, TB etc)
		if [ ! -f $CNV_G_RESULTS/${GERMLINE_PFX}_G.pdf ];then
			Rscript $PLOT_R_MAINFUNC $PLOT_R_SUBFUNCS $BROCA_GENES $REFSEQ $GERMLINE_CNR &
			sleep 5
			count=$((count+1))

		else
			echo "Skipping graph creation for sample: $GERMLINE_PFX"
		fi
	fi	

        if [ $count -ge $JOBS_IN_BATCH ];then
        	wait
        	count=0
        fi
	
	done< $LIST_OF_SAMPLES
}

#------------------------------------------------------------------------------------------------------------------------------------------------------
compress_plots () {
	#Compress plots, results found in CNVKIT_OUTPUT DIR
        #Compress pdfs - https://www.shellhacks.com/linux-compress-pdf-reduce-pdf-size/
        #we need the ghostscript suite that provides the ps2pdf utility - reduces about 3000 KB to 550KB
	JOBS_IN_BATCH=20
        count=0

        while IFS='' read -r line
        do
	echo $line
        TUMOR_PFX=$(echo $line|awk -F'|' '{print $1}')
        GERMLINE_PFX=$(echo $line| awk -F'|' '{print $2}')

	################################For each tumor sample########################################
        if [ ! -z $TUMOR_PFX ];then
		echo "Compressing tumor file pdf for $TUMOR_PFX"
        	ps2pdf $CNV_T_RESULTS/${TUMOR_PFX}_T.pdf $CNVKIT_OUTPUT/${TUMOR_PFX}_T.pdf &
		count=$((count+1))
	fi
	###############################For each germline sample#######################################	
        if [ ! -z $GERMLINE_PFX ];then
		echo "Compressing germline file pdf for $GERMLINE_PFX"
		ps2pdf $CNV_G_RESULTS/${GERMLINE_PFX}_G.pdf $CNVKIT_OUTPUT/${GERMLINE_PFX}_G.pdf &
		count=$((count+1))
	fi

        if [ $count -ge $JOBS_IN_BATCH ];then
        	wait
        	count=0
        fi
        
	done< $LIST_OF_SAMPLES
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
merge_pfs_scatter_diagram () {
        #Merge scatter.pdfs for tumors and germlines respectively in CNV_T_RESULTS or CNV_G_RESULTS, but produce output in cnvkit finall output dir(CNVKIT_OUTPUT) after compression
	if [ $(ls $CNV_T_RESULTS | grep '.*scatter.pdf' | wc -l) -gt 0 ]; then
                gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_T_RESULTS/${RUN}_T_scatter.pdf -dBATCH $CNV_T_RESULTS/*-scatter.pdf
                ps2pdf  -dAutoRotatePages=/None $CNV_T_RESULTS/${RUN}_T_scatter.pdf $CNVKIT_OUTPUT/${RUN}_T_scatter.pdf
        fi

        if [ $(ls $CNV_G_RESULTS | grep '.*scatter.pdf' | wc -l) -gt 0 ]; then
                gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_G_RESULTS/${RUN}_G_scatter.pdf -dBATCH $CNV_G_RESULTS/*-scatter.pdf
                ps2pdf  -dAutoRotatePages=/None $CNV_G_RESULTS/${RUN}_G_scatter.pdf $CNVKIT_OUTPUT/${RUN}_G_scatter.pdf
        fi

        #Merge diagram.pdfs for tumors and germlines respectively in CNV_T_RESULTS or CNV_G_RESULTS, but produce output in cnvkit finall output dir(CNVKIT_OUTPUT) after compression
        if [ $(ls $CNV_T_RESULTS | grep '.*diagram.pdf' | wc -l) -gt 0 ]; then
                gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_T_RESULTS/${RUN}_T_diagram.pdf -dBATCH $CNV_T_RESULTS/*-diagram.pdf
                ps2pdf  -dAutoRotatePages=/None $CNV_T_RESULTS/${RUN}_T_diagram.pdf $CNVKIT_OUTPUT/${RUN}_T_diagram.pdf
        fi

        if [ $(ls $CNV_G_RESULTS | grep '.*diagram.pdf' | wc -l) -gt 0 ]; then
                gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$CNV_G_RESULTS/${RUN}_G_diagram.pdf -dBATCH $CNV_G_RESULTS/*-diagram.pdf
                ps2pdf  -dAutoRotatePages=/None $CNV_G_RESULTS/${RUN}_G_diagram.pdf $CNVKIT_OUTPUT/${RUN}_G_diagram.pdf
        fi
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
combine_results_exons_genes () {
        #produce a combined result for all samples in run for a)exon analysis b)gene analysis. result found in CNV_OUTPUT
        python $COMBINE_RESULTS_SCRIPT $RUN $CNVKIT_OUTPUT $LIST_OF_ORDERED_SAMPLES
}
#------------------------------------------------------------------------------------------------------------------------------------------------------
format_output () {
	#Format output, result found in CNV_OUTPUT
	python $FORMAT_OUTPUTS_SCRIPT $CNVKIT_OUTPUT
}
#------------------------------------------------------------------------------------------------------------------------------------------------------


############################
#DELETE INTERMEDIATE FILES
############################
#------------------------------------------------------------------------------------------------------------------------------------------------------
del_dirs_files () {
        #Remember to delete all intermediate large files
        rm -r $CNV_BAMS
	rm -r $CNV_T_RESULTS $CNV_G_RESULTS
	rm $CNVKIT_OUTPUT/*.txt
}
#------------------------------------------------------------------------------------------------------------------------------------------------------

#Intermediate functions

cnvkit () {
	cnvkit_t_g
	exon_gene_breakdown
	wait
	calculate_z_scores
	plot_graphs
	compress_plots
	merge_pfs_scatter_diagram
	combine_results_exons_genes
	format_output
}



#####Main code######
copy_and_rename_bams
cnvkit
del_dirs_files
