#The purpose of this script is to calculate z scores (or rather p values) for each row in a cnvkit_T_exon or cnvkit_G_exon file. 
#How we do this is for each row of the output, we calculate the average and standard deviation using all the cnvkit_G_exon files and then we use that values against the T_file and G_file. Compare row by row.
#For exons/genes with no reads across all samples, we will see a z score of 0 (because we fill missing values with 0) and therefore, a p value of 1. just FYI.

import os
import sys
import pandas as pd
import scipy.stats as st

SAVEROOT = sys.argv[1] #CP/results #no / at the end
CNVKIT_OUTPUT = sys.argv[2] # results/CNVKIT_OUTPUT

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Dir check
if os.path.exists(SAVEROOT) == False or os.path.exists(CNVKIT_OUTPUT) == False:
	print "ERROR:One of required directories not present"
	sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#EXONS
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
def calculate_exon_means_from_germlines ():
	all_G_df_cols_exon = pd.DataFrame()
	#First we loop through each cnvkit_G_exon file and get the "Ave_Log_Ratio" column out and append them to a new file
	directory = SAVEROOT + "/cnvkit_G_results/"

	for filename in os.listdir(directory):
     		if filename.endswith(".cnvkit_exon_analysis.txt"):
			full_file_path = directory + filename
			df = pd.read_csv(full_file_path, sep="\t", header = 0, index_col=None)
			col_name = filename.split(".cnvkit")[0] #getting the G PFX
			all_G_df_cols_exon[col_name] = df["Ave_Log_Ratio"]


	#Calculate mean and s.d per row for all_G_df_cols_exon df and save this to new df
	concat_df = pd.DataFrame()
	concat_df["Mean"] = all_G_df_cols_exon.mean(axis=1)
	concat_df["Standard_Deviation"] = all_G_df_cols_exon.std(axis=1)

	all_G_df_cols_exon = pd.concat([all_G_df_cols_exon,concat_df],axis=1)
	outputfile =  directory + "/allGdfcols_exon.txt"
	all_G_df_cols_exon.to_csv(outputfile,sep="\t",index=False)

	return all_G_df_cols_exon

#--------------------------------------------------------------------------------------------------------------------------
def calculate_exon_pscore (file_path, all_G_df_cols_exon):
	print "\nrunning EXON z score analysis for samples...."
	#Now we loop through each cnvkit_TorG_exon file and calculate p value

	print file_path
        exon_df = pd.read_csv(file_path, sep="\t", header = 0, index_col=None)
			
	#Join the mean and sd cols calculated from all germlines to the df
	exon_df = exon_df.join(all_G_df_cols_exon[["Mean","Standard_Deviation"]])

	#Calculate a new col which shows the z score
	exon_df.loc[exon_df["Mean"] != 0, "z_score"] = (exon_df["Ave_Log_Ratio"].astype(float) - exon_df["Mean"].astype(float)) / exon_df["Standard_Deviation"].astype(float)
	exon_df["z_score"] = exon_df["z_score"].fillna(0)
		
	#Calculate the a new column which coverts the z score to a p value. Here, we use a 2 tailed test because our hypothesis is that the sample average log ratio is significantly different than the mean of all germline samples.
	#For cols with Mean = 0, we set z score to 0 already so we do not need to worry abt fillna for the col "p-value"
	exon_df["p_value"] = 2 * (1 - st.norm.cdf(abs(exon_df["z_score"])))
			
	#write to file
	filename = os.path.basename(file_path)
	outputfile = CNVKIT_OUTPUT + "/" + filename
	exon_df.to_csv(outputfile,sep="\t",index=False)
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------




#GENES
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
def calculate_gene_means_from_germlines ():
        all_G_df_cols_gene = pd.DataFrame()
        #First we loop through each cnvkit_G_gene file and get the "Ave_Log_Ratio" column out and append them to a new file
        directory = SAVEROOT + "/cnvkit_G_results/"

        for filename in os.listdir(directory):
                if filename.endswith(".cnvkit_gene_analysis.txt"):
			full_file_path = directory + filename
                        df = pd.read_csv(full_file_path, sep="\t", header = 0, index_col=None)
                        col_name = filename.split(".cnvkit")[0] #getting the G PFX
                        all_G_df_cols_gene[col_name] = df["Ave_Log_Ratio"]


        #Calculate mean and s.d per row for all_G_df_cols_gene df
        concat_df = pd.DataFrame()
        concat_df["Mean"] = all_G_df_cols_gene.mean(axis=1)
        concat_df["Standard_Deviation"] = all_G_df_cols_gene.std(axis=1) 

        all_G_df_cols_gene = pd.concat([all_G_df_cols_gene,concat_df],axis=1)
	outputfile =  directory + "/allGdfcols_gene.txt"
        all_G_df_cols_gene.to_csv(outputfile,sep="\t",index=False)

        return all_G_df_cols_gene
#--------------------------------------------------------------------------------------------------------------------------
def calculate_gene_pscore (file_path, all_G_df_cols_gene):
	print "\nrunning GENE z score analysis for samples...."
        #Now we loop through each cnvkit_TorG_gene file and calculate p value

	print file_path
        gene_df = pd.read_csv(file_path, sep="\t", header = 0, index_col=None)

        #Join the mean and sd cols calculated from all germlines to the df
        gene_df = gene_df.join(all_G_df_cols_gene[["Mean","Standard_Deviation"]])

        #Calculate a new col which shows the z score
	gene_df.loc[gene_df["Mean"] != 0, "z_score"] = (gene_df["Ave_Log_Ratio"].astype(float) - gene_df["Mean"].astype(float)) / gene_df["Standard_Deviation"].astype(float)
        gene_df["z_score"] = gene_df["z_score"].fillna(0)

        #Calculate the a new column which coverts the z score to a p value. Here, we use a 2 tailed test because our hypothesis is that the sample average log ratio is significantly different than the mean of all germline samples.
        gene_df["p_value"] = 2 * (1 - st.norm.cdf(abs(gene_df["z_score"]))) #using 2 tailed test

        #write to file
	filename = os.path.basename(file_path)
	outputfile = CNVKIT_OUTPUT + "/" + filename
        gene_df.to_csv(outputfile,sep="\t",index=False)
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

#Main code
#####################EXONS#####################################
#Calculate means using all germline results for exons
all_G_df_cols_exon = calculate_exon_means_from_germlines ()
#Calculate z and p scores for exons
file_name = CNVKIT_OUTPUT + "/list_of_exon_samples.txt"
with open(file_name) as file:
	for line in file:
		calculate_exon_pscore (line.rstrip('\n'), all_G_df_cols_exon)

#####################GENES#####################################
#Calculate means using all germline results for genes
all_G_df_cols_gene = calculate_gene_means_from_germlines ()
#Calculate z and p scores for exons
file_name = CNVKIT_OUTPUT + "/list_of_gene_samples.txt"
with open(file_name) as file:
	for line in file:
		calculate_gene_pscore (line.rstrip('\n'), all_G_df_cols_gene)


