set -x -e
RUN=$1
RUN_TYPE=$2

#Some information about the run to check what gene file, bedfile and bait file was used.
touch "$RUN_RESULTS/${RUN}_info.txt"
> $RUN_RESULTS/${RUN}_info.txt

cat > $RUN_RESULTS/${RUN}_info.txt <<EOL 
Running $RUN.
The run type is $RUN_TYPE.
The broca genes list file used: $BROCA_GENES.
The bed file (targets) file used: $BEDFILE_0B.
The baits file (baits - longer) used: $BEDFILE_0B_BAITS.

Dependences
-------------
1. Python 2.7 
	- pandas 0.24.2 (re-install in August 2022)
	- numpy 1.16.6 (re-install in August 2022)
	- xlrd 1.2.0 #allows Python to read Excel files (re-install in August 2022)
	- xlsxwriter 2.0.0 (re-install in August 2022)
	- pysam 0.19.0
	- matplotlib 2.2.5
	- future 0.18.2 #for cnvkit (re-install in August 2022)
2. Python 3.6 (local install in case newer Python version is needed, currently not used for any tool))
3. R 3.4.4
4. R 3.5.1 (local install in case newer R version is needed, currently not used for any tool)
5. BBMap 38.23
6. Cutadapt 1.18 (was lost and made availabe on server in Aug 2022)
7. BWA 0.7.17
8. Samtools 0.1.9 (htslib 1.15)
9. GATK 4.0.8.1
10. Vardict 1.8.2
11. Varscan 2.3.9
12. Tabix 0.2.6
13. Bcftools 1.9
14. Annovar(2019Oct24)
15. GRIDSS 2.7.3 (remember to add bwa to PATH)
16. AnnotSV 3.0.9
17. Bedtools 2.27.1
18. Pindel 0.3
19. msings
20. CNVKIT 0.9.11 (docker)
21. Java 8
22. PureCN Docker image markusriester/purecn:latest

(other dependencies: cURL 7.50.3)
EOL
