#This script is to create a BROCA bed file for bedtools coverage to use to tell us the average read depth per EXON
#Therefore, this bed file should be broken down in terms of each gene's exon starts and stops for the gene's preferred transcript!

import sys
import re
import pandas as pd

#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#Create the layout of the bed file
outputfile = ""
def create_output(outputdir):
	global outputfile
	outputfile = outputdir +  "/QC_bedfile_exons.bed" 
	with open (outputfile, "w") as output:
		output.write("Chromosome	Start	Stop	Gene	Exon_Number\n")
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#save a list of gene names we want to use (also preferred transcripts) 
def gene_names(capturedgenes):
	genes = []
	transcript_chr = []
	with open(capturedgenes, "r") as BROCAgenes:
		header = BROCAgenes.readline()
		for line in BROCAgenes:
			line = line.replace("\r\n","")
			genes.append(line.split("\t")[0])
			transcript_string = (re.sub("\..*","",(line.split("\t")[4]))).strip("\n") #remove version no of transcript to better match refseq table
			chromosome_string = line.split("\t")[1] #use chr as well, transcript without version no and gene can match to 2 rows in refSeq doc eg. CUX1
                        transcript_chr.append(transcript_string + "-" + chromosome_string)
                        geneTranscriptChr = dict(zip(genes,transcript_chr))
        return geneTranscriptChr #Eg. {'MAD2L2': 'NM_001127325-1', 'BRCA2': 'NM_000059-13'}
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#now that we have the names of the genes in the BROCA gene assay, for each gene, determine the refseq exon ranges we are interested in.
#This will produce an array [chr9-1000-2000-Gene-exon:1, ch11-500-999-Gene-exon:2 etc.]
def create_refseq_exon_ranges(geneTranscriptChr, refseq):
	exonpositions = []
	for gene, transcript_chr in geneTranscriptChr.items():
		#establish an array of exon start and stop postions for each exon in preferred transcript. Will assemble a list of contiguous exons.
		exon_per_gene = [] 
		with open (refseq, "r") as refseqq:
			header = refseqq.readline()
			for line in refseqq:
				line.rstrip("\n")
				refline= line.split("\t")
				
                                if refline[12] == gene and re.sub("\..*","", refline[1]) == transcript_chr.split("-")[0] and (refline[2].split("chr"))[1] == transcript_chr.split("-")[1] : #regex to remove version no for NM	
					refline[9] = refline[9].strip("\"")
					refline[9] = refline[9].rstrip(",") #exon starts end with a ,
					refline[10] = refline[10].strip("\"")
					refline[10] = refline[10].rstrip(",") #exon stops
				
					exonstarts = refline[9].split(",")
					exonstops = refline[10].split(",")
						
					numberexons = len(exonstarts)
					#set a counter
					currentexon = 0
					exoncounter = len(exonstarts)
					while currentexon < numberexons:
						if refline[3] == "+":
							#Add this exon to the list of recorded positions.
							rangee = refline[2].split("chr")[1] + "\t" + exonstarts[currentexon] + "\t" + exonstops[currentexon] + "\t" + gene + "\t" + "exon:" + str(currentexon + 1)
							
							exonpositions.append(rangee)
							currentexon = currentexon + 1
						else:
							#Add this exon to the list of recorded positions.
							rangee = refline[2].split("chr")[1] + "\t" + exonstarts[currentexon] + "\t" + exonstops[currentexon] + "\t" + gene + "\t" + "exon:" + str(exoncounter)
							
							exonpositions.append(rangee)
							currentexon = currentexon + 1
							exoncounter = exoncounter - 1
	return exonpositions
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
def write_to_output(exonpositions):
	with open (outputfile, "a+") as output:
		for exon in exonpositions:
			string = exon + "\n"
			output.write(string)
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
def reorder_genes () :
	qc_exon_bed = pd.read_csv(outputfile, sep = "\t", header = 0 , index_col = None)
	qc_exon_bed.sort_values(["Gene","Start"], inplace= True) #sort by gene, and then by position so exon numbering will be 11,10,9 for -ve strand genes that are read backwards
	#drop the header row as the bedfile for bedtools coverage does not read headers
	qc_exon_bed.to_csv(outputfile, sep="\t", index=False, header = False)
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
							
				
	
