import sys
import create_exon_bedfile_subfuncs as qc
import os #only for dir check

#Read in args
captured_genes = sys.argv[1]
refseq = sys.argv[2]
outputdir = sys.argv[3] #creating bedfile at CP/results (outside sample results) one bedfile for ALL samples

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 4 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[captured_genes,refseq]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#Dir check
        if os.path.exists(outputdir) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Main function
qc.create_output(outputdir)
geneTranscript = qc.gene_names(captured_genes)
exonPositions = qc.create_refseq_exon_ranges(geneTranscript, refseq)
qc.write_to_output(exonPositions)
qc.reorder_genes()
