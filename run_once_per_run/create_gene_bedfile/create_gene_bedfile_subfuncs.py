#This script is to create a bed file for bedtools coverage to use to tell us the average read depth per GENE
#Therefore, this bed file should be broken down in terms of each gene for preferred transcript!

import sys
import re
import pandas as pd

#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#Create layout of the bed file
outputfile = ""
def create_output(outputdir):
	global outputfile
	outputfile = outputdir + "/QC_bedfile_genes.bed"
	with open (outputfile, "w") as output:
		output.write("Chromosome	Start	Stop	Gene\n")
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#save a list of gene names you want to use (also preferred transcripts)
def gene_names(capturedgenes):
        genes = []
        transcript_chr = []
        with open(capturedgenes, "r") as BROCAgenes:
                header = BROCAgenes.readline()
                for line in BROCAgenes:
                        line = line.replace("\r\n", "")
                        genes.append(line.split("\t")[0])
                        transcript_string = (re.sub("\..*","",(line.split("\t")[4]))).strip("\n") #remove version no of transcript to better match refseq table
                        chromosome_string = line.split("\t")[1] #use chr as well, transcript without version no and gene can match to 2 rows in refSeq doc eg. CUX1
                        transcript_chr.append(transcript_string + "-" + chromosome_string)
                        geneTranscriptChr = dict(zip(genes,transcript_chr))
        return geneTranscriptChr #Eg. {'MAD2L2': 'NM_001127325-1', 'BRCA2': 'NM_000059-13'}
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#now that we have the names of the genes in the BROCA gene assay, we determine the start and stop positions of each gene from refseq

def create_refseq_gene_ranges(geneTranscriptChr, refseq):
        genepositions = []
        for gene, transcript_chr in geneTranscriptChr.items():
                with open (refseq, "r") as refseqq:
                        header = refseqq.readline()
                        for line in refseqq:
                                line.rstrip("\n")
                                refline = line.split("\t")

                                if refline[12] == gene and re.sub("\..*","", refline[1]) == transcript_chr.split("-")[0] and (refline[2].split("chr"))[1] == transcript_chr.split("-")[1] : #regex to remove version no for NM
                                        txstart = refline[4]
                                        txend = refline[5]

                                        rangee = refline[2].split("chr")[1] + "\t" + txstart + "\t" + txend + "\t" + gene
                                        genepositions.append(rangee)
        return genepositions
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
def write_to_output(genepositions):
	with open (outputfile, "a+") as output:
		for gene in genepositions:
			string = gene + "\n"
			output.write(string)
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
def reorder_genes ():
	qc_gene_bed = pd.read_csv(outputfile, sep="\t", header = 0 , index_col = None)
	qc_gene_bed.sort_values(["Gene"], inplace= True) #sort by gene alphabetically
	#drop the header row as the bedfile for bedtools coverage does not read headers
	qc_gene_bed.to_csv(outputfile, sep="\t", index=False, header = False)
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
	
