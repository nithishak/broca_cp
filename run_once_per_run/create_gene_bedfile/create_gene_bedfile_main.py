import sys
import create_gene_bedfile_subfuncs as qc
import os #only for dir check

#Read in args
captured_genes = sys.argv[1] #BROCAgenes
refseq = sys.argv[2]
outputdir = sys.argv[3]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 4 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[captured_genes,refseq]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#Dir check
	if os.path.exists(outputdir) == False:
		print "ERROR:One of required directories not present"
		sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Main function
qc.create_output(outputdir)
geneTranscript = qc.gene_names(captured_genes)
genePositions = qc.create_refseq_gene_ranges (geneTranscript, refseq)
qc.write_to_output(genePositions)
qc.reorder_genes()
