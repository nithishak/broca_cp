set -x -e
#This script produces files that are needed for each paired sample for the ENTIRE RUN
#So only run this ONCE per run and save these common files to $SAVEROOT - CP/results

RUN=$1
RUN_TYPE=$2
SAVEROOT=$3

#variables used
#BROCA_GENES
#REFSEQ
#QC_EXON_CREATE_BEDFILE
#QC_GENE_CREATE_BEDFILE
#CREATE_ANNOTSV_FILES


#delete files IF already present so they do not get appended to
rm -f $SAVEROOT/QC_bedfile_exons.bed $SAVEROOT/QC_bedfile_genes.bed $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt


#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 3 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BROCA_GENES|$REFSEQ|$QC_EXON_CREATE_BEDFILE|$QC_GENE_CREATE_BEDFILE|$CREATE_ANNOTSV_FILES"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#############################generate some information about reference files and package versions used for this run#######################
	generate_run_info () {
		$GENERATE_RUN_INFO $RUN $RUN_TYPE
	}
#########################################################################################################################################


#############################create bedfiles for exons and genes, have them handy, its good to know the exon ranges and gene ranges for your assay#############################################
create_exon_gene_bedfiles () {
        #To create a bedfile for QC per exon #DO THIS ONCE PER RUN
        python $QC_EXON_CREATE_BEDFILE $BROCA_GENES $REFSEQ $SAVEROOT #last arg is output dir
        #To create a bedfile for QC per gene
        python $QC_GENE_CREATE_BEDFILE $BROCA_GENES $REFSEQ $SAVEROOT #last arg is output dir
}
#################################################################################################################################################################################################


#################################create txFile needed for AnnotSV that is used for Gridss and Pindel (this is the list of preferred transcripts)#################################################
create_AnnotSVfiles () {
	#https://lbgi.fr/AnnotSV/Documentation/README.AnnotSV_latest.pdf (ANNOTSV's README has instructions) (The txFile can be space or tab delimited, ours is tab separated) 
	#TAKE OUT VERSION NOS. Note that our BROCA_genes file may have NMxxxx while refseq has NMxxxx.3 (a version no) and this was causing problems for HELQ version in broca genes was 2, refseq was 3, annotsv output also does NOT have version nos
	python $CREATE_ANNOTSV_FILES $BROCA_GENES $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt
}
#################################################################################################################################################################################################

#Main code
generate_run_info
create_exon_gene_bedfiles
create_AnnotSVfiles
