#This script is to create the list of preferred transcripts and genes for AnnosotSV (txFile arg) and I am creating it without version numbers and tab spaced
import sys
import pandas as pd
import re

BROCA_GENES_FILE= sys.argv[1]
OUTPUT_TXFILE = sys.argv[2] #output - list of preferred transcripts (txFile)
OUTPUT_CANGENES = sys.argv[3] #output - list of broca genes, also known as candidate genes in AnnotSV

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 4 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[BROCA_GENES_FILE]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

broca_genes = pd.read_csv(BROCA_GENES_FILE, sep= "\t", header = 0)
transcripts = list(broca_genes["partition refseq"])
genes =list(broca_genes["Gene"])


def preferred_transcripts(transcripts_list) :
	string = ""
	for transcript in transcripts:
		t = transcript.strip(" ").strip("\n")
		#print t + "test"
		t_no_version = re.sub("\..*","",t)
		string = string + t_no_version + "\t"


	with open (OUTPUT_TXFILE, "a+") as output:
		output.write(string.rstrip("\t"))



def preferred_genes(genes_list) :
	string = ""
        for gene in genes:
                g = gene.strip(" ").strip("\n")
                string = string + g + "\t"


        with open (OUTPUT_CANGENES, "a+") as output:
                output.write(string.rstrip("\t"))


#Main code
preferred_transcripts(transcripts)
preferred_genes(genes)

