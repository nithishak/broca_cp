import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Tidy up the output
#Re-orderfing the cols
def re_order_columns (ordered_col_list,output):
    output = output[ordered_col_list]                                                                                                                                                                                                            
    return output

#Create a new "Count" col
def create_count_col (ordered_col_list,output) :
	##ONLY total number of samples with sufficent coverage that indicate MSI
	i = 0
	while i < output.shape[0]:
		row = output.loc[i]
		if i <= 3:
			output.loc[i, "Count"] = ""
		else:
			count = 0
			for col in ordered_col_list:
				if str(row[col]) != "": 
					count = count + int(row[col]) 
			output.loc[i, "Count"] = str(count) 
		i += 1
			
	
	return output
#-----------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code
def tidy_output(ordered_col_list, output):
	#Re-order cols
        output = re_order_columns (ordered_col_list,output)
	#De-index df
	output = output.reset_index()
	#Add a count col
	final_output = create_count_col(ordered_col_list,output)
	#Re-index df
	final_output = final_output.set_index("Position")

	return final_output
