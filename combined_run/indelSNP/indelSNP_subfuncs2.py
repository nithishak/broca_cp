import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Quality scores
#Find min and max qual across all samples for a particular position
#------------------------------------------------------
def add_qual (qual_dict,merged_df):
	#For min qual
	qual_col_min = {key:str(min(value)) for (key,value) in qual_dict.items()}
	#Add back the Qual min scores to the final output
	#MAKE SURE TO MAP so that the positions match
	merged_df["QUAL_min"] = merged_df["Primary_key"].map(qual_col_min)

	#For max qual	 
	qual_col_max = {key:str(max(value)) for (key,value) in qual_dict.items()}
	merged_df["QUAL_max"] = merged_df["Primary_key"].map(qual_col_max)
		
	return merged_df


#Filter scores
#------------------------------------------------------
#Inner func
def set_filters (filter_list):
	#Find the unique values of filter col across all samples for a particular position
	#sort is needed as some show filter as "PASS,NMxxxx", "NMxxx,PASS" but both are the same!
	long_str = ";".join(filter_list)
	unique = sorted(list(set(long_str.split(";"))))
	
	return ";".join(unique)
#------------------------------------------------------
def add_filter (filter_dict,merged_df):
	filter_col = {key:set_filters(value) for key,value in filter_dict.items()}
	#Add back the filter col to the final output
	merged_df['Filter'] = merged_df["Primary_key"].map(filter_col)
		
	return merged_df


#Eventstatus scores
#------------------------------------------------------
#Inner func
def set_eventstatus (eventstatus_list):
	#Find the unique values of eventstatuscol across all samples for a particular position
	long_str = ";".join(eventstatus_list)
	unique = sorted(list(set(long_str.split(";"))))
	
	return ";".join(unique)
#------------------------------------------------------
def add_eventstatus (eventstatus_dict,merged_df):
	eventstatus_col = {key:set_eventstatus(value) for key,value in eventstatus_dict.items()}
	#Add back the eventstatus col to the final output
	merged_df['EventStatus'] = merged_df["Primary_key"].map(eventstatus_col)
		
	return merged_df

#Tidy up the output
#Re-orderfing the cols
def re_order_columns (ordered_col_list,output):
	ordered_col_list = [x + "_Ref|Var" for x in ordered_col_list]
	common_column_names = ["Position","Ref","Alt","Gene","TranscriptRegion","TypeOfEvent", "EventStatus","VariantType","PT_Exon","PT_Transcript_Code_Change","PT_Amino_Acid_Change","MSI","PreferredTranscripts","Transcripts","Polyphen2_HDIV_score","SIFT_score","SIFT4G_score","REVEL","AlphaMissense_score","AlphaMissense_pred","GERP++_RS","gerp++gt2","dbscSNV_ADA_SCORE","dbscSNV_RF_SCORE","CLNALLELEID","CLNDN","CLNDISDB","CLNREVSTAT","CLNSIG","ONCDN","ONCDISDB","ONCREVSTAT","ONC","SCIDN","SCIDISDB","SCIREVSTAT","SCI","SwisherFaves","al_et_Starita_Shendure_2018_Prediction","RAD51C_SGE_functional_classification", "RAD51C_SGE_z_score","Interpro_domain","genomicSuperDups","snp138","cosmic70","AF","AF_popmax","AF_male","AF_female","AF_raw","AF_afr","AF_sas","AF_amr","AF_eas","AF_nfe","AF_fin","AF_asj","AF_oth","non_topmed_AF_popmax","non_neuro_AF_popmax","non_cancer_AF_popmax","controls_AF_popmax","ExAC_nontcga_ALL","ExAC_nontcga_AFR","ExAC_nontcga_AMR","ExAC_nontcga_EAS","ExAC_nontcga_FIN","ExAC_nontcga_NFE","ExAC_nontcga_OTH","ExAC_nontcga_SAS","1000g2015aug_eas","1000g2015aug_eur","1000g2015aug_sas","1000g2015aug_amr","1000g2015aug_afr","1000g2015aug_all","esp6500siv2_ea","esp6500siv2_aa","esp6500siv2_all","Filter","QUAL_min","QUAL_max"]
	column_names = common_column_names + ordered_col_list
	output = output[column_names]
	
	return output

#Create a new "Count" col
def create_count_col (ordered_col_list,output) :
	#ONLY count cols in which the number of variant reads (a|b, we are talking about b) is NOT 0, meaning we only want samples in which we SEE the mutation
	sample_list = [x + "_Ref|Var" for x in ordered_col_list] 
	i = 0
	while i < (output.shape[0]):
		row  = output.loc[i]
		count = 0
		for col in sample_list:
			if str(row[col]) != "nan" and int(row[col].split("|")[1]) > 0:
				count = count + 1
		output.loc[i,"Count"] = str(count)
		i += 1
		
	return output

#Sort the df by gene and position
def sort_df(output):
	output["POS"] = output["Position"].apply (lambda x: x.split(":")[1])
	output.sort_values(by=["Gene", "POS"], inplace=True)
	output.drop("POS", axis=1,inplace=True)
	
	return output
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Main Code
def tidy_output(ordered_col_list,output,qual_dict,filter_dict,eventstatus_dict):
	output["Primary_key"] =  output["Position"] + "_" + output['TypeOfEvent']  + "_" + output["Ref"] + "_" + output["Alt"] 
	#Add in quality col
	output = add_qual (qual_dict,output)
	#Add in filter col
	output = add_filter (filter_dict,output)
	#Add in eventstatus col
	output = add_eventstatus (eventstatus_dict,output)
	#Re-order cols
	output = re_order_columns (ordered_col_list,output)
	#Add in the count col
	output = create_count_col (ordered_col_list,output)
	#Lastly, sort df by genes and position
	final_output = sort_df(output)
		
	return final_output
