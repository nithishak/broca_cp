import pandas as pd
import sys
import numpy as np

#Inner funcs for merge files
#--------------------------------------------------------------------------------------------------------------------------------------------------------------
def read_file(file_name,type_of_analysis):
	if type_of_analysis == "indelSNP_BROCA":
		df =  pd.read_excel(file_name, sheet_name="indelSNP_BROCA",dtype="str")
	else:
		df =  pd.read_excel(file_name, sheet_name="indelSNP",dtype="str")
	return df

def select_for_vardict (df):
	#also drop rows which varscan picked it up but not vardict
	df = df[df["TypeOfEvent"] != "."]
	return df

####QUALITY SCORES#######
def qual_int (df):
	df['QUAL'] = df['QUAL'].apply(pd.to_numeric)
	return df

def create_qual_dict (df, qual_dict):
	current_dict = dict(zip(df['Position'] + "_" + df['TypeOfEvent']  + "_" + df["Ref"] + "_" + df["Alt"], df['QUAL']))
	for k, v in current_dict.items():
		qual_dict[k].append(v)
	return qual_dict	

####FITLER SCORES####
def create_filter_dict (df, filter_dict):
	current_dict = dict(zip(df['Position'] + "_" + df['TypeOfEvent']  + "_" + df["Ref"] + "_" + df["Alt"],df['Filter(Vardict)']))
	for k,v in current_dict.items():
		filter_dict[k].append(v)
	return filter_dict

####EventStatus#### - ONLY PRESENT IN PAIRED SAMPLES
def create_eventstatus_dict (df, eventstatus_dict):
	current_dict = dict(zip(df['Position'] + "_" + df['TypeOfEvent']  + "_" + df["Ref"] + "_" + df["Alt"],df['EventStatus']))
	for k,v in current_dict.items():
		eventstatus_dict[k].append(v)
	return eventstatus_dict

####DROPPING COLS WE DO NOT NEED####
def drop_cols (df,sample_status,file1,file2): 
	#status is  "paired" or "unpaired"
	if sample_status == "paired":
		df= df.drop(['T_VAF(Varscan)','TReads_R|V(Varscan)','G_VAF(Varscan)','GReads_R|V(Varscan)','T_VAF(Vardict)','G_VAF(Vardict)','QUAL','Filter(Vardict)','EventStatus'], axis =1)
		df = df.rename(columns={"TReads_R|V(Vardict)":file1 + "_Ref|Var", "GReads_R|V(Vardict)": file2 + "_Ref|Var"})

	if sample_status == "unpaired":
		df = df.drop(['VAF(Varscan)','Reads_R|V(Varscan)','VAF(Vardict)','QUAL','Filter(Vardict)'], axis =1)
		df = df.rename(columns={"Reads_R|V(Vardict)":file1 + "_Ref|Var"})
	
	return df

####MERGE####
def merge_dfs (merged_df, df,sample_status,file1,file2): 
	if merged_df.shape[0] == 0:
			merged_new_df = df
	else:
		if sample_status == "paired" and (file2 +"_Ref|Var") in merged_df.columns: #if germline sample is already in the merged_df (as some paired samples can share the same germline sample)
			#to record info from 2nd G file for rows in tumor2 that were not in tumor1 and so not in 1st G file
			df[file1 + "_Ref|Var"] = df[file1 + "_Ref|Var"] + "/" + df[file2 + "_Ref|Var"]
			df = df.drop([(file2 + "_Ref|Var")], axis=1) #drop germline col that is repeated among samples from this file
			merged_new_df = pd.merge(merged_df,df, on= ["Position","Ref","Alt","Gene","TranscriptRegion","TypeOfEvent","VariantType","PT_Exon","PT_Transcript_Code_Change","PT_Amino_Acid_Change","MSI","PreferredTranscripts","Transcripts","Polyphen2_HDIV_score","SIFT_score","SIFT4G_score","REVEL","AlphaMissense_score","AlphaMissense_pred","GERP++_RS","gerp++gt2","dbscSNV_ADA_SCORE","dbscSNV_RF_SCORE","CLNALLELEID","CLNDN","CLNDISDB","CLNREVSTAT","CLNSIG","ONCDN","ONCDISDB","ONCREVSTAT","ONC","SCIDN","SCIDISDB","SCIREVSTAT","SCI","SwisherFaves","al_et_Starita_Shendure_2018_Prediction","RAD51C_SGE_functional_classification", "RAD51C_SGE_z_score","Interpro_domain","genomicSuperDups","snp138","cosmic70","AF","AF_popmax","AF_male","AF_female","AF_raw","AF_afr","AF_sas","AF_amr","AF_eas","AF_nfe","AF_fin","AF_asj","AF_oth","non_topmed_AF_popmax","non_neuro_AF_popmax","non_cancer_AF_popmax","controls_AF_popmax","ExAC_nontcga_ALL","ExAC_nontcga_AFR","ExAC_nontcga_AMR","ExAC_nontcga_EAS","ExAC_nontcga_FIN","ExAC_nontcga_NFE","ExAC_nontcga_OTH","ExAC_nontcga_SAS","1000g2015aug_eas","1000g2015aug_eur","1000g2015aug_sas","1000g2015aug_amr","1000g2015aug_afr","1000g2015aug_all","esp6500siv2_ea","esp6500siv2_aa","esp6500siv2_all"], how= "outer")
			#to record info from 2nd G file for rows in tumor2 that were not in tumor1 and so not in 1st G file 
			for i in range(0,merged_new_df.shape[0]):
				row  = merged_new_df.loc[i]
				if str(row[file1 + "_Ref|Var"]) != "nan": #if tumor sample for that locus is filled
					if str(row[file2 + "_Ref|Var"]) == "nan": #if germline sample for that locus is filled
						row[file2 + "_Ref|Var"] = row[file1 + "_Ref|Var"].split("/")[1]
						row[file1 + "_Ref|Var"] = row[file1 + "_Ref|Var"].split("/")[0]
					else:
						row[file1 + "_Ref|Var"] = row[file1 + "_Ref|Var"].split("/")[0]     
			
		else:
			merged_new_df = pd.merge(merged_df,df, on= ["Position","Ref","Alt","Gene","TranscriptRegion","TypeOfEvent","VariantType","PT_Exon","PT_Transcript_Code_Change","PT_Amino_Acid_Change","MSI","PreferredTranscripts","Transcripts","Polyphen2_HDIV_score","SIFT_score","SIFT4G_score","REVEL","AlphaMissense_score","AlphaMissense_pred","GERP++_RS","gerp++gt2","dbscSNV_ADA_SCORE","dbscSNV_RF_SCORE","CLNALLELEID","CLNDN","CLNDISDB","CLNREVSTAT","CLNSIG","ONCDN","ONCDISDB","ONCREVSTAT","ONC","SCIDN","SCIDISDB","SCIREVSTAT","SCI","SwisherFaves","al_et_Starita_Shendure_2018_Prediction","RAD51C_SGE_functional_classification", "RAD51C_SGE_z_score","Interpro_domain","genomicSuperDups","snp138","cosmic70","AF","AF_popmax","AF_male","AF_female","AF_raw","AF_afr","AF_sas","AF_amr","AF_eas","AF_nfe","AF_fin","AF_asj","AF_oth","non_topmed_AF_popmax","non_neuro_AF_popmax","non_cancer_AF_popmax","controls_AF_popmax","ExAC_nontcga_ALL","ExAC_nontcga_AFR","ExAC_nontcga_AMR","ExAC_nontcga_EAS","ExAC_nontcga_FIN","ExAC_nontcga_NFE","ExAC_nontcga_OTH","ExAC_nontcga_SAS","1000g2015aug_eas","1000g2015aug_eur","1000g2015aug_sas","1000g2015aug_amr","1000g2015aug_afr","1000g2015aug_all","esp6500siv2_ea","esp6500siv2_aa","esp6500siv2_all"], how= "outer")

	return merged_new_df
#--------------------------------------------------------------------------------------------------------------------------------------------------------------

#Sub-function
def merge_dfs_subfunc(file_name,file1,file2,sample_status,qual_dict,filter_dict,eventstatus_dict,merged_df,type_of_analysis):
	#read df
	df = read_file(file_name,type_of_analysis)
	#select for vardict
	df = select_for_vardict(df)
	#make Qual col numeric
	df = qual_int (df)
	#create qual_dict
	qual_dict = create_qual_dict (df, qual_dict)
	#create filter_dict
	filter_dict = create_filter_dict (df, filter_dict)
	#if paired sample, create event_status_dict
	if sample_status == "paired":
		eventstatus_dict =  create_eventstatus_dict(df, eventstatus_dict)
	#drop cols we do not need
	df = drop_cols (df,sample_status,file1,file2)
	#Perform merge
	merged_df = merge_dfs (merged_df, df,sample_status,file1,file2)

	return merged_df																																																				 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------- 

#Main code
def combine_dfs (my_file_list,savepath,merged_df,qual_dict,filter_dict,eventstatus_dict,type_of_analysis): #my file list is the list I create under MH runs
	print "=======================The type of analysis is " + type_of_analysis + "==============================="
	#to merge all dfs in a run
	file_count = 0
	with open (my_file_list, 'r+') as files_list:
		for filee in files_list:
			file_count = file_count + 1
			files_array =  filee.strip("\n").split("|")
			if "" in files_array:
				#single sample
				file1 = ''.join(files_array).strip('|')
				file2 = ""
				sample_status = "unpaired"
			else:
				#paired sample
				file1 = files_array[0]
				file2 = files_array[1]
				sample_status = "paired"

			file_name = savepath + "/" + file1 + "/" + file1 + ".xlsx"
			print file_name

			merged_df = merge_dfs_subfunc(file_name,file1,file2,sample_status,qual_dict,filter_dict,eventstatus_dict,merged_df,type_of_analysis)

	return merged_df
