#The purpose of this script is to make one main output file for the WHOLE run

import sys
import pandas as pd
import string
import os #For dir check only

MH_RUN = sys.argv[1]
SAVEROOT = sys.argv[2]
OUTPUT_FILE = sys.argv[3]
NO_OF_SAMPLES = sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
	print "CHECK: Not all python args provided "
	sys.exit (1)

#Dir check
if os.path.exists(SAVEROOT) == False:
		print "ERROR:One of required directories not present"
		sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

QC_METRICS = SAVEROOT + "/" + MH_RUN + "_final_output/combined_QCMetrics.txt"
QC_EXONS = SAVEROOT + "/" + MH_RUN + "_final_output/combined_QCexons.txt"
QC_GENES = SAVEROOT + "/" + MH_RUN + "_final_output/combined_QCgenes.txt"
INDELSNP = SAVEROOT + "/" + MH_RUN + "_final_output/combined_indelSNP.txt"
INDELSNP_BROCA = SAVEROOT + "/" + MH_RUN + "_final_output/combined_indelSNP_BROCA.txt"
GRIDSS = SAVEROOT + "/" + MH_RUN + "_final_output/combined_gridss.txt"
PINDEL = SAVEROOT + "/" + MH_RUN + "_final_output/combined_pindel.txt"
MSINGS = SAVEROOT + "/" + MH_RUN + "_final_output/combined_msings.txt"

#open files as dfs
qc_metrics = pd.read_csv(QC_METRICS, sep ="\t", header = 0,index_col=0, dtype="str")
qc_exons = pd.read_csv(QC_EXONS, sep ="\t", header =0)
qc_genes = pd.read_csv(QC_GENES, sep ="\t",header = 0)
indelsnp= pd.read_csv(INDELSNP, sep = "\t", header = 0, dtype="str")
indelsnp_BROCA = pd.read_csv(INDELSNP_BROCA, sep="\t", header=0, dtype="str")
gridss = pd.read_csv(GRIDSS, sep ="\t",header = 0, dtype="str")
pindel = pd.read_csv (PINDEL, sep ="\t",header = 0, dtype="str")
msings = pd.read_csv (MSINGS, sep ="\t",header = 0,index_col=0, dtype="str")

# Create a Pandas Excel writer using XlsxWriter as the engine. specify OUTPUT file here!.
writer = pd.ExcelWriter(OUTPUT_FILE, engine='xlsxwriter',options={'strings_to_numbers': True})

# Write each dataframe to a different worksheet.
qc_metrics.to_excel(writer, sheet_name='QC_Metrics')
qc_exons.to_excel(writer, sheet_name='QC_exons',index=False)
qc_genes.to_excel(writer, sheet_name='QC_genes',index=False)
indelsnp.to_excel(writer, sheet_name='indelSNP',index=False)
indelsnp_BROCA.to_excel(writer, sheet_name='indelSNP_BROCA',index=False)
gridss.to_excel(writer, sheet_name='gridss',index=False)
pindel.to_excel(writer, sheet_name='pindel',index=False)
msings.to_excel(writer, sheet_name='msings')


workbook  = writer.book


#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#1. For QC Metrics
def apply_formatting_with_index (dataframe,type_of_analysis):
		worksheet = writer.sheets[type_of_analysis]

		#To see headers clearly without them being condensed
		#To align all contents of the sheet to the left
		format1 = workbook.add_format({'align': 'left'})

		def get_col_widths(dataframe):
				# First we find the maximum length of the index column  #Here we do have an index col so use this!
				idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))]) *1.2
				# Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
				return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)+3]) for col in dataframe.columns]

		for i, width in enumerate(get_col_widths(dataframe)):
				worksheet.set_column(i, i, width,format1)

apply_formatting_with_index (qc_metrics, 'QC_Metrics')
apply_formatting_with_index (msings, 'msings')
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#2. For QC_exon_gene

#Inner func
def find_cells_conditioning(dataframe):
		#Simple function to specify rows and columns for conditional formatting
		#This function is to get Excel sheet cells [F2 - F..] for exon_qc, tells excel conditioning to focus on these cells only - in this case, "Average_Read_Depth" column (or E2-E... in the case of gene_qc)
		limits = []
		no_of_cols = dataframe.shape[1]
		no_of_rows = dataframe.shape[0]
		
		col_alphabet_1 = string.ascii_uppercase[no_of_cols-int(NO_OF_SAMPLES)]
		if no_of_cols > 26:
			col_alphabet_2 = string.ascii_uppercase[no_of_cols/26 - 1] + string.ascii_uppercase[no_of_cols%26 - 1] #this is because the col names are like "CR" for eg.
		else:
			col_alphabet_2 = string.ascii_uppercase[no_of_cols-1]
		top_limit = col_alphabet_1 + "2"
		bottom_limit = col_alphabet_2 + str(no_of_rows+1)
		limits.append(top_limit)
		limits.append(bottom_limit)

		return (limits)

#Main function
def highlight_poor_coverage_cells(dataframe,type_of_qc): #type of qc is a string - "exon" or "gene"
		#Let us try to highlight cells in "Average_Read_Depth" column that are less than 50 to show which exons failed QC
		limits = find_cells_conditioning (dataframe)

		# Get the xlsxwriter workbook and worksheet objects.
		worksheet = writer.sheets['QC_'+ type_of_qc]

		#Apply a conditional format to the cell range.
		#Add a format. Light red fill with dark red text.
		format1 = workbook.add_format({'bg_color': '#FFC7CE',
									   'font_color': '#9C0006'})
		#To align all contents of the sheet to the left
		format2 = workbook.add_format({'align': 'left'})

		#Apply a conditional format to the cell range.
		#limits = find_cells_conditioning(dataframe) #is a list
		string = limits[0] + ":" + limits[1]
		worksheet.conditional_format(string, {'type':	 'cell',
									   'criteria': '<',
										'value':	50,
										'format':   format1})

		#To see headers clearly without them being condensed
		def get_col_widths(dataframe):
				# First we find the maximum length of the index column #Not needed here as we don't have an index col

				# Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
				return [max([len(str(s)) for s in dataframe[col].values] + [len(col) + 3]) for col in dataframe.columns]

		for i, width in enumerate(get_col_widths(dataframe)):
				worksheet.set_column(i, i, width,format2)

highlight_poor_coverage_cells(qc_exons,"exons")
highlight_poor_coverage_cells(qc_genes,"genes")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#3.For indelSNPs
def apply_formatting(dataframe,type_of_analysis):
	   worksheet = writer.sheets[type_of_analysis]

	   format1 = workbook.add_format({'align': 'left'})

	   #To see headers clearly without them being condensed
	   def get_col_widths(dataframe):
			   # First we find the maximum length of the index column #Not needed here as we don't have an index col

			   # Then, we concatenate this to the lengths of column name
				return [(len(x)+3) for x in dataframe.columns]

	   for i, width in enumerate(get_col_widths(dataframe)):
			   worksheet.set_column(i, i, width,format1)

apply_formatting(indelsnp, "indelSNP")
apply_formatting(indelsnp_BROCA, "indelSNP_BROCA")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#For gridss
apply_formatting(gridss, "gridss")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#5.For Pindel
apply_formatting(pindel,"pindel")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#6.For msings
apply_formatting_with_index (msings, 'msings')
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Close the Pandas Excel writer and output the Excel file.
writer.save()
