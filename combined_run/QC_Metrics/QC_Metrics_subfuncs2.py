import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Tidy up the output
#Re-orderfing the cols
def re_order_columns (ordered_col_list,output):
    output = output[ordered_col_list]                                                                                                                                                                                                            
    return output
#-----------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code
def tidy_output(ordered_col_list, output):
	#Re-order cols
        output = re_order_columns(ordered_col_list,output)
	#Transpose df so that all samples are rows (easier format to analyze)
	final_output = output.transpose()

	return final_output
