import pandas as pd
import sys

#Inner funcs for merge files
#--------------------------------------------------------------------------------------------------------------------------------------------------------------
def read_file(file_name):
    df =  pd.read_excel(file_name, sheet_name="pindel",dtype="str", na_filter = False)
    return df


####FITLER SCORES####
def create_filter_dict (df, filter_dict):
	current_dict = dict(zip(df["SV_Position"] + "_" + df["REF"] + "_" + df['ALT'],df['FILTER']))
	for k,v in current_dict.items():
        	filter_dict[k].append(v)
	return filter_dict

####DROPPING COLS WE DO NOT NEED####
def drop_cols (df): 
	df= df.drop(["FILTER"], axis =1)
	return df

####MERGE####
def merge_dfs (merged_df,df,sample_status,file1,file2): 
	if merged_df.shape[0] == 0:
        	merged_new_df = df
	else:
		if sample_status == "paired" and (file2 + "_Var[Fwd,Rev]") in merged_df.columns: #if germline sample is already in the merged_df (as some paired samples can share the same germline sample)
			#to record info from 2nd G file for rows in tumor2 that were not in tumor1 and so not in 1st G file 
			df[file1 + "_Var[Fwd,Rev]"] = df[file1 + "_Var[Fwd,Rev]"] + "/" + df[file2 + "_Var[Fwd,Rev]"]
			df = df.drop([file2 + "_Var[Fwd,Rev]"], axis=1) #drop germline col that is repeated among samples from this file
			merged_new_df = pd.merge(merged_df,df, on= ["SV_Position","SV_length","SV_type","REF","ALT","Gene_name","Gene_count","Tx","Tx_start","Tx_end","Overlapped_tx_length","Overlapped_CDS_length","Overlapped_CDS_percent","Frameshift","Exon_count","Location","Location_Type","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"], how= "outer")

			#to record info from 2nd G file for rows in tumor2 that were not in tumor1 and so not in 1st G file 
			for i in range(0,merged_new_df.shape[0]):
				row  = merged_new_df.loc[i]
				if str(row[file1 + "_Var[Fwd,Rev]"]) != "nan": #if tumor sample for that locus is filled
					if str(row[file2 + "_Var[Fwd,Rev]"]) == "nan": #if germline sample for that locus is filled
						row[file2 + "_Var[Fwd,Rev]"] = row[file1 + "_Var[Fwd,Rev]"].split("/")[1]
						row[file1 + "_Var[Fwd,Rev]"] = row[file1 + "_Var[Fwd,Rev]"].split("/")[0]
					else:
						row[file1 + "_Var[Fwd,Rev]"] = row[file1 + "_Var[Fwd,Rev]"].split("/")[0]  

		else:
			merged_new_df = pd.merge(merged_df,df, on= ["SV_Position","SV_length","SV_type","REF","ALT","Gene_name","Gene_count","Tx","Tx_start","Tx_end","Overlapped_tx_length","Overlapped_CDS_length","Overlapped_CDS_percent","Frameshift","Exon_count","Location","Location_Type","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"], how= "outer")
    
	return merged_new_df
#--------------------------------------------------------------------------------------------------------------------------------------------------------------

#Sub-function
def merge_dfs_subfunc(file_name,filter_dict,merged_df,sample_status,file1,file2):
	#read file
	df = read_file(file_name)
	#create filter_dict
	filter_dict = create_filter_dict (df, filter_dict)
	#drop cols we do not need
	df = drop_cols (df)
	#Perform merge
	merged_df = merge_dfs (merged_df,df,sample_status,file1,file2)

	return merged_df                                                                                                                                                                                                                 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------- 

#Main code
def combine_dfs (my_file_list,savepath,filter_dict,merged_df): #my file list is the list I create under MH runs
	#to merge all dfs in a run
	with open (my_file_list, 'r+') as files_list:

    		for filee in files_list:
        		files_array =  filee.strip("\n").split("|")
        		if "" in files_array:
        			#single sample
            			file1 = ''.join(files_array).strip('|')
            			file2 = ""
            			sample_status = "unpaired"
        		else:
            			#paired sample
            			file1 = files_array[0]
            			file2 = files_array[1]
            			sample_status = "paired"

        		file_name = savepath + "/" + file1 + "/" + file1 + ".xlsx"
			print file_name

			merged_df = merge_dfs_subfunc(file_name,filter_dict,merged_df,sample_status,file1,file2)
			#merged_df.to_csv(str(counter)+ ".xlsx", sep= "\t", index=None) #use this for testing

	return merged_df
