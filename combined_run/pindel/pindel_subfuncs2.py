#note that create_count-col func can take pretty long, 20 mins for 10 samples etc.
import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Filter scores
#------------------------------------------------------
#Inner func
def set_filters (filter_list):
	#Find the unique values of filter col across all samples for a particular position
	long_str = ";".join(filter_list)
	unique = sorted(list(set(long_str.split(";"))))
	return ";".join(unique)
#------------------------------------------------------
def add_filter (filter_dict,merged_df):
		filter_col = {key:set_filters(value) for key,value in filter_dict.items()}
		#Add back the filter col to the final output
		merged_df['Filter'] = merged_df["Primary_key"].map(filter_col)
		
		return merged_df


#Tidy up the output
#Re-orderfing the cols
def re_order_columns (ordered_col_list,output):
	ordered_col_list = [x + "_Var[Fwd,Rev]" for x in ordered_col_list]
	common_column_names = ["SV_Position","SV_length","SV_type","REF","ALT","Gene_name","Gene_count","Tx","Tx_start","Tx_end","Overlapped_tx_length","Overlapped_CDS_length","Overlapped_CDS_percent","Frameshift","Exon_count","Location","Location_Type","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]
	column_names = common_column_names + ordered_col_list
	output = output[column_names]
	
	return output

#Create a new "Count" col
def create_count_col (ordered_col_list,output) :
	#ONLY count cols in which the number of variant reads exceed 0 reads (forward and reverse reads)
	sample_list = [x + "_Var[Fwd,Rev]" for x in ordered_col_list]
	i = 0
	while i < output.shape[0]:
		row  = output.loc[i]
		count = 0 
		for col in sample_list:
			if str(row[col]) != "nan" and int(row[col].split("[")[0]) > 0:
				count = count + 1 
		output.loc[i,"Count"] = str(count)
		i += 1

	return output

#Sort the df by gene and position
def sort_df(output):
	output["SV_start"] = output["SV_Position"].apply (lambda x: x.split(":")[1].split("-")[0])
	output.sort_values(by=["Gene_name", "SV_start"], inplace=True)
	output.drop("SV_start", axis =1 ,inplace= True)
	
	return output
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Main Code
def tidy_output(ordered_col_list,output,filter_dict):
	output["Primary_key"] =  output["SV_Position"] + "_" + output["REF"] + "_" + output["ALT"] 
	#Add in filter col
	output = add_filter (filter_dict,output)
	#Re-order cols
	output = re_order_columns (ordered_col_list,output)
	#Add in the count col
	output = create_count_col (ordered_col_list,output)
	#Lastly, sort df by genes and position
	final_output = sort_df(output)
		
	return final_output
