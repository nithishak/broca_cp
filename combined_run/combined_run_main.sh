set -x -e
RUN=$1
RUN_RESULTS=$2

#variables used
#COMBINE_QC_METRICS
#COMBINE_QC_EXONS
#COMBINE_QC_GENES
#COMBINE_INDELSNPS
#COMBINE_GRIDSS
#COMBINE_PINDEL
#COMBINE_MSINGS
#FINAL_OUTPUT_SCRIPT

############################################################################CHECKS#####################################################################################
#Parameter check
if [ "$#" -ne 2 ]; then
        echo "Not all parameters were provided"
        exit 1
fi


#File check
IFS="|"
FILES="$COMBINE_HS_METRICS|$COMBINE_QC_METRICS|$COMBINE_QC_EXONS|$COMBINE_QC_GENES|$COMBINE_INDELSNPS|$COMBINE_GRIDSS|$COMBINE_PINDEL|$COMBINE_MSINGS"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$RUN_RESULTS"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#######################################################################################################################################################################
#Main code

combine_program_outputs () {
	#For QC metrics
	python $COMBINE_QC_METRICS $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For QC exons
	python $COMBINE_QC_EXONS $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For QC genes
	python $COMBINE_QC_GENES $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For indelSNP AND indelSNP_BROCA
	python $COMBINE_INDELSNPS $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For gridss
	python $COMBINE_GRIDSS $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For pindel
	python $COMBINE_PINDEL $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
	#For msings
	python $COMBINE_MSINGS $LIST_OF_SAMPLES $LIST_OF_ORDERED_SAMPLES $SAVEROOT $RUN_RESULTS
}


final_output () {
	echo $LIST_OF_ORDERED_SAMPLES
	NO_OF_SAMPLES=$(wc -l < $LIST_OF_ORDERED_SAMPLES)
	python $FINAL_OUTPUT_SCRIPT $RUN $SAVEROOT $SAVEROOT/${RUN}_final_output/${RUN}.xlsx $NO_OF_SAMPLES
}


remove_files () {
	#remove intermediate files like combined_QCMetrics.txt, combined_indelSNP.txt etc. (combined pages for all samples in a run for each tab)
	rm $RUN_RESULTS/*.txt
}


combine_program_outputs
final_output
remove_files
