import pandas as pd
import sys

#Inner funcs for merge files
#--------------------------------------------------------------------------------------------------------------------------------------------------------------
def read_file(file_name):
	df =  pd.read_excel(file_name, sheet_name="QC_exons")
	return df

def rename_cols (df,sample_status, file1,file2):
	#drop unneeded cols
	df = df.drop(["Segment_Position"], axis =1)

	#Rename cols with proper col names
	if sample_status == "paired":
		df =  df.rename (columns = {"Avg_Read_Depth_T":file1, "Avg_Read_Depth_G": file2})
	else:
		df = df.rename(columns = {"Avg_Read_Depth": file1})
		
	return df

def concat_dfs (df, merged_df, sample_status,file2):
	if merged_df.shape[0] == 0:
		merged_df = df
	else:
		if sample_status == "paired" and file2 in merged_df.columns: #if germline sample is already in the merged_df (as some paired samples can share the same germline sample)
			df = df.drop([file2], axis=1) #drop germline col that is repeated among samples from this file
			merged_df = pd.merge(df, merged_df, on = ["Position","Gene","Exon_Number"], how = "inner")
		else: 
			merged_df = pd.merge(df, merged_df, on = ["Position","Gene","Exon_Number"], how = "inner")
	return merged_df

#-------------------------------------------------------------------------------------------------------------------------------------------------------------- 
#Sub-function

def merge_dfs_subfunc (file_name,file1,file2,sample_status,merged_df):
	#read file
	df = read_file(file_name)
	#drop and rename cols
	df =  rename_cols(df,sample_status,file1,file2)
	#concat dfs
	merged_df = concat_dfs (df,merged_df,sample_status,file2)
	
	return merged_df
#--------------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code

def combine_dfs (my_file_list,savepath,merged_df):
	#to merge all dfs in a run
	with open (my_file_list, 'r+') as files_list:

		for filee in files_list:
			files_array =  filee.strip("\n").split("|")
			if "" in files_array:
				#single sample
				file1 = ''.join(files_array).strip('|')
				file2 = ""
				sample_status = "unpaired"
			else:
				#paired sample
				file1 = files_array[0]
				file2 = files_array[1]
				sample_status = "paired"

			file_name = savepath + "/" + file1 + "/" + file1 + ".xlsx"
			print file_name

			merged_df = merge_dfs_subfunc(file_name,file1,file2,sample_status,merged_df)


	return merged_df
