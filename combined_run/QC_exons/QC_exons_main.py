import sys
import os
import pandas as pd
import QC_exons_subfuncs1 as subf1
import QC_exons_subfuncs2 as subf2

MH_FILE_LIST = sys.argv[1]
ORDERED_COLS = sys.argv[2]
SAVEPATH = sys.argv[3]
RUN_OUTPUT_PATH = sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
	print "CHECK: Not all python args provided"
	sys.exit (1)

#File check
files=[MH_FILE_LIST,ORDERED_COLS]
for filee in files:
	try:
		with open(filee) as f:
			pass
	except IOError as e:
		print "ERROR:Unable to open file" #Does not exist OR no read permissions
		sys.exit(1)

#Dir check
	if os.path.exists(SAVEPATH) == False or os.path.exists(RUN_OUTPUT_PATH) == False:
		print "ERROR:One of required directories not present"
		sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Main code
#Create a merged df skeleton
merged_df = pd.DataFrame()
#Get an ordered list of cols we want to see in final output
with open (ORDERED_COLS, 'r+') as ordered_col_file:
    ordered_col_list = [line.rstrip("\r\n") for line in ordered_col_file]   

merged_df = subf1.combine_dfs (MH_FILE_LIST,SAVEPATH,merged_df)
final_output = subf2.tidy_output(ordered_col_list,merged_df)
#save to file
output_file = RUN_OUTPUT_PATH + "/combined_QCexons.txt"
final_output.to_csv(output_file, sep= "\t", index=False)             
