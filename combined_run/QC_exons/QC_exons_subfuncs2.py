import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Tidy up the output

def re_order_columns (ordered_col_list,output):
	#Re-ordering the cols
	common_column_names = ["Position","Gene","Exon_Number"]
	column_names = common_column_names + ordered_col_list  
    	output = output[column_names]                                                                                                                                                                                                            
	return output

def round_up(ordered_col_list,output):
	#change certain cols to numeric and round off avg no of reads to nearest int
	output[ordered_col_list] = output[ordered_col_list].round().astype(int)
	
	return output
	
#-----------------------------------------------------------------------------------------------------------------------------------------------------------

#Main code
def tidy_output(ordered_col_list, output):
	#Re-order cols
	output = re_order_columns (ordered_col_list,output)
	#Round off all sample avg reads to whole numbers
	final_output = round_up(ordered_col_list,output)
	
	return final_output
