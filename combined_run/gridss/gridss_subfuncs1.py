import pandas as pd
import sys

#Inner funcs for merge files
#--------------------------------------------------------------------------------------------------------------------------------------------------------------
def read_file(file_name):
	df =  pd.read_excel(file_name, sheet_name="gridss",dtype="str",na_filter = False)
	return df

##################IF PAIRED SAMPLE################################
def create_somatic_df (df,file1):
	#split this df into somatic and germline - ignore ANNOTSV ranking col, the main file does NOT need this col
	somatic_df = df[["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "Type","QUAL(somatic)", "FILTER(somatic)","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]]
	#delete rows blank for QUAL(somatic) - to make sure you ONLY have somatic rows
	for i in range(0, somatic_df.shape[0]):
                line = somatic_df.loc[i,:]
                if line["QUAL(somatic)"] == "":
			somatic_df.drop(i, inplace=True)
	#Rename cols
	somatic_df = somatic_df.rename(columns = {"QUAL(somatic)": "QUAL", "FILTER(somatic)": "FILTER"})
	#Add a col to identify this df during merge
	somatic_df[file1] = somatic_df["QUAL"]
	
	return somatic_df

def create_germline_df (df, file2):
	germline_df =  df[["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "Type","QUAL(germline)", "FILTER(germline)","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]]
	#delete rows blank for QUAL(germline) - to make sure you ONLY have germline rows
	for i in range(0, germline_df.shape[0]):
		line = germline_df.loc[i,:]
		if line["QUAL(germline)"] == "":
			germline_df.drop(i, inplace=True)
	#Rename cols
	germline_df = germline_df.rename(columns = {"QUAL(germline)": "QUAL", "FILTER(germline)": "FILTER"})
	#Add a col to identify this df during merge
	germline_df[file2] = germline_df["QUAL"]
	
	return germline_df

##################IF SINGLE SAMPLE################################
def create_unpaired_df(df, file1):
	#Add a col to identify this df during merge
	df[file1] = df["QUAL"]	
	return df

###############COMMON FUNCTIONS FOR PAIRED/UNPAIRED################
####QUALITY SCORES#######
def qual_int (df):
	#Reset  index after previous filters - this CREATES a col called "index"
	df = df.reset_index()
	df['QUAL'] = df['QUAL'].apply(pd.to_numeric)
	return df

def create_qual_dict (df, qual_dict):
	current_dict = dict(zip(df["Event1"] + "_" + df["Event2"]  + "_" + df["Gene1"] + "_" + df["Gene2"], df["QUAL"]))
	for k, v in current_dict.items():
		qual_dict[k].append(v)
	return qual_dict    

####FITLER SCORES####
def create_filter_dict (df, filter_dict):
	current_dict = dict(zip(df["Event1"] + "_" + df["Event2"]  + "_" + df["Gene1"] + "_" + df["Gene2"],df["FILTER"]))
	for k,v in current_dict.items():
		filter_dict[k].append(v)
	return filter_dict

####DROP COLS####
def drop_cols (df):
	df.drop(["index","QUAL", "FILTER"], axis=1, inplace = True)
	return df
	
####MERGE####
def merge_dfs (merged_df,df,type_of_df,file2):
	if merged_df.shape[0] == 0:
        	merged_new_df = df
	else:
		if type_of_df == "germline" and file2 in merged_df.columns: #if germline sample from PAIRED sample is already in the merged_df (as some paired samples can share the same germline sample)
			merged_new_df = merged_df
		else:
			merged_new_df = pd.merge(merged_df,df, on= ["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "Type","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"], how= "outer")

    		
	return merged_new_df
#--------------------------------------------------------------------------------------------------------------------------------------------------------------

#Sub-function
def merge_dfs_subfunc(file_name,file1,file2,sample_status,qual_dict,filter_dict,merged_df):
	if sample_status == "paired":
		#read file
		df = read_file(file_name)
		somatic_df = create_somatic_df (df,file1)
		germline_df = create_germline_df (df, file2)
	
		#make Qual col numeric
		somatic_df = qual_int(somatic_df)
		germline_df = qual_int(germline_df)
		#create qual_dict
		qual_dict = create_qual_dict(somatic_df, qual_dict)
		qual_dict = create_qual_dict(germline_df, qual_dict)
		#create filter_dict
		filter_dict = create_filter_dict (somatic_df, filter_dict)
		filter_dict = create_filter_dict (germline_df, filter_dict)
		#Drop cols
		somatic_df = drop_cols(somatic_df)
		germline_df = drop_cols (germline_df)		
		#Perform merge
		merged_df = merge_dfs (merged_df, somatic_df,"somatic",file2)
		merged_df = merge_dfs (merged_df, germline_df,"germline",file2)

	else: #for unpaired sample
		#read file
		df = read_file(file_name)
		df = create_unpaired_df(df,file1)
		#make Qual col numeric
		df = qual_int(df)
		#create qual_dict
		qual_dict = create_qual_dict(df, qual_dict)
		#create filter_dict
		filter_dict = create_filter_dict (df, filter_dict)
		#Drop cols
		df = drop_cols(df)
		#Perform merge
		merged_df = merge_dfs (merged_df,df,"unpaired",file2)

	return merged_df                                                                                                                                                                                                                 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------- 

#Main code
def combine_dfs (my_file_list,savepath,merged_df,qual_dict,filter_dict): 
	#to merge all dfs in a run
	with open (my_file_list, 'r+') as files_list:

    		for filee in files_list:
        		files_array =  filee.strip("\n").split("|")
        		if "" in files_array:
        			#single sample
            			file1 = ''.join(files_array).strip('|')
            			file2 = ""
            			sample_status = "unpaired"
        		else:
            			#paired sample
            			file1 = files_array[0]
            			file2 = files_array[1]
            			sample_status = "paired"

        		file_name = savepath + "/" + file1 + "/" + file1 + ".xlsx"
			print file_name

			merged_df = merge_dfs_subfunc(file_name,file1,file2,sample_status,qual_dict,filter_dict,merged_df)

	return merged_df
