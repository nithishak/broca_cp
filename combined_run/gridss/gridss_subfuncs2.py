import sys

#Inner funcs
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Quality scores
#Find min and max qual across all samples for a particular position
#------------------------------------------------------
def add_qual (qual_dict,merged_df):
	#For min qual
        qual_col_min = {key:str(min(value)) for (key,value) in qual_dict.items()}
        #Add back the Qual min scores to the final output
        #MAKE SURE TO MAP so that the positions match
        merged_df["QUAL_min"] = merged_df["Primary_key"].map(qual_col_min)

	#For max qual     
	qual_col_max = {key:str(max(value)) for (key,value) in qual_dict.items()}
	merged_df["QUAL_max"] = merged_df["Primary_key"].map(qual_col_max)
        
        return merged_df


#Filter scores
#------------------------------------------------------
#Inner func
def set_filters (filter_list):
    #Find the unique values of filter col across all samples for a particular position
    #sort is needed as some show filter as "PASS,NMxxxx", "NMxxx,PASS" but both are the same!
    long_str = ";".join(filter_list)
    unique = sorted(list(set(long_str.split(";"))))
    return ";".join(unique)
#------------------------------------------------------
def add_filter (filter_dict,merged_df):
        filter_col = {key:set_filters(value) for key,value in filter_dict.items()}
        #Add back the filter col to the final output
        merged_df['Filter'] = merged_df["Primary_key"].map(filter_col)
        
        return merged_df


#Tidy up the output
#Re-orderfing the cols
def re_order_columns (ordered_col_list,output):
    common_column_names = ["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "Type","QUAL_min", "QUAL_max", "Filter","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]
    column_names = common_column_names + ordered_col_list
    output = output[column_names]
    
    return output

#Create a new "Count" col
def create_count_col (ordered_col_list,output) :
	#ONLY count cols in which are filled meaning that that sample has a SV going on
	i = 0
	while i < output.shape[0]:
		row  = output.loc[i]
		count = 0
		for col in ordered_col_list:
			if str(row[col]) != "nan":
				count = count + 1
		output.loc[i,"Count"] = str(count)
		i += 1

	return output

#Sort the df by gene and position
def sort_df(output):
	output["POS"] = output["Event1"].apply (lambda x: x.split(":")[1])
	output.sort_values(by=["Gene1", "POS"], inplace=True)
	output.drop("POS", axis=1,inplace=True)
	
	return output
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
#Main Code
def tidy_output(ordered_col_list,output,qual_dict,filter_dict):
	output["Primary_key"] =  output["Event1"] + "_" + output["Event2"]  + "_" + output["Gene1"] + "_" + output["Gene2"] 
	#Add in quality col
	output = add_qual (qual_dict,output)
	#Add in filter col
	output = add_filter (filter_dict,output)
	#Re-order cols
	output = re_order_columns (ordered_col_list,output)
	#Add in the count col
	output = create_count_col (ordered_col_list,output)
	#Lastly, sort df by genes and position
	final_output = sort_df(output)
        
	return final_output
