#!/bin/bash 
#Always make sure this is first line or else line 162 will error out saying bad substitution

TUMOR_PFX=$1
GERMLINE_PFX=$2 
RUN=$3
RUN_TYPE=$4
source paths/paired_paths.sh
source paths/common_paths.sh $RUN $RUN_TYPE

set -x -e

#create subdirectory for this tumor sample
SAVEPATH=$SAVEROOT/$TUMOR_PFX
mkdir -p $SAVEPATH

#create a dir within SAVEPATH for RESULTS from each program that runs PER tumor sample pair (dir of FINAL OUTPUTS)
mkdir -p $SAVEPATH/results

####################################################################################CHECK THAT ALL FILES NEEDED ABOVE EXIST###########################################################################
#Parameter check
if [ "$#" -ne 4 ]; then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BBMERGE|$CUTADAPT|$BWA|$REF_GENOME_b37|$SAMTOOLS|$PICARD|$GATK|$BEDFILE_0B|$BEDFILE_0B_genes|$BROCA_GENES|$REFSEQ|$QC_METRICS_SCRIPT|$QC_EXON_GENE_SCRIPT|$INDEL_SCRIPT|$INDEL_POST_PROCESSING_SCRIPT|$GRIDSS_SCRIPT|$PINDEL_SCRIPT|$MSINGS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$VARIANT_FILES_b37|$SAVEPATH|$SAVEPATH/results"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
########################################################################################################################################################################################################

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#FOR EACH TUMOR AND GERMLINE FROM A TUMOR-GERMLINE PAIRED SAMPLE OF A RUN:
#########
#PART1#
#########

find_fastqs () {
	#King lab provides .txt.gz files while MedGenome provides us with fastq.gz files 
	#Find tumor sequences
	TUMOR_R1_old=$(find $DATAPATH/$TUMOR_PFX/ | grep -E '*_1_sequence.txt.gz|*_R1.fastq.gz') 
	TUMOR_R2_old=$(find $DATAPATH/$TUMOR_PFX/ | grep -E '*_2_sequence.txt.gz|*_R2.fastq.gz')

	#Find normal sequences
	GERMLINE_R1_old=$(find $DATAPATH/$GERMLINE_PFX/ | grep -E '*_1_sequence.txt.gz|*_R1.fastq.gz')
	GERMLINE_R2_old=$(find $DATAPATH/$GERMLINE_PFX/ | grep -E '*_2_sequence.txt.gz|*_R2.fastq.gz')
}

bbmerge () {
	mkdir -p $SAVEPATH/bbmap
	$BBMERGE -Xmx1g -ignorejunk in1=$GERMLINE_R1_old  in2=$GERMLINE_R2_old outa=$SAVEPATH/bbmap/${GERMLINE_PFX}_G_adapters.fa itn 
	$BBMERGE -Xmx1g -ignorejunk in1=$TUMOR_R1_old  in2=$TUMOR_R2_old outa=$SAVEPATH/bbmap/${TUMOR_PFX}_T_adapters.fa itn
}

obtain_adapters () {
        #Obtain forward and reverse adapters from tumor and germline sample to use for CUTADAPT. Obtain this from previous BBMERGE output!
        FP_GERMLINE=`sed '2q;d' $SAVEPATH/bbmap/${GERMLINE_PFX}_G_adapters.fa`
        RP_GERMLINE=`sed '4q;d' $SAVEPATH/bbmap/${GERMLINE_PFX}_G_adapters.fa`

        FP_TUMOR=`sed '2q;d' $SAVEPATH/bbmap/${TUMOR_PFX}_T_adapters.fa`
        RP_TUMOR=`sed '4q;d' $SAVEPATH/bbmap/${TUMOR_PFX}_T_adapters.fa`
	echo "OBTAINING PRIMERS done...."
}

renaming_files () {
	if [[ "$TUMOR_R1_old" == *"txt.gz"* ]];then
		#Rename file names (.fastq prefix needed for cutadapt)
		replace="fastq.gz"

		TUMOR_R1=${TUMOR_R1_old/txt.gz/$replace}
		cp $TUMOR_R1_old $TUMOR_R1
		TUMOR_R2=${TUMOR_R2_old/txt.gz/$replace}
		cp $TUMOR_R2_old $TUMOR_R2

		GERMLINE_R1=${GERMLINE_R1_old/txt.gz/$replace}
		cp $GERMLINE_R1_old $GERMLINE_R1
		GERMLINE_R2=${GERMLINE_R2_old/txt.gz/$replace}
		cp $GERMLINE_R2_old $GERMLINE_R2
	else
		TUMOR_R1=$TUMOR_R1_old
		TUMOR_R2=$TUMOR_R2_old

		GERMLINE_R1=$GERMLINE_R1_old
		GERMLINE_R2=$GERMLINE_R2_old
	fi
	echo "RENAMING FILES done....."
}	

cutadapt () {
	#CUTADAPT
	#https://cutadapt.readthedocs.io/en/stable/guide.html (Trimming paired-end reads???)
	#remove sequence adapters NOTE: adapters are different from PCR primers!!
	echo "CUTADAPT starting...."
	$CUTADAPT -a $FP_GERMLINE -A $RP_GERMLINE -o $SAVEPATH/bbmap/s_2_1_sequence_trimmedG.fastq.gz -p $SAVEPATH/bbmap/s_2_2_sequence_trimmedG.fastq.gz $GERMLINE_R1 $GERMLINE_R2
	$CUTADAPT -a $FP_TUMOR -A $RP_TUMOR -o $SAVEPATH/bbmap/s_2_1_sequence_trimmedT.fastq.gz -p $SAVEPATH/bbmap/s_2_2_sequence_trimmedT.fastq.gz $TUMOR_R1 $TUMOR_R2
	echo "CUTADAPT done...."
}

bwa_mem () {	
	#BWA ALIGNMENT
	mkdir -p $SAVEPATH/bwa
	echo "BWA MEM starting...."
	$BWA mem -t $CORES -M -R "@RG\tID:G\tPL:ILLUMINA\tPU:NA\tLB:null\tSM:${GERMLINE_PFX}" $REF_GENOME_b37 $SAVEPATH/bbmap/s_2_1_sequence_trimmedG.fastq.gz $SAVEPATH/bbmap/s_2_2_sequence_trimmedG.fastq.gz > $SAVEPATH/bwa/${GERMLINE_PFX}_G.sam
	$BWA mem -t $CORES -M -R "@RG\tID:G\tPL:ILLUMINA\tPU:NA\tLB:null\tSM:${TUMOR_PFX}" $REF_GENOME_b37 $SAVEPATH/bbmap/s_2_1_sequence_trimmedT.fastq.gz $SAVEPATH/bbmap/s_2_2_sequence_trimmedT.fastq.gz > $SAVEPATH/bwa/${TUMOR_PFX}_T.sam
	echo "BWA MEM done...."
}


samtools () {
	#version 1.9 versions -(http://www.htslib.org/doc/) specific link-(http://www.htslib.org/doc/samtools.html)
	#SAMTOOLS to view (change to bam file) and sort
	mkdir -p $SAVEPATH/samtools 
	$SAMTOOLS view -bSu -F 4 -q 20 $SAVEPATH/bwa/${GERMLINE_PFX}_G.sam | $SAMTOOLS sort - -o $SAVEPATH/samtools/${GERMLINE_PFX}_G_sorted.bam
	$SAMTOOLS view -bSu -F 4 -q 20 $SAVEPATH/bwa/${TUMOR_PFX}_T.sam | $SAMTOOLS sort - -o $SAVEPATH/samtools/${TUMOR_PFX}_T_sorted.bam
}

picard_index_bam () {
        java -jar $PICARD BuildBamIndex I=$SAVEPATH/samtools/${GERMLINE_PFX}_G_sorted.bam
        java -jar $PICARD BuildBamIndex I=$SAVEPATH/samtools/${TUMOR_PFX}_T_sorted.bam
}

picard_metrics () {
	mkdir -p $SAVEPATH/picard
	java -jar $PICARD CollectInsertSizeMetrics I=$SAVEPATH/samtools/${GERMLINE_PFX}_G_sorted.bam O=$SAVEPATH/picard/${GERMLINE_PFX}_G_insert_metrics.txt H= $SAVEPATH/picard/${GERMLINE_PFX}_G_insert_metrics_histogram.pdf
	java -jar $PICARD CollectInsertSizeMetrics I=$SAVEPATH/samtools/${TUMOR_PFX}_T_sorted.bam O=$SAVEPATH/picard/${TUMOR_PFX}_T_insert_metrics.txt H= $SAVEPATH/picard/${TUMOR_PFX}_T_insert_metrics_histogram.pdf
}


############# LAB ANALYSIS FILE ########################
picard_deduplicates_mark () {
	#This is the version the lab wants for analysis - dups marked but not removed
	java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${GERMLINE_PFX}_G_sorted.bam O=$SAVEPATH/picard/${GERMLINE_PFX}_G_marked_duplicates.bam REMOVE_DUPLICATES=false M=$SAVEPATH/picard/marked_dup_metrics_${GERMLINE_PFX}_G.txt CREATE_INDEX=true
	java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${TUMOR_PFX}_T_sorted.bam O=$SAVEPATH/picard/${TUMOR_PFX}_T_marked_duplicates.bam REMOVE_DUPLICATES=false M=$SAVEPATH/picard/marked_dup_metrics_${TUMOR_PFX}_T.txt CREATE_INDEX=true
	echo "PICARD MARKDUPLICATES done...."
}


gatk_recalibration_no_bedfile () {
	#This is the version the lab wants for analysis - recalibration wihthout filtering with bedfile
	mkdir -p $SAVEPATH/recal
	echo "GATK BASE RECALIBRATOR starting without bedfile...."
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${GERMLINE_PFX}_G_marked_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -O $SAVEPATH/recal/${GERMLINE_PFX}_G_recal_labv.table
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${TUMOR_PFX}_T_marked_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -O $SAVEPATH/recal/${TUMOR_PFX}_T_recal_labv.table
	echo "GATK BASE RECALIBRATOR done without bedfile...."

	echo "CREATION OF FINAL BAMS starting without bedfile...."
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${GERMLINE_PFX}_G_marked_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${GERMLINE_PFX}_G_recal_labv.table -O $SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G_labv.bam
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${TUMOR_PFX}_T_marked_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${TUMOR_PFX}_T_recal_labv.table -O $SAVEPATH/recal/bqsr_${TUMOR_PFX}_T_labv.bam
	echo "FINAL BAMS creation done without bedfile...."
	
}

QC_metrics () {
	#Twist's QC is based on a bam that is sorted but not filtered using a bedfile or duplicated reads (these are marked but not removed)
	#To match their analysis, we are doing the same here

        #this is to create interval list for BAITS used by QC_metrics
        java -jar $PICARD BedToIntervalList \
        I=$BEDFILE_0B_BAITS \
        O=$SAVEPATH/recal/baits.interval_list \
        SD=$REF_GENOME_b37

        #this is to create interval list for TARGETS used by QC_metrics
        java -jar $PICARD BedToIntervalList \
        I=$BEDFILE_0B \
        O=$SAVEPATH/recal/targets.interval_list \
        SD=$REF_GENOME_b37

        #For germline
        #This command requires an .interval_list file as target and bait files.
        java -jar $PICARD CollectHsMetrics \
        I=$SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G_labv.bam \
        O=$SAVEPATH/picard/${GERMLINE_PFX}_G_hs_metrics.txt \
        R=$REF_GENOME_b37 \
        BAIT_INTERVALS=$SAVEPATH/recal/baits.interval_list \
        TARGET_INTERVALS=$SAVEPATH/recal/targets.interval_list

        #For tumor
        #This command requires an .interval_list file as target and bait files.
        java -jar $PICARD CollectHsMetrics \
        I=$SAVEPATH/recal/bqsr_${TUMOR_PFX}_T_labv.bam \
        O=$SAVEPATH/picard/${TUMOR_PFX}_T_hs_metrics.txt \
        R=$REF_GENOME_b37 \
        BAIT_INTERVALS=$SAVEPATH/recal/baits.interval_list \
        TARGET_INTERVALS=$SAVEPATH/recal/targets.interval_list
}

############# LAB ANALYSIS FILE ########################


#We are re-running picard and recalibration here to remove duplicates and filter using bedfiles so that we can give clean bams to the downstream processing tools

picard_deduplicates_remove () {
        java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${GERMLINE_PFX}_G_sorted.bam O=$SAVEPATH/picard/${GERMLINE_PFX}_G_no_duplicates.bam REMOVE_DUPLICATES=true M=$SAVEPATH/picard/removed_dup_metrics_${GERMLINE_PFX}_G.txt CREATE_INDEX=true
        java -jar $PICARD MarkDuplicates I=$SAVEPATH/samtools/${TUMOR_PFX}_T_sorted.bam O=$SAVEPATH/picard/${TUMOR_PFX}_T_no_duplicates.bam REMOVE_DUPLICATES=true M=$SAVEPATH/picard/removed_dup_metrics_${TUMOR_PFX}_T.txt CREATE_INDEX=true
	echo "PICARD REMOVEDUPLICATES done...."
}

gatk_recalibration_bedfile () {
	#Version GATK 4.8.0.1 (https://software.broadinstitute.org/gatk/documentation/tooldocs/4.0.8.1/org_broadinstitute_hellbender_tools_walkers_bqsr_BaseRecalibrator.php)
	#Explanation of what this does -https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_bqsr_BaseRecalibrator.php
	#Various positions for -L tried, -L 1 works not -L chr1. Placement in command crucial. Not enough documentation
        #https://software.broadinstitute.org/gatk/documentation/article?id=11009 (L arg GATK for interval list) (should be 0 based bed file) (.bed in itself means 0 based)
        #https://gatkforums.broadinstitute.org/gatk/discussion/4133/when-should-i-use-l-to-pass-in-a-list-of-intervals
	echo "GATK BASE RECALIBRATOR starting with bedfile...."
	mkdir -p $SAVEPATH/recal
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${GERMLINE_PFX}_G_no_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -L $BEDFILE_0B -O $SAVEPATH/recal/${GERMLINE_PFX}_G_recal.table
	$GATK BaseRecalibrator -R $REF_GENOME_b37 -I $SAVEPATH/picard/${TUMOR_PFX}_T_no_duplicates.bam --known-sites $VARIANT_FILES_b37/dbsnp_138.b37.vcf --known-sites $VARIANT_FILES_b37/Mills_and_1000G_gold_standard.indels.b37.vcf --known-sites $VARIANT_FILES_b37/1000G_phase1.indels.b37.vcf -L $BEDFILE_0B -O $SAVEPATH/recal/${TUMOR_PFX}_T_recal.table
	echo "GATK BASE RECALIBRATOR done...."

	#GATK to apply recalibration results to bam files
	#FINAL BAM FILES
	#LOOKS LIKE THIS CREATES AN UPDATED BAI FILE AS WELL!! #https://github.com/broadinstitute/gatk/issues/4219, because optional common argument --create-output-bam-index is by default true!!
	echo "CREATION OF FINAL BAMS starting...."
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${GERMLINE_PFX}_G_no_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${GERMLINE_PFX}_G_recal.table -L $BEDFILE_0B -O $SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G.bam
	$GATK ApplyBQSR -R $REF_GENOME_b37 -I $SAVEPATH/picard/${TUMOR_PFX}_T_no_duplicates.bam --bqsr-recal-file $SAVEPATH/recal/${TUMOR_PFX}_T_recal.table -L $BEDFILE_0B -O $SAVEPATH/recal/bqsr_${TUMOR_PFX}_T.bam
	echo "FINAL BAMS creation done...."
}


#########
#PART2#
#########

qc_metrics () {
	#For 1.QC metrics
	$QC_METRICS_SCRIPT $SAVEPATH $TUMOR_PFX $GERMLINE_PFX $SAVEPATH/picard/${GERMLINE_PFX}_G_hs_metrics.txt $SAVEPATH/picard/${TUMOR_PFX}_T_hs_metrics.txt $SAVEPATH/picard/removed_dup_metrics_${GERMLINE_PFX}_G.txt $SAVEPATH/picard/removed_dup_metrics_${TUMOR_PFX}_T.txt $SAVEPATH/picard/${GERMLINE_PFX}_G_insert_metrics.txt $SAVEPATH/picard/${TUMOR_PFX}_T_insert_metrics.txt
}

qc_exon_gene () {
	#For 2.QC per exon or per gene
	$QC_EXON_GENE_SCRIPT $TUMOR_PFX $GERMLINE_PFX $SAVEPATH 
}

indelSNP () {
	#For 2.Varscan and 3.Vardict #uses ANNOVAR for annotation
	$INDEL_SCRIPT $SAVEPATH $TUMOR_PFX $GERMLINE_PFX
	$INDEL_POST_PROCESSING_SCRIPT $TUMOR_PFX $GERMLINE_PFX $SAVEPATH
}

gridss () {
	#For 4.Gridss #uses ANNOTSV for annotation
	$GRIDSS_SCRIPT $SAVEPATH $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt $TUMOR_PFX $GERMLINE_PFX
}

pindel () {
	#For 5.Pindel #uses ANNOTSV and custom script for annotation
	$PINDEL_SCRIPT $SAVEPATH $TUMOR_PFX $GERMLINE_PFX  $SAVEROOT/preferred_transcripts.txt $SAVEROOT/preferred_genes.txt  
}

msings () {
	#For 6.Msings
	$MSINGS_SCRIPT $SAVEPATH $TUMOR_PFX $GERMLINE_PFX
}


#For 7.Cnvkit
#takes in all normal bams at once to create pooled reference, so run as standalone script

#Combine all outputs
produce_final_output () {
	python $COMBINE_SCRIPT $SAVEPATH $TUMOR_PFX $GERMLINE_PFX $SAVEPATH/${TUMOR_PFX}.xlsx
}

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#####Sub-functions######
CP_read_alignment () {

        #Functions from top till bwa mem need to run TOGETHER
	find_fastqs
        bbmerge
	obtain_adapters
	renaming_files
        cutadapt
        bwa_mem
        samtools
        picard_index_bam
        picard_metrics
	#=========labv_files==========
        picard_deduplicates_mark #new bai formed
        gatk_recalibration_no_bedfile #new bai formed
	QC_metrics
	#=========labv_files==========
	picard_deduplicates_remove #new bai formed
        gatk_recalibration_bedfile #new bai formed
}

run_analysis () {
	qc_metrics
	qc_exon_gene
	indelSNP
	gridss
	pindel
	msings
	produce_final_output
}

#####Main-functions######
CP_read_alignment
run_analysis
