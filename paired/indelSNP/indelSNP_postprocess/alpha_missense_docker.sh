#!/bin/bash

vep \
	--offline \
	--cache \
	--dir_cache $VEP_CACHE_DIR \
	--refseq \
	--use_given_ref \
	--species homo_sapiens \
	--assembly GRCh37 \
	--fasta $REF_FASTA \
	--format vcf \
	--plugin AlphaMissense,file=$ALPHA_MISSENSE_TSV \
	--input_file $INPUT_VCF \
	--output_file $OUTPUT_VCF
