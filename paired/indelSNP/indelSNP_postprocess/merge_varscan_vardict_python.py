import pandas as pd
import sys

VARSCAN_G_SNP=sys.argv[1]
VARSCAN_T_SNP=sys.argv[2]
VARSCAN_G_INDEL=sys.argv[3]
VARSCAN_T_INDEL=sys.argv[4]
VARDICT=sys.argv[5]
OUTFILE=sys.argv[6]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 7 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[VARSCAN_T_SNP,VARSCAN_G_SNP,VARSCAN_T_INDEL,VARSCAN_G_INDEL,VARDICT]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

##############################VARSCAN SNPS##########################################################################################
#let us load varscan snp tumor- only pos is read in as int64, rest all object type which means str
varscan_t_snp = pd.read_csv(VARSCAN_T_SNP, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan snp tumor
varscan_t_snp['ref_reads'] = varscan_t_snp['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_t_snp['var_reads'] = varscan_t_snp['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_t_snp['TReads_R|V'] = varscan_t_snp['ref_reads'] + "|" + varscan_t_snp['var_reads']
varscan_t_snp['ref_reads'] = varscan_t_snp['ref_reads'].astype("int64")
varscan_t_snp['var_reads'] = varscan_t_snp['var_reads'].astype("int64")
varscan_t_snp['T_VAF'] = (varscan_t_snp['var_reads']/(varscan_t_snp['var_reads']+varscan_t_snp['ref_reads'])).round(2)

#let us load varscan snp germline- only pos is read in as int64, rest all object type which means str
varscan_g_snp = pd.read_csv(VARSCAN_G_SNP, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan snp germline
varscan_g_snp['ref_reads'] = varscan_g_snp['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_g_snp['var_reads'] = varscan_g_snp['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_g_snp['GReads_R|V'] = varscan_g_snp['ref_reads'] + "|" + varscan_g_snp['var_reads']
varscan_g_snp['ref_reads'] = varscan_g_snp['ref_reads'].astype("int64")
varscan_g_snp['var_reads'] = varscan_g_snp['var_reads'].astype("int64")
varscan_g_snp['G_VAF'] = (varscan_g_snp['var_reads']/(varscan_g_snp['var_reads']+varscan_g_snp['ref_reads']) ).round(2)

#outer join varscan tumor and germline SNP vcfs
varscan_SNP = pd.merge(varscan_t_snp.loc[:,["#CHROM","POS","REF","ALT","T_VAF","TReads_R|V"]],varscan_g_snp.loc[:,["#CHROM","POS","REF","ALT","G_VAF","GReads_R|V"]], on=["#CHROM","POS","REF","ALT"], how = "outer")

##############################VARSCAN INDELS##########################################################################################
#let us load varscan indel tumor- only pos is read in as int64, rest all object type which means str
varscan_t_indel = pd.read_csv(VARSCAN_T_INDEL, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan indel tumor
varscan_t_indel['ref_reads'] = varscan_t_indel['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_t_indel['var_reads'] = varscan_t_indel['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_t_indel['TReads_R|V'] = varscan_t_indel['ref_reads'] + "|" + varscan_t_indel['var_reads']
varscan_t_indel['ref_reads'] = varscan_t_indel['ref_reads'].astype("int64")
varscan_t_indel['var_reads'] = varscan_t_indel['var_reads'].astype("int64")
varscan_t_indel['T_VAF'] = (varscan_t_indel['var_reads']/(varscan_t_indel['var_reads']+varscan_t_indel['ref_reads'])).round(2)

#let us load varscan indel germline
varscan_g_indel = pd.read_csv(VARSCAN_G_INDEL, sep= "\t", header = 0, dtype = "str")
#Let us post process varscan indel germline
varscan_g_indel['ref_reads'] = varscan_g_indel['Sample1'].apply(lambda x: x.split(":")[4]) #RD
varscan_g_indel['var_reads'] = varscan_g_indel['Sample1'].apply(lambda x: x.split(":")[5]) #AD
varscan_g_indel['GReads_R|V'] = varscan_g_indel['ref_reads'] + "|" + varscan_g_indel['var_reads']
varscan_g_indel['ref_reads'] = varscan_g_indel['ref_reads'].astype("int64")
varscan_g_indel['var_reads'] = varscan_g_indel['var_reads'].astype("int64")
varscan_g_indel['G_VAF'] = (varscan_g_indel['var_reads']/(varscan_g_indel['var_reads']+varscan_g_indel['ref_reads'])).round(2)

##outer join varscan tumor and germline INDEL vcfs
varscan_INDEL = pd.merge(varscan_t_indel.loc[:,["#CHROM","POS","REF","ALT","T_VAF","TReads_R|V"]],varscan_g_indel.loc[:,["#CHROM","POS","REF","ALT","G_VAF","GReads_R|V"]], on=["#CHROM","POS","REF","ALT"], how = "outer")

##############################VARSCAN###################################################################################
########################################################################################################################
#concat results from SNP and INDEL from varscan vcfs
varscan = pd.concat([varscan_SNP,varscan_INDEL])

##############################VARDICT##########################################################################################
###############################################################################################################################
#Let us load vardict output
vardict = pd.read_csv(VARDICT, sep= "\t", header = 0, dtype = "str")
#Let us post-process vardict vcf
#For tumor
vardict["t_ref_reads"] = vardict["tumor"].apply(lambda x: x.split(":")[5]).apply(lambda x: x.split(",")[0]) #AD[0] = ADref
vardict["t_var_reads"] = vardict["tumor"].apply(lambda x: x.split(":")[5]).apply(lambda x: x.split(",")[1]) #AD[1] = ADvar
#For germline
vardict["g_ref_reads"] = vardict["germline"].apply(lambda x: x.split(":")[5]).apply(lambda x: x.split(",")[0]) #AD[0] = ADref
vardict["g_var_reads"] = vardict["germline"].apply(lambda x: x.split(":")[5]).apply(lambda x: x.split(",")[1]) #AD[1] = ADvar
#Combine info
vardict["TReads_R|V"] = vardict["t_ref_reads"] + "|" + vardict["t_var_reads"]
vardict["GReads_R|V"] = vardict["g_ref_reads"] + "|" + vardict["g_var_reads"]
#Add the VAF col
#For tumor
vardict["t_ref_reads"] = vardict["t_ref_reads"].astype("int64")
vardict["t_var_reads"] = vardict["t_var_reads"].astype("int64")
vardict["T_VAF"] = (vardict["t_var_reads"]/(vardict["t_var_reads"]+vardict["t_ref_reads"])).round(2)
#For germline
vardict["g_ref_reads"] = vardict["g_ref_reads"].astype("int64")
vardict["g_var_reads"] = vardict["g_var_reads"].astype("int64")
vardict["G_VAF"] = (vardict["g_var_reads"]/(vardict["g_var_reads"]+vardict["g_ref_reads"])).round(2)

#Let us parse out the filter, quality, Type of event, Event Status and MSI status too
vardict ["Filter(Vardict)"] = vardict["FILTER"] #Specifying (Vardict) in col name because I am not taking Varscan's Filter col, too many filter cols will cause confusion
vardict["EventStatus"] = vardict["INFO"].apply(lambda x: x.split(";")[0]).apply(lambda x: x.split("=")[1]) # parsing out STATUS= from "INFO" col
vardict["TypeOfEvent"] = vardict["INFO"].apply(lambda x: x.split(";")[2]).apply(lambda x: x.split("=")[1]) # parsing out TYPE= from "INFO" col
vardict["MSI"] = vardict["INFO"].apply(lambda x: x.split(";")[7]).apply(lambda x: x.split("=")[1]) # parsing out MSI= from "INFO" col

#################################################################################################################################
##############################SNP_INDEL##########################################################################################
#################################################################################################################################
indels = pd.merge(varscan.loc[:,["#CHROM","POS","REF","ALT","T_VAF","TReads_R|V","G_VAF","GReads_R|V"]],vardict.loc[:,["#CHROM","POS","REF","ALT","T_VAF","TReads_R|V","G_VAF","GReads_R|V","QUAL","EventStatus","TypeOfEvent","MSI","Filter(Vardict)"]], on =["#CHROM","POS","REF","ALT"], how="outer", suffixes=["(Varscan)","(Vardict)"])
#write output to file
indels.to_csv(OUTFILE,sep = "\t", index= False)
