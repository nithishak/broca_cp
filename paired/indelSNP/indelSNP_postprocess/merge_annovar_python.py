import pandas as pd
import sys

BROCA_GENES = sys.argv[1]
ANNOTATED_VCF=sys.argv[2] #Annovar annotated file
SNP_INDEL_VCF=sys.argv[3] #python merged file
BRCA1_STARITA_PRED=sys.argv[4]
MISSENSES_FAVES=sys.argv[5]
SGE=sys.argv[6] 
OUTFILE=sys.argv[7]
OUTFILE_BROCA=sys.argv[8]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 9 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[BROCA_GENES,ANNOTATED_VCF,SNP_INDEL_VCF,BRCA1_STARITA_PRED,MISSENSES_FAVES,SGE]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#Let us read in annotated vcf
annotated = pd.read_csv(ANNOTATED_VCF, sep= "\t", header = 0, dtype = "str")
#Let us load snp indel vcf
snp_indels = pd.read_csv(SNP_INDEL_VCF, sep= "\t", header = 0, dtype = "str")

#Merge both outputs
output = pd.merge(annotated,snp_indels,left_on=["Otherinfo4","Otherinfo5","Otherinfo7","Otherinfo8"], right_on=["#CHROM","POS","REF","ALT"], how = "outer")
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Post-processing of final output

#Let us first re-name some columns
output.rename(columns={"Func.refGene":"TranscriptRegion","Gene.refGene":"Gene","ExonicFunc.refGene":"VariantType"}, inplace=True)
#-----------------------------------------------------------------------------------------------------------------------------------
#Let us analyze 3 cols "GeneDetail.refGene|ExonicFunc.refGene|AAChange.refGene" in the vcf.
#Assumption made below: Transcript is ONLY made up of exons so a change in an intron is NOT reflected. Makes sense because NMxxxx, position 0000 must refer to an the actual transcript (without the introns)???

#IMP_NOTES: Look at "Func.refGene" col - If snp or indel is in EXONIC and CODING region, both  "ExonicFunc.refGene" AND "AAChange.refGene" cols are populated. [synonymous SNV|UBE2T:NM_014176:exon2:c.A15G:p.S5S eg.]
# (for the above, even if the amino acid is not changed, it is still reflected as a.a Q to Q)
#IMP_NOTES: Look at "Func.refGene" col - If event occurs in upstream/downstream/intergenic, only "GeneDetail.refGene" is populated with a "dist=xxx".
#IMP_NOTES: Look at "Func.refGene" col - If event is UTR5/UTR3/splicing, only "GeneDetail.refGene" is populated with a "NM_xxxx:c.-0000G>C eg. This indicates transcript change in non-coding region (therefore no a.a. change)
#IMP_NOTES: Look at "Func.refGene" col - If event happen in an intron, all three cols "GeneDetail.refGene|ExonicFunc.refGene|AAChange.refGene" are blank or in this script, indicated by a .

#Let us analyze 2 cols "GeneDetail.refGene|AAChange.refGene" in the vcf and create a new col "Transcripts" from them.
#If "Func.refGene" is "exonic", col "AAChange.refGene" will be populated. Anything other than an exon, the col "GeneDetail.refGene" will be populated. Only for introns, both these cols are blank.
#Only 1 of these 2 cols is populated for any given row!!! <----- this is TRUE EXCEPT when "Func.refGene" is "exonic;splicing", in this case, both cols are populated!
#So let us make a new col "Transcripts" that merge both of them!
#Please note is that in col "GeneDetail.refGene", delimiter is ";" but in col "AAChange.refGene", delimter is "," 
#So let us merge these 2 cols to make our lives easier - Remember for each row of final_indel, only ONE of these cols would be filled, the other would just have a .
#First, let us change delimiter in col "AAChange.refGene" to ";"

output["AAChange.refGene"] = output["AAChange.refGene"].apply(lambda x: x.replace(",",";"))
output["Transcripts"] = output["GeneDetail.refGene"] + ";" + output["AAChange.refGene"]
output["Transcripts"] = output["Transcripts"].apply(lambda x: x.strip(".;"))
output["Transcripts"] = output["Transcripts"].apply(lambda x: x.strip(";."))
#-----------------------------------------------------------------------------------------------------------------------------------

# Now, from the col "Transcripts", let us create a temp new col called "Preferred Transcripts"
#This means that if for a gene from the BROCA_gene list is present, and its "Transcripts" col contains many transcripts out of which the preferred transcript is there,
#we want the info that follows the preferred transcript : <exon8:c.A1056G:p.K352K> till next , or till end of cell
#For this to happen, we need to first load the BROCA_GENES list
BROCA_genes = pd.read_csv(BROCA_GENES, sep ="\t", header=0)
#lets create a dictionary with genes as the keys and their transcripts as values - let us remove version no from transcripts incase they dont match annovar transcript version
mydict = {}
for i in range(0,(BROCA_genes.shape[0])):
    row = BROCA_genes.iloc[i,:]
    mydict[row["Gene"]] = (row["partition refseq"].split("."))[0]

#Populate the preferred transcript col
for i in range(0,(output.shape[0])):
    row = output.iloc[i,:]
    transcripts_for_row = row["Transcripts"].split(";")
    genes_per_row =  row["Gene"].split(";") #a row can have geneA/geneB for eg out of which Gene B might be BROCA gene 
    for gene in genes_per_row:
	if gene in mydict.keys():
            p_transcript = mydict[gene]
            for transcript in transcripts_for_row:
                if p_transcript in transcript: #at this point, we are saying that there is a BROCA gene and that info for its preferred transcript is available
                #if row["TranscriptRegion"] == "exonic": #can't do this as we have to account for "exonic/splicing" events where exonic component comes from preferred transcript
		    if transcript.startswith("NM_"):
		        replace_str = transcript.split(":")[0] + ":"
			pre_T = transcript.replace(replace_str,"")

                    else: #these are exonic or exonic/splicing events where instead of starting with NM_..., string starts with GeneName:NM_....
                        replace_str = transcript.split(":")[0] + ":" + transcript.split(":")[1] + ":"
			pre_T = transcript.replace(replace_str,"")

		    output.loc[i,"PreferredTranscripts"] = pre_T

#From the col "Preferred Transcripts", let us create 3 new cols "PT_Exon", "PT_Transcript_Code_Change" and  "PT_Amino_Acid_Change"
#After creating these 3 cols, let us delete "Preferred Transcripts" col
#Note that if "GeneDetail.refGene" had dist= previously, then "Preferred Transcripts" is made to be blank
for i in range(0,(output.shape[0])):
    row = output.iloc[i,:]
    if str(row["PreferredTranscripts"]) == "nan":
        pass
    else:
        split_list = str(row["PreferredTranscripts"]).split(":")
        #if (len(split_list)) == 0: #this happens when col "Func.refGene" = "dist=...."
         #pass
        if len(split_list) == 1: #this happens when col "Func.refGene" = non coding part of transcript "NM...."
            output.loc[i,"PT_Exon"] = split_list[0] = None
            output.loc[i,"PT_Transcript_Code_Change"] = split_list[0]
            output.loc[i,"PT_Amino_Acid_Change"] = None
        elif len(split_list)==2: #this seems to happen for col "Func.refGene" = "splicing"
            output.loc[i,"PT_Exon"] = split_list[0]
            output.loc[i,"PT_Transcript_Code_Change"] = split_list[1]
            output.loc[i,"PT_Amino_Acid_Change"] = None
        elif len(split_list) == 3: #this happens when col "Func.refGene" = "exon"
            output.loc[i,"PT_Exon"] = split_list[0]
	    output.loc[i,"PT_Transcript_Code_Change"] = split_list[1]
            output.loc[i,"PT_Amino_Acid_Change"] = split_list[2]
    
 #Drop the col "Preferred Transcripts"	
#-----------------------------------------------------------------------------------------------------------------------------------
#Let us create a col called "Position" - and include all the info from cols "Chr", "Start" and "End"
#if this is a SNP, the position will be chrX:xxxx
#if this is an indel, the position will be chrX:xxxx-xxxxx
#Use info from Annovar
for i in range(0,(output.shape[0])):
	row = output.iloc[i,:]
	if row["Start"] == row["End"]:
		string = "chr" + row["Chr"] + ":" + row["Start"]
		#output.loc[i,"Position"] = "chr" + row["Chr"] + ":" + row["Start"]
	else:
		string = "chr" + row["Chr"] + ":" + row["Start"] + "-" + row["End"]
	
	output.loc[i,"Position"] = string

#replace gene name "ABRAXAS1" with "FAM175A". Our gene list and refseq table browser doc has "FAM175A" but ANNOVAR annotates this as "ABRAXAS1"
output['Gene'].replace('ABRAXAS1','FAM175A', inplace=True, regex=True) #regex true so that it matches ABRAXAS1;geneX too
output['Transcripts'].replace('ABRAXAS1','FAM175A', inplace=True, regex=True) 

#Sort df
output.sort_values(by=["Gene","Start"], inplace = True)
#-----------------------------------------------------------------------------------------------------------------------------------
#Add 3 custom columns - BRCA1_Starita_predictions, Missenses_Faves and SGE RAD51C  (generated by the Swisher lab)
MF = pd.read_csv(MISSENSES_FAVES, sep = "\t", header=0, dtype="str")
SP = pd.read_csv(BRCA1_STARITA_PRED, sep="\t", header=0, dtype="str")
SGE = pd.read_csv(SGE, sep="\t", header=0, dtype="str")

output_MF = output.merge(MF, left_on=["Position","Ref","Alt"], right_on=["ChrPosition","Ref","Alt"], how="left")
output_MF_SP = output_MF.merge(SP, left_on=["Position","Ref","Alt"], right_on=["ChrPosition","ref","alt"], how="left")
output_MF_SP_SGE =  output_MF_SP.merge(SGE, left_on=["Position","Ref","Alt"], right_on=["ChrPosition","ref","alt"], how="left")
#----------------------------------------------------------------------------------------------------------------------------------- 
output_lab = output_MF_SP_SGE[["Position","Ref","Alt","Gene","TranscriptRegion","TypeOfEvent","VariantType","PT_Exon","PT_Transcript_Code_Change","PT_Amino_Acid_Change","EventStatus","Filter(Vardict)","T_VAF(Vardict)","TReads_R|V(Vardict)","G_VAF(Vardict)","GReads_R|V(Vardict)","T_VAF(Varscan)","TReads_R|V(Varscan)","G_VAF(Varscan)","GReads_R|V(Varscan)","QUAL","MSI","PreferredTranscripts","Transcripts","Polyphen2_HDIV_score","SIFT_score","SIFT4G_score","REVEL","AlphaMissense_score","AlphaMissense_pred","GERP++_RS","gerp++gt2","dbscSNV_ADA_SCORE","dbscSNV_RF_SCORE","CLNALLELEID","CLNDN","CLNDISDB","CLNREVSTAT","CLNSIG","ONCDN","ONCDISDB","ONCREVSTAT","ONC","SCIDN","SCIDISDB","SCIREVSTAT","SCI","SwisherFaves","al_et_Starita_Shendure_2018_Prediction","RAD51C_SGE_functional_classification", "RAD51C_SGE_z_score","Interpro_domain","genomicSuperDups","snp138","cosmic70","AF","AF_popmax","AF_male","AF_female","AF_raw","AF_afr","AF_sas","AF_amr","AF_eas","AF_nfe","AF_fin","AF_asj","AF_oth","non_topmed_AF_popmax","non_neuro_AF_popmax","non_cancer_AF_popmax","controls_AF_popmax","ExAC_nontcga_ALL","ExAC_nontcga_AFR","ExAC_nontcga_AMR","ExAC_nontcga_EAS","ExAC_nontcga_FIN","ExAC_nontcga_NFE","ExAC_nontcga_OTH","ExAC_nontcga_SAS","1000g2015aug_eas","1000g2015aug_eur","1000g2015aug_sas","1000g2015aug_amr","1000g2015aug_afr","1000g2015aug_all","esp6500siv2_ea","esp6500siv2_aa","esp6500siv2_all"]]

#Deal with non-ascii characters
def remove_non_ascii(text):
    return ''.join(i for i in text if ord(i) < 128)

output_lab['CLNDN'] = output_lab['CLNDN'].apply(remove_non_ascii)
#-----------------------------------------------------------------------------------------------------------------------------------
#write output to file, this is FULL version (for stringent version, refer to next step)
output_lab.to_csv(OUTFILE, sep="\t", index=False, na_rep=".")

#-----------------------------------------------------------------------------------------------------------------------------------
#write output to file, stringent version. This means we only want output for genes in BROCA_genes. We may have other genes too if capture regions for SNVs of interest in non Broco genes were included in bed file so we want to disregard those.
#Take note that even if the col "Gene" contains one broca gene and one non-broca gene, we still want that row in our stringent output!
output_lab_broca = pd.DataFrame(columns=output_lab.columns)
for i in range(0,(output_lab.shape[0])):
    row = output_lab.iloc[i,:]
    for gene in BROCA_genes["Gene"]:
	list_of_genes = row["Gene"].split(";") #have to do this because we do not want "in" in next sentence, we want  ==  or else "GAAABX" will also go through if BROCA GENE is just "BX"
	for annovar_gene in list_of_genes:
		if gene == annovar_gene:
            		output_lab_broca.loc[i] = row
#write output to file, this is STRINGENT version
output_lab_broca.to_csv(OUTFILE_BROCA, sep="\t", index=False, na_rep=".")
#-----------------------------------------------------------------------------------------------------------------------------------
