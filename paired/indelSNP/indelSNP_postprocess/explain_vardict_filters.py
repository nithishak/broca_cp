#Small script to create the vardict filters that may be generated 

import sys
import pandas as pd

OUTPUT_FILE = sys.argv[1]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 2 :
    print "CHECK: Not all python args provided "
    sys.exit (1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

with open (OUTPUT_FILE, "w+") as output:
	output.write ("Vardict_Filter\tFilter_Explanation\n")
	output.write ("q22.5\tMean Base Quality Below 22.5\nQ0\tMean Mapping Quality Below 0\np8\tMean Position in Reads Less than 8\nSN1.5\tSignal to Noise Less than 1.5\nBias\tStrand Bias\npSTD\tPosition in Reads has STD of 0\nMAF0.05\tMatched sample has AF  0.05, thus not somatic\nd5\tTotal Depth < 5\nv5\tVar Depth < 5\nf0.03\tAllele frequency < 0.03\nP0.05\tNot significant with p-value  0.05\nDIFF0.2\tNon-somatic or LOH and allele frequency difference < 0.2\nP0.01Likely\tLikely candidate but p-value  0.01/5**vd2\nInDelLikely\tLikely Indels are not considered somatic\nMSI12\tVariant in MSI region with 12 non-monomer MSI or 12 monomer MSI\nNM5.25\tMean mismatches in reads = 5.25, thus likely false positive\nInGap\tThe somatic variant is in the deletion gap, thus likely false positive\nInIns\tThe somatic variant is adjacent to an insertion variant\nCluster0bp\tTwo somatic variants are within 0 bp\nLongAT\tThe somatic variant is flanked by long A/T (=14)\n")
