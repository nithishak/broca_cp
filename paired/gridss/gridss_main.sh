set -x
SAVEPATH=$1
TX_FILE=$2
BROCA_GENES_FILE=$3 
TUMOR_PFX=$4
GERMLINE_PFX=$5

#variables used
#GRIDSS
#BEDTOOLS
#GRIDSS_POSTPROCESS_SCRIPT

GRIDSS_DIR=$SAVEPATH/gridss
mkdir $GRIDSS_DIR


#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 5 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$REF_GENOME_b37|$TX_FILE|$CANGENES_FILE|$GRIDSS|$BEDTOOLS|$GRIDSS_POSTPROCESS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$SAVEPATH|$GRIDSS_DIR"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#single sample analysis - Run this too for paired sample as we want to know SVs in the germline sample as well! So consider this germline analysis for a G sample in a T/G pair!
gridss_single_sample () {
        java -ea -Xmx31g -Dsamjdk.create_index=true -Dsamjdk.use_async_io_read_samtools=true -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=true -Dsamjdk.compression_level=1 -Dgridss.gridss.output_to_temp_file=true -cp $GRIDSS gridss.CallVariants REFERENCE_SEQUENCE=$REF_GENOME_b37 INPUT=$SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G.bam OUTPUT=$GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.vcf ASSEMBLY=$GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.gridss.assembly.bam

	#Annotate using AnnotSV
        export "ANNOTSV=/mnt/disk4/labs/salipante/nithisha/programs/AnnotSV"
        $ANNOTSV/bin/AnnotSV -SVinputFile $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.vcf -outputFile $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf -bedtools $BEDTOOLS -SVminSize 0 -txFile $TX_FILE -annotationMode split -bcftools $BCFTOOLS
	
	#if gridss could not find any variants, it produdes an empty gridss_{GERMLINE_PREFIX}_G_output.sv.vcf, in this case, exits ANNNOTSV without error and does NOT produce gridss_${GERMLINE_PREFIX}_output.sv.annotated.vcf.tsv
	if [ ! -f $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf.tsv ];then
		echo "AnnotSV_ID	SV_chrom	SV_start	SV_end	SV_length	SV_type	Samples_ID	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	NCI9881.11081.107.G	Annotation_mode	Gene_name	Gene_count	Tx	Tx_start	Tx_end	Overlapped_tx_length	Overlapped_CDS_length	Overlapped_CDS_percent	Frameshift	Exon_count	Location	Location2	Dist_nearest_SS	Nearest_SS_type	Intersect_start	Intersect_end	RE_gene	P_gain_phen	P_gain_hpo	P_gain_source	P_gain_coord	P_loss_phen	P_loss_hpo	P_loss_source	P_loss_coord	P_ins_phen	P_ins_hpo	P_ins_source	P_ins_coord	P_snvindel_nb	P_snvindel_phen	B_gain_source	B_gain_coord	B_loss_source	B_loss_coord	B_ins_source	B_ins_coord	B_inv_source	B_inv_coord	TAD_coordinate	ENCODE_experiment	GC_content_left	GC_content_right	Repeat_coord_left	Repeat_type_left	Repeat_coord_right	Repeat_type_right	Gap_left	Gap_right	SegDup_left	SegDup_right	ENCODE_blacklist_left	ENCODE_blacklist_characteristics_left	ENCODE_blacklist_right	ENCODE_blacklist_characteristics_right	ACMG	HI	TS	DDD_HI_percent	DDD_status	DDD_mode	DDD_consequence	DDD_disease	DDD_pmid	ExAC_delZ	ExAC_dupZ	ExAC_cnvZ	ExAC_synZ	ExAC_misZ	OMIM_ID	OMIM_phenotype	OMIM_inheritance	OMIM_morbid	OMIM_morbid_candidate	LOEUF_bin	GnomAD_pLI	ExAC_pLI	AnnotSV_ranking_score	AnnotSV_ranking_criteria	ACMG_class" >> $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf.tsv
	fi

        #This file may contain non-ASCII characters and causes problems when merging so remove them here inplace
        LANG=C sed -i 's/[\d128-\d255]//g' $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf.tsv
}



#somatic analysis
gridss_paired_sample () {
	java -ea -Xmx31g -Dsamjdk.create_index=true -Dsamjdk.use_async_io_read_samtools=true -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=true -Dsamjdk.compression_level=1 -Dgridss.gridss.output_to_temp_file=true -cp $GRIDSS gridss.CallVariants REFERENCE_SEQUENCE=$REF_GENOME_b37 INPUT=$SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G.bam INPUT=$SAVEPATH/recal/bqsr_${TUMOR_PFX}_T.bam OUTPUT=$GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.sv.vcf ASSEMBLY=$GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.gridss.assembly.bam

	#Annotate using AnnotSV
	export "ANNOTSV=/mnt/disk4/labs/salipante/nithisha/programs/AnnotSV"
        $ANNOTSV/bin/AnnotSV -SVinputFile  $GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.sv.vcf -outputFile  $GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.sv.annotated.vcf -bedtools $BEDTOOLS -SVminSize 0 -txFile $TX_FILE -annotationMode split -bcftools $BCFTOOLS

        #This file may contain non-ASCII characters and causes problems when merging so remove them here inplace
        LANG=C sed -i 's/[\d128-\d255]//g' $GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.sv.annotated.vcf.tsv
}


#postprocess outputs
gridss_postprocess () {
	python $GRIDSS_POSTPROCESS_SCRIPT $GRIDSS_DIR/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf.tsv $GRIDSS_DIR/gridss_${TUMOR_PFX}_TG_output.sv.annotated.vcf.tsv $SAVEPATH/results/gridss_${GERMLINE_PFX}_G_output.sv.annotated.vcf $SAVEPATH/results/gridss_${TUMOR_PFX}_TG_output.sv.annotated.vcf $SAVEPATH/results/gridss_${TUMOR_PFX}_merged_output.sv.annotated.vcf $BROCA_GENES_FILE 
}


rm_files () {
	rm -r $GRIDSS_DIR
}


#Main code
gridss_single_sample
gridss_paired_sample
gridss_postprocess
rm_files
