#This script is simply to 1) filter for BROCA genes 2)remove certain columns and 3)sort by gene and position the gridss outputs
import sys
import pandas as pd
import numpy as np

#Read input
GRIDSS_GERMLINE_INPUT =  sys.argv[1] #input for this pipeline
GRIDSS_SOMATIC_INPUT = sys.argv[2] #input for this pipeline
GRIDSS_GERMLINE_OUTPUT_FILE = sys.argv[3]
GRIDSS_SOMATIC_OUTPUT_FILE = sys.argv[4]
GRIDSS_OUTPUT_FILE = sys.argv[5]
BROCA_GENES_FILE = sys.argv[6]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 7 :
	print "CHECK: Not all python args provided "
	sys.exit (1)


#File check
files=[GRIDSS_GERMLINE_INPUT,GRIDSS_SOMATIC_INPUT,BROCA_GENES_FILE]
for filee in files:
		try:
				with open(filee) as f:
						pass
		except IOError as e:
				print "ERROR:Unable to open file" #Does not exist OR no read permissions
				sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

def read_file (inputfile):
	df = pd.read_csv(inputfile, sep = "\t", header = 0, index_col=None, dtype="str", na_filter = False)
	
	return df

def filter_df (df):
	#Let us filter out any rows with QUAL below 200. Typically we can believe rows with FILTER pass and QUAL > 1000 with high confidence 
	df["QUAL"] = df["QUAL"].astype("float64").astype(int)
	df = df[df["QUAL"] > 200]
	df["QUAL"] = df["QUAL"].astype("str")

	#Let us drop rows that are contain orphaned reads in ALT col - these either start with or end with a .
	df = df[~df["ALT"].str.contains(".", regex= False)]

	return df

def tidy_df (df):
	#Let us create a new col called  "ParID". This contains the paired mate's id from "INFO" col's PARID field. This helps us match the other matching mate using the col "ID"
	df['ParID'] = df['INFO'].apply (lambda x: x.split("PARID=")[1].split(";")[0]) #should not have any rows without ParID at this point as orphaned reads have been remmoved already

	#Let us create a col to identify introns
	#Let us modify the "Location" col such that if it says "intronX - intronX" (the same intron), col "Location2" would say "intron" instead of "CDS".
	#Note that if col "Location" says "intronA-intronC", it includes exonB so it is ok for  "Location2" to say "CDS"
	def indicate_introns (Location,Location2) :
		if (pd.notnull(Location) and "intron" in Location and Location.split("-")[0] == Location.split("-")[1]):
			return "intron"
		else:
			return Location2 
	df['Location2'] = df.apply(lambda x: indicate_introns (x['Location'], x['Location2']), axis=1) 

	return df

def mark_similar_ids (df):
	#Ok so if a Location is in 2 genes, we will see identical gridss IDs for 2 rows for eg with the "Gene name" col different. All the other cols hold the same info.  This creates a prob for zipping below.
	#Let us make sure gridss ids are unique, with gene names reflecting "geneA/geneB" if Location matches 2 genes for eg.
	
	#Make a df that contains all instances of duplicate rows
	dup_rows = df[df.duplicated(['ID'],keep=False)]
	#Generate a unique set of ids for duplicate rows
	dup_row_ids = list (set(dup_rows["ID"]))

	dict_id_genes = {}

	for unique_id in dup_row_ids:
 		#call out all the lines that correspond to that unique gridss id
		lines =df.loc[df["ID"] == unique_id] #this gives df with 2 rows
		genes = "/".join(list(lines["Gene_name"])) #geneA/geneB for eg.
		dict_id_genes[unique_id]= genes

	#Only keep first instance of every unique gridss id
	df.drop_duplicates (subset = ["ID"], keep = "first", inplace= True)

	#For these cols, change geneA to geneA/geneB, thus reflecting all genes that gridss id covers
	for key, value in dict_id_genes.items():
		df.loc[df["ID"] == key, "Gene_name"] = value
	#At this point in time,we should NOT have rows with similar GRIDSS IDs

	return df

def generate_id_pairs (df):
	#Let us zip the ID and PAR IDs together
	#Note that pairs in GRIDSS always match with an o/h at the end of ID/PAR ID, unmatched rows end with a b
	pairs = list(zip(df["ID"], df["ParID"]))

	#Let us create a unique set of [(ID,PAR ID), (ID2, PAR ID2)] where we do not see  (123o,123h) followed by (124h,123o) - 2 breakends of same breakpoint, we only want one of the 2 entries!
	unique_pair_no = []
	[unique_pair_no.append(pair[0][:-1]) for pair in pairs if pair[0][:-1] not in unique_pair_no]
	unique_pairs = [((x+"o"),(x+"h")) for x in unique_pair_no] #can still contain a pair where o is in ID but not h (PAR ID) or h is there in ID but o is not in PAR ID OR VICE VERSA, yes this really happens 

	return unique_pairs
	
def generate_new_df ():
	#Let us create a new df
        new_df =  pd.DataFrame(columns=["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "QUAL", "FILTER", "Type","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"])

	return new_df

def populate_new_df (df, new_df, unique_pairs):
	#let us make the "ID" col the index
        df = df.set_index("ID")
	
	#Let us populate the new df
	for counter,pair in enumerate(unique_pairs):
		row1_index = pair[0]
		row2_index = pair[1]
		if row1_index in df.index and row2_index in df.index: #because sometimes, PAR ID is present but row with PAR ID as ID cannot be found!?
			row1 =  df.loc[row1_index,:].astype(str) #imp, some cols may be read in as a float series here! #One breakend of a SV
			row2 =  df.loc[row2_index,:].astype(str) #Matching breakend of SV
	
			new_df.loc[counter,"Event1"] = "chr" + row1["SV_chrom"] + ":" + row1["SV_start"]
			new_df.loc[counter,"Event2"] = "chr" + row2["SV_chrom"] + ":" + row2["SV_end"]
			new_df.loc[counter,"Gene1"] = row1["Gene_name"]
			new_df.loc[counter,"Gene2"] = row2["Gene_name"]
			new_df.loc[counter,"Location1"] = row1["Location"].split("-")[0] + "(" + row1["Location2"] + ")"
			new_df.loc[counter,"Location2"] = row2["Location"].split("-")[0] + "(" + row2["Location2"] + ")"
			
			#Let us create an Indicate_Region col to indicate events that take place in same intron/5UTR/3UTR as these are not of so much imp to us	
			if (row1["SV_chrom"] == row2["SV_chrom"]): #if events on same chrom
				if row1["Location2"] == "intron" and row2["Location2"] == "intron": #if both events are introns
					if ((row1["Location"].split("-")[0]) == (row2["Location"].split("-")[0])): #if both events occur on same intron no
						new_df.loc[counter,"Indicate_Region"] = "I"
					#else this means that it is intron x to intron y which means coding region as exon is involved so take no action
				
				elif row1["Location2"] == "3\'UTR" and row2["Location2"] == "3\'UTR":
					new_df.loc[counter,"Indicate_Region"] = "3UTR"

				elif row1["Location2"] == "5\'UTR" and row2["Location2"] == "5\'UTR":
					new_df.loc[counter,"Indicate_Region"] = "5UTR"

				#check what to do if UTR going into exon/intron or vice versa
			else: #if events on diff chroms
				new_df.loc[counter,"Indicate_Region"] = "DIFF_CHROM" #as this is gene fusion event
			
			#Let us indicate length of events if they take place in same chromosome or else indicate "DIF_CHROM" (labmed indicates this using "CTX")
			if row1["SV_chrom"] == row2["SV_chrom"]:
				new_df.loc[counter,"Length"] = str(abs((int(row2["SV_end"]) - int(row1["SV_start"]))))
			else:
				new_df.loc[counter,"Length"] = "DIFF_CHROM"
			#From this col onwards, let us be careful and add outputs from both rows of a breakpoint, let us separate them with a ; 
			#and remove one of the two inputs if both rows are the same for the field downstream
			#Note that for FILTER and GD_ID_others cols, separators are ; to begin with so change these in each row to a ; first!
			new_df.loc[counter,"Tx"] = row1["Tx"] + "|" +  row2["Tx"]
                        new_df.loc[counter,"QUAL"] =  row1["QUAL"] + "|" + row2["QUAL"]
                        new_df.loc[counter,"FILTER"] = row1["FILTER"].replace(";",",") + "|" + row2["FILTER"].replace(";",",")
                        #If genes for row 1 and 2 are the same, name Intragenic, if not Gene Fusion
                        if  row1["Gene_name"] == row2["Gene_name"] or row1["Gene_name"] in row2["Gene_name"] or row2["Gene_name"] in row1["Gene_name"]: #to account for instances like ATM and ATM/C11orf65 as Gene1 and Gene2:
                                new_df.loc[counter,"Type"] = "Intragenic"
                        else:
                                new_df.loc[counter,"Type"] = "Gene Fusion"
                        new_df.loc[counter,"Ref"] = row1["REF"] + "|" + row2["REF"]
                        new_df.loc[counter,"Alt"] = row1["ALT"] + "|" + row2["ALT"]
                        new_df.loc[counter,"Gene_count"] = row1["Gene_count"] + "|" + row2["Gene_count"]
                        new_df.loc[counter,"Overlapped_tx_length"] = row1["Overlapped_tx_length"] + "|" + row2["Overlapped_tx_length"]
                        new_df.loc[counter,"Overlapped_tx_length"] = row1["Overlapped_tx_length"] + "|" + row2["Overlapped_tx_length"]
                        new_df.loc[counter,"Overlapped_tx_length"] = row1["Overlapped_tx_length"] + "|" + row2["Overlapped_tx_length"]
                        new_df.loc[counter,"Overlapped_CDS_length"] = row1["Overlapped_CDS_length"] + "|" + row2["Overlapped_CDS_length"]
                        new_df.loc[counter,"Overlapped_CDS_percent"] = row1["Overlapped_CDS_percent"] + "|" + row2["Overlapped_CDS_percent"]
                        new_df.loc[counter,"Frameshift"] = row1["Frameshift"] + "|" + row2["Frameshift"]
                        new_df.loc[counter,"Exon_count"] = row1["Exon_count"] + "|" + row2["Exon_count"]
                        new_df.loc[counter,"Dist_nearest_SS"] = row1["Dist_nearest_SS"] + "|" + row2["Dist_nearest_SS"]
                        new_df.loc[counter,"Nearest_SS_type"] = row1["Nearest_SS_type"] + "|" + row2["Nearest_SS_type"]
                        new_df.loc[counter,"Intersect_start"] = row1["Intersect_start"] + "|" + row2["Intersect_start"]
                        new_df.loc[counter,"Intersect_end"] = row1["Intersect_end"] + "|" + row2["Intersect_end"]
                        new_df.loc[counter,"P_gain_phen"] = row1["P_gain_phen"] + "|" + row2["P_gain_phen"]
                        new_df.loc[counter,"P_gain_hpo"] = row1["P_gain_hpo"] + "|" + row2["P_gain_hpo"]
                        new_df.loc[counter,"P_gain_source"] = row1["P_gain_source"] + "|" + row2["P_gain_source"]
                        new_df.loc[counter,"P_gain_coord"] = row1["P_gain_coord"] + "|" + row2["P_gain_coord"]
                        new_df.loc[counter,"P_loss_phen"] = row1["P_loss_phen"] + "|" + row2["P_loss_phen"]
                        new_df.loc[counter,"P_loss_hpo"] = row1["P_loss_hpo"] + "|" + row2["P_loss_hpo"]
                        new_df.loc[counter,"P_loss_source"] = row1["P_loss_source"] + "|" + row2["P_loss_source"]
                        new_df.loc[counter,"P_loss_coord"] = row1["P_loss_coord"] + "|" + row2["P_loss_coord"]
                        new_df.loc[counter,"P_ins_phen"] = row1["P_ins_phen"] + "|" + row2["P_ins_phen"]
                        new_df.loc[counter,"P_ins_hpo"] = row1["P_ins_hpo"] + "|" + row2["P_ins_hpo"]
                        new_df.loc[counter,"P_ins_source"] = row1["P_ins_source"] + "|" + row2["P_ins_source"]
                        new_df.loc[counter,"P_ins_coord"] = row1["P_ins_coord"] + "|" + row2["P_ins_coord"]
                        new_df.loc[counter,"P_snvindel_nb"] = row1["P_snvindel_nb"] + "|" + row2["P_snvindel_nb"]
                        new_df.loc[counter,"P_snvindel_phen"] = row1["P_snvindel_phen"] + "|" + row2["P_snvindel_phen"]
                        new_df.loc[counter,"B_gain_source"] = row1["B_gain_source"] + "|" + row2["B_gain_source"]
                        new_df.loc[counter,"B_gain_coord"] = row1["B_gain_coord"] + "|" + row2["B_gain_coord"]
                        new_df.loc[counter,"B_loss_source"] = row1["B_loss_source"] + "|" + row2["B_loss_source"]
                        new_df.loc[counter,"B_loss_coord"] = row1["B_loss_coord"] + "|" + row2["B_loss_coord"]
                        new_df.loc[counter,"B_ins_source"] = row1["B_ins_source"] + "|" + row2["B_ins_source"]
                        new_df.loc[counter,"B_ins_coord"] = row1["B_ins_coord"] + "|" + row2["B_ins_coord"]
                        new_df.loc[counter,"B_inv_source"] = row1["B_inv_source"] + "|" + row2["B_inv_source"]
                        new_df.loc[counter,"B_inv_coord"] = row1["B_inv_coord"] + "|" + row2["B_inv_coord"]
                        new_df.loc[counter,"SegDup_left"] = row1["SegDup_left"] + "|" + row2["SegDup_left"]
                        new_df.loc[counter,"SegDup_right"] = row1["SegDup_right"] + "|" + row2["SegDup_right"]
		else:
                        print "The 2 breakends for " + pair[0] + "," + pair[1] + " were not found!"
                        if row1_index in df.index:
                                row =  df.loc[row1_index,:].astype(str)
                        else:
                                row =  df.loc[row2_index,:].astype(str)


                        new_df.loc[counter,"Event1"] = "chr" + row["SV_chrom"] + ":" + row["SV_start"]
                        new_df.loc[counter,"Event2"] = "unknown"
                        new_df.loc[counter,"Gene1"] = row["Gene_name"]
                        new_df.loc[counter,"Gene2"] = "unknown"
                        new_df.loc[counter,"Location1"] = row["Location"].split("-")[0] + "(" + row["Location2"] + ")"
                        new_df.loc[counter,"Location2"] = "unknown"
                        #Let us create an Indicate_Region col to indicate eventst that take place entirely in same intron (not of such imp to us) - NOT relevant here
                        new_df.loc[counter,"Indicate_Region"] = "unknown"
                        #Let us indicate length of events if they take place in same chromosome or else indicate "DIF_CHROM" (labmed indicates this using "CTX") - NOT relevant here
                        new_df.loc[counter,"Length"] = "unknown"
                        new_df.loc[counter,"Tx"] = row["Tx"]
                        new_df.loc[counter,"QUAL"] =  row["QUAL"]
                        new_df.loc[counter,"FILTER"] = row["FILTER"].replace(";",",")
                        #If genes for row 1 and 2 are the same, name Intragenic, if not Gene Fusion - NOT relevant here
                        new_df.loc[counter,"Type"] = "unknown"
                        new_df.loc[counter,"Ref"] = "unknown"
                        new_df.loc[counter,"Alt"] = "unknown"
                        new_df.loc[counter,"Gene_count"] = "NA"
                        new_df.loc[counter,"Overlapped_tx_length"] = "NA"
                        new_df.loc[counter,"Overlapped_CDS_length"] = "NA"
                        new_df.loc[counter,"Overlapped_CDS_percent"] = "NA"
                        new_df.loc[counter,"Frameshift"] = "NA"
                        new_df.loc[counter,"Exon_count"] = "NA"
                        new_df.loc[counter,"Dist_nearest_SS"] = "NA"
                        new_df.loc[counter,"Nearest_SS_type"] = "NA"
                        new_df.loc[counter,"Intersect_start"] = "NA"
                        new_df.loc[counter,"Intersect_end"] = "NA"
                        new_df.loc[counter,"P_gain_phen"] = "NA"
                        new_df.loc[counter,"P_gain_hpo"] = "NA"
                        new_df.loc[counter,"P_gain_source"] = "NA"
                        new_df.loc[counter,"P_gain_coord"] = "NA"
                        new_df.loc[counter,"P_loss_phen"] = "NA"
                        new_df.loc[counter,"P_loss_hpo"] = "NA"
                        new_df.loc[counter,"P_loss_source"] = "NA"
                        new_df.loc[counter,"P_loss_coord"] = "NA"
                        new_df.loc[counter,"P_ins_phen"] = "NA"
                        new_df.loc[counter,"P_ins_hpo"] = "NA"
                        new_df.loc[counter,"P_ins_source"] = "NA"
                        new_df.loc[counter,"P_ins_coord"] = "NA"
                        new_df.loc[counter,"P_snvindel_nb"] = "NA"
                        new_df.loc[counter,"P_snvindel_phen"] = "NA"
                        new_df.loc[counter,"B_gain_source"] = "NA"
                        new_df.loc[counter,"B_gain_coord"] = "NA"
                        new_df.loc[counter,"B_loss_source"] = "NA"
                        new_df.loc[counter,"B_loss_coord"] = "NA"
                        new_df.loc[counter,"B_ins_source"] = "NA"
                        new_df.loc[counter,"B_ins_coord"] = "NA"
                        new_df.loc[counter,"B_inv_source"] = "NA"
                        new_df.loc[counter,"B_inv_coord"] = "NA"
                        new_df.loc[counter,"SegDup_left"] = "NA"
                        new_df.loc[counter,"SegDup_right"] = "NA"
	return new_df


def clean_new_df (new_df):
	#Now for the cols that have row1 input;row2 input, let us keep only one of the two inputs if repeated
	column_names = ["Tx", "QUAL", "FILTER", "Type","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]
	for column in column_names:	
                new_df[column] = new_df[column].apply(lambda x: x.split("|")[0] if (len(x.split("|"))>1 and x.split("|")[0] == x.split("|")[1]) else x)
                #Strip for any "|" at beginning or ending of str incase it was blank|xxx or xxx|blank
                new_df[column] = new_df[column].apply(lambda x: x.strip("|"))

	#If Qual col has more than 1 values, choose max of both values
	new_df["QUAL"] = new_df["QUAL"].apply(lambda x: str(max([int(i) for i in (x.split("|"))])) if x.split("|") > 1 else x)	

	#Let us drop rows with length < 10
	new_df = new_df[new_df["Length"] > 10]

	return new_df


def filter_BROCA (BROCA_GENES_FILE, new_df):
	#Filter for rows that have BROCA gene in either Gene1 or Gene2
	with open (BROCA_GENES_FILE, 'r+') as broca_genes_file:
		broca_genes = broca_genes = broca_genes_file.readline().strip("\n").split("\t")
	
	for i in range(0,new_df.shape[0]):
		row  = new_df.loc[i]
		all_genes = row['Gene1'].split("/") + row['Gene2'].split('/') #if unknown present, ['LRIF1', 'unknown'] eg.


		drop = "yes"
		for gene in all_genes:
			if gene in(broca_genes):
				drop = "no"
		
		if drop == "yes":
			new_df.drop(i, inplace=True)

	return new_df

def write_to_csv (new_df, outputfile):
	#write new df to file
	new_df.to_csv(outputfile, sep="\t", index=False)


#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Sub-func

def gridss_postprocess (inputfile, outputfile, BROCA_GENES_FILE):
	df = read_file (inputfile)
	df = filter_df(df)
	if df.shape[0] != 0: #not an empty dataframe
		df = tidy_df (df)
		df = mark_similar_ids (df)
		unique_pairs = generate_id_pairs (df)
		new_df = generate_new_df ()
		new_df = populate_new_df (df, new_df, unique_pairs)
		new_df = clean_new_df (new_df)
		new_df = filter_BROCA (BROCA_GENES_FILE, new_df)
	else: 
		new_df = pd.DataFrame(columns= ["Event1", "Event2", "Gene1", "Gene2", "Location1", "Location2", "Indicate_Region", "Length", "Tx", "Type","QUAL", "FILTER","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"])
 	
	write_to_csv (new_df, outputfile)
	
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def gridss_merge (germline_file,somatic_file,merged_file):
	#Lets merge gridss germline and somatic together!
	gridss_germline = pd.read_csv(GRIDSS_GERMLINE_OUTPUT_FILE, sep = "\t", header = 0, index_col=None,dtype="str")
	gridss_somatic = pd.read_csv(GRIDSS_SOMATIC_OUTPUT_FILE, sep = "\t", header = 0, index_col=None,dtype="str")

	gridss = pd.merge (gridss_somatic, gridss_germline, on=["Event1","Event2","Gene1","Gene2","Location1","Location2","Indicate_Region","Length","Tx","Type","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"], how = "outer", suffixes = ["(somatic)","(germline)"])

	#Sort by genes and position
	gridss["POS"] = gridss["Event1"].apply(lambda x: x.split(":")[1])
	gridss.sort_values(["Gene1","POS"], inplace= True)
	
	gridss_rearranged = gridss[["Event1","Event2","Gene1","Gene2","Location1","Location2","Indicate_Region","Length","Tx","Type","QUAL(somatic)","FILTER(somatic)","QUAL(germline)","FILTER(germline)","Ref","Alt","Gene_count","Overlapped_tx_length", "Overlapped_CDS_length", "Overlapped_CDS_percent","Frameshift","Exon_count","Dist_nearest_SS","Nearest_SS_type","Intersect_start","Intersect_end","P_gain_phen","P_gain_hpo","P_gain_source","P_gain_coord","P_loss_phen","P_loss_hpo","P_loss_source","P_loss_coord","P_ins_phen","P_ins_hpo","P_ins_source","P_ins_coord","P_snvindel_nb","P_snvindel_phen","B_gain_source","B_gain_coord","B_loss_source","B_loss_coord","B_ins_source","B_ins_coord","B_inv_source","B_inv_coord","SegDup_left","SegDup_right"]]

	gridss_rearranged.to_csv(GRIDSS_OUTPUT_FILE,sep="\t", index=False)
	

#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Main code
gridss_postprocess(GRIDSS_GERMLINE_INPUT,GRIDSS_GERMLINE_OUTPUT_FILE,BROCA_GENES_FILE)
gridss_postprocess(GRIDSS_SOMATIC_INPUT,GRIDSS_SOMATIC_OUTPUT_FILE, BROCA_GENES_FILE)
gridss_merge (GRIDSS_GERMLINE_OUTPUT_FILE,GRIDSS_SOMATIC_OUTPUT_FILE,GRIDSS_OUTPUT_FILE)
