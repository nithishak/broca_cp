#IF SCRIPT FOR INDELSNP IS MODIFIED, PLEASE MAKE CHANGES HERE, COLS FOR INDELSNP HAVE BEEN HARDCODED!
#The purpose of this script is to make one main output file per paired T/G sample

###########################################################################################################################
#CHECKKKKK that in the indelSNP final analysis, Vardict cols are M-P inclusive and Varscan cols are Q-T inclusive!!!!!!!!!!
indelsnp_str = ["M","P","Q","T"]
#CHECKKKKK that in the gridds final analysis, somatic cols are K-L inclusive and germline cols are M-N  inclusive!!!!!!!!!!
gridss_str = ["K","L","M","N"]
###########################################################################################################################

import sys
import pandas as pd
import string
import os #For dir check only

SAVEPATH = sys.argv[1]
TUMOR_PFX = sys.argv[2]
GERMLINE_PFX = sys.argv[3] 
OUTPUT_FILE = sys.argv[4] #This is an excel so .xlsx

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#Dir check
if os.path.exists(SAVEPATH) == False:
	print "ERROR:One of required directories not present"
        sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#From qc for each sample
QC_METRICS = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_qc_metrics.txt"

#From qc_exons and qc_genes
QC_EXONS = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_exon_qc.txt"
QC_GENES = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_gene_qc.txt"

#From indel analysis
VARDICT_FILTERS = SAVEPATH + "/results/vardict_filters.txt"
INDELSNP = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_snp_indel_analysis.txt"
INDELSNP_BROCA = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_snp_indel_analysis_BROCA.txt"

#From gridss
GRIDSS = SAVEPATH + "/results/gridss_" + TUMOR_PFX + "_merged_output.sv.annotated.vcf"

#From pindel
PINDEL = SAVEPATH + "/results/pindel_" + TUMOR_PFX + "_TG.txt"

#from Msings
MSINGS = SAVEPATH + "/results/" + TUMOR_PFX + "_TG_MSI_Analysis.txt"



#open files as dfs
qc_metrics = pd.read_csv(QC_METRICS, sep ="\t", header = 0,index_col=0)
qc_exons = pd.read_csv(QC_EXONS, sep ="\t", header =0)
qc_genes = pd.read_csv(QC_GENES, sep ="\t",header = 0)
vardict_filters = pd.read_csv(VARDICT_FILTERS, sep ="\t",header = 0)
indelsnp= pd.read_csv(INDELSNP, sep = "\t", header = 0)
indelsnp_BROCA = pd.read_csv(INDELSNP_BROCA, sep="\t", header=0)
gridss = pd.read_csv(GRIDSS, sep ="\t",header = 0)
pindel = pd.read_csv (PINDEL, sep ="\t",header = 0)
msings = pd.read_csv (MSINGS, sep ="\t",header = 0,index_col=0)

# Create a Pandas Excel writer using XlsxWriter as the engine. specify OUTPUT file here!.
writer = pd.ExcelWriter(OUTPUT_FILE, engine='xlsxwriter',options={'strings_to_numbers': True})

# Write each dataframe to a different worksheet.
qc_metrics.to_excel(writer, sheet_name='QC_Metrics')
qc_exons.to_excel(writer, sheet_name='QC_exons',index=False)
qc_genes.to_excel(writer, sheet_name='QC_genes',index=False)
vardict_filters.to_excel (writer, sheet_name='vardict_filters',index=False)
indelsnp.to_excel(writer, sheet_name='indelSNP',index=False)
indelsnp_BROCA.to_excel(writer, sheet_name='indelSNP_BROCA',index=False)
gridss.to_excel(writer, sheet_name='gridss',index=False)
pindel.to_excel(writer, sheet_name='pindel',index=False)
msings.to_excel(writer, sheet_name='msings')


workbook  = writer.book


#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#1. For QC Metrics
def apply_formatting_with_index (dataframe,type_of_analysis):
	worksheet = writer.sheets[type_of_analysis]

	#To see headers clearly without them being condensed
	#To align all contents of the sheet to the left
	format1 = workbook.add_format({'align': 'left'})

	def get_col_widths(dataframe):
		# First we find the maximum length of the index column  #Here we do have an index col so use this!
		idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))]) *1.2
    		# Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
    		return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)+3]) for col in dataframe.columns]

	for i, width in enumerate(get_col_widths(dataframe)):
    		worksheet.set_column(i, i, width,format1)


apply_formatting_with_index (qc_metrics, 'QC_Metrics')
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#2. For QC_exon_gene

#Inner func
def find_cells_conditioning(dataframe):
	#Simple function to specify rows and columns for conditional formatting
	#This function is to get Excel sheet cells [F2 - F..] for exon_qc, tells excel conditioning to focus on these cells only - in this case, "Average_Read_Depth" column (or E2-E... in the case of gene_qc)
	limits = []
	no_of_cols = dataframe.shape[1]
	no_of_rows = dataframe.shape[0]
	#doing this under the assumption that the qc file will always have avg read depth for T and G in the last 2 columns of file!
	col_alphabet_1 = list(string.ascii_uppercase)[no_of_cols-2]
	col_alphabet_2 = list(string.ascii_uppercase)[no_of_cols-1]
	top_limit = col_alphabet_1 + "2"	
	bottom_limit = col_alphabet_2 + str(no_of_rows+1)
	limits.append(top_limit)
	limits.append(bottom_limit)

	return (limits)

#Main function
def highlight_poor_coverage_cells(dataframe,type_of_qc): #type of qc is a string - "exon" or "gene"
	#Let us try to highlight cells in "Average_Read_Depth" column that are less than 50 to show which exons failed QC
	limits = find_cells_conditioning (dataframe)

	# Get the xlsxwriter workbook and worksheet objects.
	worksheet = writer.sheets['QC_'+ type_of_qc]

	#Apply a conditional format to the cell range.
	#Add a format. Light red fill with dark red text.
	format1 = workbook.add_format({'bg_color': '#FFC7CE',
	                               'font_color': '#9C0006'})
	#To align all contents of the sheet to the left
        format2 = workbook.add_format({'align': 'left'})

	#Apply a conditional format to the cell range.
	string = limits[0] + ":" + limits[1]
	worksheet.conditional_format(string, {'type':     'cell',
                                       'criteria': '<',
                                        'value':    50,
                                        'format':   format1})

        #To see headers clearly without them being condensed
        def get_col_widths(dataframe):
                # First we find the maximum length of the index column #Not needed here as we don't have an index col

                # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
                return [max([len(str(s)) for s in dataframe[col].values] + [len(col) + 3]) for col in dataframe.columns]

        for i, width in enumerate(get_col_widths(dataframe)):
                worksheet.set_column(i, i, width,format2)

highlight_poor_coverage_cells(qc_exons,"exons")
highlight_poor_coverage_cells(qc_genes,"genes")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#3. For indelSNP

def find_cells_conditioning(dataframe):
        #Simple function to specify rows and columns for conditional formatting
	#Let us change the font color for Vardict and Varscan col for easier analysis
	#Assumption is that Vardict cols are ALWAYS M-P inclusive and Varscan are cols Q-T inclusive
        limits = []
        no_of_cols = dataframe.shape[1]
        no_of_rows = dataframe.shape[0]
        
	vardict_string = indelsnp_str[0] + "2:" + indelsnp_str[1] + str(no_of_rows+1)
        varscan_string = indelsnp_str[2]+ "2:" + indelsnp_str[3] + str(no_of_rows+1)
        limits.append(varscan_string)
        limits.append(vardict_string)

        return (limits)

#Main function
def highlight_varscan_vardict(dataframe, type_of_analysis): #type_of_analysis is a str, either "indelSNP" or "indelSNP_BROCA"
	limits = find_cells_conditioning (dataframe)

	# Get the xlsxwriter workbook and worksheet objects.
	worksheet = writer.sheets[type_of_analysis]

	#Apply a conditional format to the cell range.
	format1 = workbook.add_format({'font_color': '#9C0006'})
	format2 = workbook.add_format({'font_color': '#191970'})
	#To align all contents of the sheet to the left
	format3 = workbook.add_format({'align': 'left'})

	#Apply a conditional format to the cell range.
	worksheet.conditional_format(limits[0], {'type': 'no_blanks',
                                        'format':   format1})

	worksheet.conditional_format(limits[1], {'type': 'no_blanks',
                                        'format':   format2})

        #To see headers clearly without them being condensed
        def get_col_widths(dataframe):
                # First we find the maximum length of the index column #Not needed here as we don't have an index col

                # Then, we concatenate this to the lengths of column name
		return [(len(x)+3) for x in dataframe.columns]
		
        for i, width in enumerate(get_col_widths(dataframe)):
                worksheet.set_column(i, i, width,format3)

#For vardict_filters, formatting done below
highlight_varscan_vardict(indelsnp, "indelSNP")
highlight_varscan_vardict(indelsnp_BROCA, "indelSNP_BROCA")



def apply_formatting(dataframe,type_of_analysis):
       worksheet = writer.sheets[type_of_analysis]

       format1 = workbook.add_format({'align': 'left'})

       #To see headers clearly without them being condensed
       def get_col_widths(dataframe):
               # First we find the maximum length of the index column #Not needed here as we don't have an index col

               # Then, we concatenate this to the lengths of column name
                return [(len(x)+3) for x in dataframe.columns]

       for i, width in enumerate(get_col_widths(dataframe)):
               worksheet.set_column(i, i, width,format1)

#For vardict_filters 
apply_formatting (vardict_filters, "vardict_filters")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#4. For Gridss
def find_cells_conditioning(dataframe):
        #Simple function to specify rows and columns for conditional formatting
        #Let us change the font color for somatic and germline col for easier analysis
        #Assumption is that somatic cols are ALWAYS K-L inclusive and Vardict are cols M-N inclusive
        limits = []
        no_of_cols = dataframe.shape[1]
        no_of_rows = dataframe.shape[0]

        varscan_string = gridss_str[0]+ "2:" + gridss_str[1] + str(no_of_rows+1)
        vardict_string = gridss_str[2] +"2:" + gridss_str[3] + str(no_of_rows+1)
        limits.append(varscan_string)
        limits.append(vardict_string)

        return (limits)

#Main function
def highlight_gridss(dataframe, type_of_analysis): #type_of_analysis is a str, either "indelSNP" or "indelSNP_BROCA"
        limits = find_cells_conditioning (dataframe)

        # Get the xlsxwriter workbook and worksheet objects.
        worksheet = writer.sheets[type_of_analysis]

        #Apply a conditional format to the cell range.
        # Add a format. Color codes - https://www.rapidtables.com/web/color/html-color-codes.html #search for #rrggbb
        format1 = workbook.add_format({'font_color': '#9C0006'})
        format2 = workbook.add_format({'font_color': '#191970'})
        #To align all contents of the sheet to the left
        format3 = workbook.add_format({'align': 'left'})

        #Apply a conditional format to the cell range.
        worksheet.conditional_format(limits[0], {'type': 'no_blanks',
                                        'format':   format1})

        worksheet.conditional_format(limits[1], {'type': 'no_blanks',
                                        'format':   format2})

        #To see headers clearly without them being condensed
        def get_col_widths(dataframe):
                # First we find the maximum length of the index column #Not needed here as we don't have an index col

                # Then, we concatenate this to the lengths of column name
                return [(len(x)+3) for x in dataframe.columns]

        for i, width in enumerate(get_col_widths(dataframe)):
                worksheet.set_column(i, i, width,format3)

highlight_gridss (gridss, "gridss")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#5.For Pindel
apply_formatting(pindel,"pindel")
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#6. For Msings
apply_formatting_with_index (msings, 'msings')
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Close the Pandas Excel writer and output the Excel file.
writer.save()
