import re
import sys
import os

sys.path.append("/mnt/disk4/labs/salipante/nithisha/CP/paired/annotation") #path where it searches for annotation_BROCA
import annotation_script_BROCA as a

#List of inputs
PINDEL_FILE= sys.argv[1]
OUTPUT_FILE = sys.argv[2]
BROCA_GENES = sys.argv[3]
REFSEQ = sys.argv[4]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 5 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[PINDEL_FILE,BROCA_GENES,REFSEQ]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def openPindelFile(PINDEL_FILE):
        with open(PINDEL_FILE, "r+") as inputFile:
                for line in inputFile:
                        if "ChrID" in line:
                                #split the line into smaller segments and store important information as variables #ALL these are strs
                                line_array= line.strip("\r\n").split("\t") 
				event_size = line_array[1].split(" ")[1] 
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				#Only proceed if a)event_size is bigger than 10 bps or lesser than 100bp AND b)addition of unique reads for +and- strand for any sample is >= 3 reads(min supporting reads PER sample)
				adjust_reads = True
				if int(event_size) > 10 and ((int(line_array[19].split(" ")[4])+int(line_array[19].split(" ")[6])) >=3 or (int(line_array[20].split(" ")[4])+int(line_array[20].split(" ")[6])) >= 3):
					event_type = line_array[1].split(" ")[0] #This is either TD/I/D/INV
					if event_type == "I":
						event_type = "INS"
						adjust_reads = False
					elif event_type == "D":
						event_type = "DEL"
						event_size = str(int(event_size)*(-1))
					elif event_type == "INV":
						event_type = "INV"
					else:
						event_type = "DUP:TANDEM"
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                	chromosome = line_array[3].split(" ")[1]
                                	SV_start = line_array[4].split(" ")[1]
					if (adjust_reads):
                                                SV_end = str(int(line_array[5])-1)
                                        else:
                                                SV_end = str(int(line_array[5])) #pindel_ALL vcf subtracts 1 from sv end and so do we here but AnnotSV 3.0.9 adds back 1 to insertions from pindel_ALL so this change had to be made for the later merge to work
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------					
					#basically: read depth at left breakpoint of SV, read depth at right bp of SV, No of reads on + strand that map to alt allele, same but UNIQUE on + strand, No of reads on - strand that map to alt allele, same but UNIQUE on - strand
					#Let us take the unique no. Technically since we remove PCR dups at beginning of CP, our reads on + strand should be the same as the no of unique reads on + strand that map to alt allele but just in case.
					germline_positive_unique = line_array[19].split(" ")[4]
					germline_negative_unique = line_array[19].split(" ")[6]
					germline_total_reads = int(germline_positive_unique) + int(germline_negative_unique)

					tumor_positive_unique = line_array[20].split(" ")[4]
					tumor_negative_unique = line_array[20].split(" ")[6]
					tumor_total_reads = int(tumor_positive_unique) + int(tumor_negative_unique)
				
					germline_string = str(germline_total_reads) + "[" + germline_positive_unique + "," + germline_negative_unique + "]"
					tumor_string = str(tumor_total_reads) + "[" + tumor_positive_unique + "," + tumor_negative_unique + "]"
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					#Annotate each region
					information = a.annotate_region (chromosome,SV_start, SV_end,BROCA_GENES,REFSEQ) #you will get back a str "Gene \t Gene_Region \t Transcripts
					information = information.strip("\n")
					genes = information.split("\t")[0]
					gene_region = information.split("\t")[1]
					transcripts  = information.split("\t")[2]
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					string = chromosome + "\t" + SV_start + "\t" + SV_end + "\t" + genes + "\t" + event_size + "\t" + gene_region + "\t" + event_type + "\t" + transcripts + "\t" + germline_string + "\t" + tumor_string + "\n"
					if "Intergenic" not in string:
						writeToOutput(OUTPUT_FILE,string)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#INNER FUNC
def writeToOutput(OUTPUT_FILE,string):
	with open(OUTPUT_FILE, "a+") as outputFile:
		outputFile.write(string)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Main code
openPindelFile(PINDEL_FILE)
