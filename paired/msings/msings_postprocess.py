import sys
import pandas as pd 

TUMOR_PFX = sys.argv[1]
GERMLINE_PFX = sys.argv[2]
TUMOR_OUTPUT= sys.argv[3]
GERMLINE_OUTPUT= sys.argv[4]
MERGED_OUTPUT_FILE= sys.argv[5]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 6 :
    print "CHECK: Not all python args provided "
    sys.exit (1)


#File check
files=[TUMOR_OUTPUT,GERMLINE_OUTPUT]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


#Let us merge the 2 files together
msings_tumor = pd.read_csv(TUMOR_OUTPUT, sep= "\t", header = 0)
msings_germline = pd.read_csv(GERMLINE_OUTPUT, sep = "\t", header = 0)

#rename cols from bqsr_SAMPLE_PFX to SAMPLE_PFX
msings_tumor.columns = ["Position", TUMOR_PFX]
msings_germline.columns = ["Position", GERMLINE_PFX]

#Func
#Deal with: if no of unstable loci and pasing loci is 0, then msings output will NOT have a msing_score row!
def add_msings_score (msings_df, SAMPLE_PFX):
	if msings_df.iloc[2,0] != "msing_score":
		#add in the msing_score for it
		msings_upper = msings_df.iloc[0:2,:]
		msings_lower = msings_df.iloc[2:,:]
	
		new_line = [{"Position": "msing_score" , SAMPLE_PFX : ""}]
		msings_upper = msings_upper.append(new_line, ignore_index=True, sort=False)
	
		msings_df = pd.concat([msings_upper, msings_lower], ignore_index= True)

		#change the msi status from neg to undetermined
                msings_df.iloc[3,1] = "UNDETERMINED"
	else:
		msings_df = msings_df
	
	return msings_df


#Send each df through above func
msings_tumor = add_msings_score(msings_tumor, TUMOR_PFX)
msings_germline = add_msings_score(msings_germline, GERMLINE_PFX)

#Merge tumor and germline dfs
merged_msings = pd.merge(msings_tumor, msings_germline, on="Position", how="inner") 

#Add chr prefix to positions
for i in range(4,merged_msings.shape[0]):
	merged_msings.loc[i,"Position"] = "chr" + merged_msings.loc[i,"Position"]

merged_msings.to_csv(MERGED_OUTPUT_FILE, sep="\t", index= False)
