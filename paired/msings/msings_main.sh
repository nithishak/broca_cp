set -x
SAVEPATH=$1
TUMOR_PFX=$2
GERMLINE_PFX=$3

#variables used
#MSINGS
#MSINGS_BEDFILE
#MSINGS_BASELINE
#MSINGS_POSTPROCESS_SCRIPT

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 3 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$MSINGS|$MSINGS_POSTPROCESS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Information on how to read output in bqsr_germline/tumor.MSI_analysis txt
#no of unstable loci - totalt number of cells that have a 1
#passing loci - number of cells that has enough information to make a call (cells with 0s/1s, so excluding blanks)
#msi score - no of unstable loci/ passing loci
#msi status - threshold of 0.2

msings () {

       	source /mnt/disk4/labs/salipante/nithisha/programs/msings/msings-env/bin/activate
        mkdir $SAVEPATH/msings

        echo "$SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G.bam" > $SAVEPATH/msings/list_BAM_files.txt
        echo "$SAVEPATH/recal/bqsr_${TUMOR_PFX}_T.bam" >> $SAVEPATH/msings/list_BAM_files.txt


       	$MSINGS $SAVEPATH/msings/list_BAM_files.txt $MSINGS_BEDFILE $MSINGS_BASELINE $REF_GENOME_b37
	
	deactivate
	
	#Move newly formed dirs to msings from where command is carried out which is in recal/
        mv $SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G $SAVEPATH/msings/ & 
        mv $SAVEPATH/recal/bqsr_${TUMOR_PFX}_T $SAVEPATH/msings/ &
	
}


msings_postprocess () {
	#combine into 1 file
	TUMOR_OUTPUT=$SAVEPATH/msings/bqsr_${TUMOR_PFX}_T/bqsr_${TUMOR_PFX}_T.MSI_Analysis.txt
	GERMLINE_OUTPUT=$SAVEPATH/msings/bqsr_${GERMLINE_PFX}_G/bqsr_${GERMLINE_PFX}_G.MSI_Analysis.txt
	OUTPUT_FILE=$SAVEPATH/results/${TUMOR_PFX}_TG_MSI_Analysis.txt
        
	python $MSINGS_POSTPROCESS_SCRIPT $TUMOR_PFX $GERMLINE_PFX $TUMOR_OUTPUT $GERMLINE_OUTPUT $OUTPUT_FILE
}


#Main code
msings #Look for analysis file with ALL loci eg. results/$TUMOR_PFX/msings/bqsr_$TUMOR_PFX_T/bqsr_xxxxxxxxT_T.MSI_Analysis.txt
msings_postprocess
