#This script is to combine the T and G metrics columns from 2 sources (CollectHSMetrics and DuplicationMetrics) from 4 files(2 from T and 2 from G) and to retain rows we need
#We transpose line 8 from each file into a column, we combine info for T from 2 files horizontally, do the same thing for G, and then combine the T and G columns columns wise. Lastly, we choose only the rows we need.

import sys
import pandas as pd
import os #just for dir check

#Read in args
QC_METRICS_DIR_PATH = sys.argv[1]
TUMOR_PFX = sys.argv[2]
T_CHM = sys.argv[3] #CHM is CollectHsMetrics
G_CHM = sys.argv[4]
T_DM = sys.argv[5] #DM is DuplicationMetrics
G_DM = sys.argv[6]
T_IM = sys.argv[7] #IM is InsertMetrics
G_IM = sys.argv[8]
OUTPUT_FILE= sys.argv[9]

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 10 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[T_CHM,G_CHM,T_DM,G_DM]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)

#Dir check
        if os.path.exists(QC_METRICS_DIR_PATH) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#For CHM T
t_chm = pd.read_csv(T_CHM, sep="\t", header= 0, index_col = None)
t_chm = t_chm.drop(columns = ["LIBRARY","READ_GROUP","SAMPLE"])
t_chm = t_chm.transpose()
t_chm.columns = [ "T_Values"] #index in automatically set

#For DM T
t_dm = pd.read_csv(T_DM, sep="\t", header= 0, index_col = None)
t_dm = t_dm.drop(columns = "LIBRARY")
t_dm = t_dm.transpose()
t_dm.columns = [ "T_Values"] #index in automatically set

#For IM T
t_im = pd.read_csv(T_IM, sep="\t", header= 0, index_col = None)
t_im = t_im.drop(columns = ["LIBRARY","READ_GROUP","SAMPLE"])
t_im = t_im.transpose()
t_im.columns = ["T_Values"]

#Let us join CHM T, DM T and IM T column wise
t_info = pd.concat([t_chm,t_dm,t_im])

#For CHM G
g_chm = pd.read_csv(G_CHM, sep="\t", header= 0, index_col = None)
g_chm = g_chm.drop(columns = ["LIBRARY","READ_GROUP","SAMPLE"])
g_chm = g_chm.transpose()
g_chm.columns = [ "G_Values"] #index in automatically set

#For DM G
g_dm = pd.read_csv(G_DM, sep="\t", header= 0, index_col = None)
g_dm = g_dm.drop(columns = "LIBRARY")
g_dm = g_dm.transpose()
g_dm.columns = [ "G_Values"] #index in automatically set

#For IM G
g_im = pd.read_csv(G_IM, sep="\t", header= 0, index_col = None)
g_im = g_im.drop(columns = ["LIBRARY","READ_GROUP","SAMPLE"])
g_im = g_im.transpose()
g_im.columns = ["G_Values"]

#Let us join CHM G, DM G and IM G column wise
g_info = pd.concat([g_chm,g_dm,g_im])

#T info anf G info merge
tg_info = t_info.join(g_info, how= "inner")
#6th august 2021 - let us keep all cols from both files
tg_info_new = tg_info.sort_index()

#Choose rows you need, use index
#tg_info_new = tg_info.loc[["MEAN_TARGET_COVERAGE","PF_READS","PF_UNIQUE_READS","PCT_PF_UQ_READS","PF_UQ_READS_ALIGNED","PCT_SELECTED_BASES","PCT_OFF_BAIT","PCT_USABLE_BASES_ON_TARGET","ZERO_CVG_TARGETS_PCT","AT_DROPOUT","GC_DROPOUT","PERCENT_DUPLICATION","READ_PAIRS_EXAMINED","READ_PAIR_DUPLICATES","UNPAIRED_READS_EXAMINED","UNMAPPED_READS","UNPAIRED_READ_DUPLICATES","READ_PAIR_OPTICAL_DUPLICATES","ESTIMATED_LIBRARY_SIZE"],:]

##Add in some documentation
#http://broadinstitute.github.io/picard/picard-metric-definitions.html#DuplicationMetrics

#tg_info_new["Column_Explanation"] = ["The mean coverage of a target region","The total number of reads that pass the vendor's filter","The number of PF reads that are not marked as duplicates","The fraction of PF_UNIQUE_READS from the TOTAL_READS, PF_UNIQUE_READS/TOTAL_READS","The number of PF_UNIQUE_READS that aligned to the reference genome with a mapping score > 0","The fraction of PF_BASES_ALIGNED located on or near a baited region (ON_BAIT_BASES + NEAR_BAIT_BASES)/PF_BASES_ALIGNED","The fraction of PF_BASES_ALIGNED that are mapped away from any baited region, OFF_BAIT_BASES/PF_BASES_ALIGNED","The number of aligned, de-duped, on-target bases out of all of the PF bases available","The fraction of targets that did not reach coverage=1 over any base","A measure of how undercovered <= 50% GC regions are relative to the mean. For each GC bin [0..50] we calculate a = % of target territory, and b = % of aligned reads aligned to these targets","A measure of how undercovered >= 50% GC regions are relative to the mean. For each GC bin [50..100] we calculate a = % of target territory, and b = % of aligned reads aligned to these targets","The fraction of mapped sequence that is marked as duplicate","The number of mapped read pairs examined. (Primary, non-supplemental)","The number of read pairs that were marked as duplicates","The number of mapped reads examined which did not have a mapped mate pair, either because the read is unpaired, or the read is paired to an unmapped mate","The total number of unmapped reads examined. (Primary, non-supplemental)","The number of fragments that were marked as duplicates","The number of read pairs duplicates that were caused by optical duplication. Value is always < READ_PAIR_DUPLICATES, which counts all duplicates regardless of source","The library on which the duplicate marking was performed"]

#write to file
tg_info_new.to_csv(OUTPUT_FILE, sep="\t")
	
