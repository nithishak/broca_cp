set -x -e
#The purpose of this script is to generate qc metrics from 2 documents per tumor/germline paired sample - savepath/picard/output_t_hs_metrics.txt and savepath/picard/marked_dup_metrics_tumor_pfx_tumor.txt
#the 2 files above are generated from CP box, specifically by the commands CollectHSMetrics and Duplication metrics respectively
#We extract line 8 of both files - this is the only line we need

#Read in args
SAVEPATH=$1
TUMOR_PFX=$2
GERMLINE_PFX=$3
G_CollectHsMetricsFile=$4
T_CollectHsMetricsFile=$5
G_DuplicationMetricsFile=$6
T_DuplicationMetricsFile=$7
G_InsertionMetricsFile=$8
T_InsertionMetricsFile=$9

#variables used
#QC_METRICS_POSTPROCESS_SCRIPT

QC_METRICS_DIR_PATH=$SAVEPATH/QC_metrics
mkdir -p $QC_METRICS_DIR_PATH

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 9 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$QC_METRICS_POSTPROCESS_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$QC_METRICS_DIR_PATH"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

amend_files () {
	#CollectHsMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
	sed -n "7,9p" $T_CollectHsMetricsFile > $QC_METRICS_DIR_PATH/${TUMOR_PFX}_T_hs_metrics.txt
	sed -n "7,9p" $G_CollectHsMetricsFile > $QC_METRICS_DIR_PATH/${GERMLINE_PFX}_G_hs_metrics.txt

	#DuplicationMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
	sed -n "7,9p" $T_DuplicationMetricsFile > $QC_METRICS_DIR_PATH/dup_metrics_${TUMOR_PFX}_T.txt
	sed -n "7,9p" $G_DuplicationMetricsFile > $QC_METRICS_DIR_PATH/dup_metrics_${GERMLINE_PFX}_G.txt

	#InsertMetrics - Amend required files and save them in this dir - just take the lines that you need and generate a new text file
	sed -n "7,8p" $T_InsertionMetricsFile > $QC_METRICS_DIR_PATH/ins_metrics_${TUMOR_PFX}_T.txt
        sed -n "7,8p" $G_InsertionMetricsFile > $QC_METRICS_DIR_PATH/ins_metrics_${GERMLINE_PFX}_G.txt
}


postprocess () {
	#Use a python script to combine these files and delete rows you do not need
	python $QC_METRICS_POSTPROCESS_SCRIPT $QC_METRICS_DIR_PATH $TUMOR_PFX $QC_METRICS_DIR_PATH/${TUMOR_PFX}_T_hs_metrics.txt $QC_METRICS_DIR_PATH/${GERMLINE_PFX}_G_hs_metrics.txt $QC_METRICS_DIR_PATH/dup_metrics_${TUMOR_PFX}_T.txt $QC_METRICS_DIR_PATH/dup_metrics_${GERMLINE_PFX}_G.txt $QC_METRICS_DIR_PATH/ins_metrics_${TUMOR_PFX}_T.txt $QC_METRICS_DIR_PATH/ins_metrics_${GERMLINE_PFX}_G.txt $SAVEPATH/results/${TUMOR_PFX}_TG_qc_metrics.txt

}

remove_files () {
	rm -r $QC_METRICS_DIR_PATH
}


#Main code
amend_files
postprocess
#remove_files
