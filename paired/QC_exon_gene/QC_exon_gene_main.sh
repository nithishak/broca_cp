set -x -e
#Read inputs
TUMOR_PFX=$1
GERMLINE_PFX=$2
SAVEPATH=$3 #/CP/results/TUMOR_PFX

#Programs and files needed
#SAVEROOT
#BROCA_GENES
#REFSEQ
#BEDTOOLS
#BEDFILE_0B
#QC_PER_EXON_GENE_SCRIPT
#MERGE_FILE

QC_EXON_GENE_PATH=$SAVEPATH/QC_exon_gene
mkdir -p $QC_EXON_GENE_PATH

#Find tumor and germline bams
TUMOR_BAM=$SAVEPATH/recal/bqsr_${TUMOR_PFX}_T.bam
GERMLINE_BAM=$SAVEPATH/recal/bqsr_${GERMLINE_PFX}_G.bam

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if [ "$#" -ne 3 ];then
        echo "Not all parameters were provided"
        exit 1
fi


IFS="|"
#File check
FILES="$BEDTOOLS|$BEDFILE_0B|$QC_PER_EXON_GENE_SCRIPT|$MERGE_FILE|$TUMOR_BAM|$GERMLINE_BAM"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$QC_EXON_GENE_PATH"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

run_qc () {
	##bedtools specifies that a 0 based bed file should be used - Specifically, bedtools uses the UCSC Genome Browser’s internal database convention of making the start position 0-based and the end position 1-based: (http://genome.ucsc.edu/FAQ/FAQtracks#tracks1)
        #https://bedtools.readthedocs.io/en/latest/content/overview.html
	#https://bedtools.readthedocs.io/en/latest/content/general-usage.html - The start position in each BED feature is therefore interpreted to be 1 greater than the start position listed in the feature. For example, start=9, end=20 is interpreted to span bases 10 through 20,inclusive.
        $BEDTOOLS coverage -a $BEDFILE_0B -b $TUMOR_BAM -mean > $QC_EXON_GENE_PATH/${TUMOR_PFX}_T_qc.txt #the output has cols chromosome start stop gene ave_read_depth #actual file does NOT have a header
        $BEDTOOLS coverage -a $BEDFILE_0B -b $GERMLINE_BAM -mean > $QC_EXON_GENE_PATH/${GERMLINE_PFX}_G_qc.txt
}

fit_qc_exons_genes () {
	python $QC_PER_EXON_GENE_SCRIPT $SAVEROOT/QC_bedfile_exons.bed $SAVEROOT/QC_bedfile_genes.bed $QC_EXON_GENE_PATH/${GERMLINE_PFX}_G_qc.txt $QC_EXON_GENE_PATH/${TUMOR_PFX}_T_qc.txt $GERMLINE_PFX $TUMOR_PFX $QC_EXON_GENE_PATH
}


merge_files () {
        python $MERGE_FILE $QC_EXON_GENE_PATH/${TUMOR_PFX}_T_exon_qc.txt $QC_EXON_GENE_PATH/${GERMLINE_PFX}_G_exon_qc.txt $QC_EXON_GENE_PATH/${TUMOR_PFX}_T_gene_qc.txt $QC_EXON_GENE_PATH/${GERMLINE_PFX}_G_gene_qc.txt $TUMOR_PFX $SAVEPATH/results/
}

rm_files () {
	rm -r $QC_EXON_GENE_PATH
}




#Main functions
run_qc
fit_qc_exons_genes
merge_files
rm_files
