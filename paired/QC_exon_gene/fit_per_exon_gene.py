#This script is to take the bedfile (that has undergone bedtools coverage and so now each bait in the bedfile has an average read depth to it) and fit them to QC per exon
#we already have a bedfile for exon ranges for our assay, let us use that

#Formats of QC_TUMOR_FILE/QC_GERMLINE_FILE: 
#Chrom	Start	Stop	Gene	Avg_Read_Depth
#Actual file has no header, first 4 cols are from the bedfile itself, gene col is NOT usable, has chrxxx-xxx in some rows! use gene names from gene bedfile.

#Format of EXON_BEDFILE:
#Chrom	Start	Stop	Gene	Exon_Number
#Actual file has no header

#Format of GENE_BEDFILE:
#Chrom	Start	Stop	Gene
#Actual file has no header

import sys
import os #just for dir check

#Read in args
EXON_BEDFILE= sys.argv[1]
GENE_BEDFILE= sys.argv[2]

QC_GERMLINE_FILE = sys.argv[3]
QC_TUMOR_FILE= sys.argv[4]

GERMLINE_PFX = sys.argv[5]
TUMOR_PFX= sys.argv[6]

QC_EXON_GENE_PATH = sys.argv[7] 

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 8 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[EXON_BEDFILE,GENE_BEDFILE,QC_GERMLINE_FILE,QC_TUMOR_FILE]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)

#Dir check
        if os.path.exists(QC_EXON_GENE_PATH) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#Create output file
def create_output(PFX, sample_type, analysis_type ): #write "T" for tumor for arg sample_type, write "exon" for exon analysis or "gene" for gene analysis for arg type_of_analysis
	global output_file_name
	output_file_name = QC_EXON_GENE_PATH + "/" + PFX + "_" + sample_type + "_" + analysis_type + "_qc.txt"
	with open(output_file_name, "w") as output:
		if analysis_type == "exon":
			output.write("Chromosome	Start	Stop	Gene	Exon_Number	Avg_Read_Depth	Segment_Start	Segment_End\n")
		else:
			output.write("Chromosome	Start	Stop	Gene	Avg_Read_Depth	Segment_Start	Segment_End\n")
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
def qc_per_feature(bedfile, qc_file): #bedfile is either exon or germline, qc_file is either tumor or germline, INPUT CORRECT FILES
	with open (bedfile, "r") as exon_gene_ranges:
		for range_line in exon_gene_ranges: #for each exon or each gene
			#print "--------------------------------------------------------------------------------------------------------------------------"
			#print range_line
			segment_start = 100000000000000000000000000
			segment_end = -10000000000000000000000000000000

			per_feature_bait_length = 0 #cumulative across all baits that make up this feature (feature here is either exon or gene)
			per_feature_read_depth = 0 #cumulative across all baits that make up this feature (feature here is either exon or gene)
			
			range_split = range_line.rstrip("\n").split("\t")
			range_chr = range_split[0] #never make chr str as you have chr X and Y!
			range_start = int(range_split[1])
			range_stop = int(range_split[2])
			
			with open(qc_file, "r") as QC_file:
				for QC_line in QC_file:
					QC_line_split = QC_line.strip("\n").split("\t")
					QC_chr = QC_line_split[0]
					QC_start = int(QC_line_split[1])
					QC_stop = int(QC_line_split[2])
					QC_avg_read_depth = float(QC_line_split[4])
					
					#CASE1 : if start of bait is within exon/gene
	                        	if range_chr == QC_chr and QC_start >= range_start and QC_start <= range_stop:
                                		if QC_start < segment_start:
                                        		segment_start = QC_start
                                		if QC_stop > segment_end:
                                        		segment_end = QC_stop

						bait_length = QC_stop - QC_start
                                                per_feature_bait_length = per_feature_bait_length + bait_length

                                                baitlength_x_avgdepth = bait_length * QC_avg_read_depth + 1 
                                                per_feature_read_depth = per_feature_read_depth + baitlength_x_avgdepth						

                        		#CASE2: if stop of bait is within exon/gene
                        		elif range_chr == QC_chr and QC_stop >= range_start and QC_stop <= range_stop:
                                		if QC_start < segment_start:
                                        		segment_start = QC_start
                                		if QC_stop > segment_end:
                                        		segment_end = QC_stop

						bait_length = QC_stop - QC_start + 1 
                                                per_feature_bait_length = per_feature_bait_length + bait_length

                                                baitlength_x_avgdepth = bait_length * QC_avg_read_depth
                                                per_feature_read_depth = per_feature_read_depth + baitlength_x_avgdepth


                        		#CASE3: if the entire exon in within the bait 
                        		elif range_chr == QC_chr and QC_start < range_start and QC_stop > range_stop:
                                		if QC_start < segment_start:
                                        		segment_start = QC_start
                                		if QC_stop > segment_end:
                                        		segment_end = QC_stop

						bait_length = QC_stop - QC_start + 1
                                                per_feature_bait_length = per_feature_bait_length + bait_length

                                                baitlength_x_avgdepth = bait_length * QC_avg_read_depth
                                                per_feature_read_depth = per_feature_read_depth + baitlength_x_avgdepth

                        		#CASE4: if entire bait is within exon/gene
                                		#In this case, the segment first satisfies and passes through Case 1 if and so it gets appended anyway. 
					


			#so for each feature(exon/gene)
			#print per_feature_read_depth
			if per_feature_read_depth != 0:
				per_feature_avg_read_depth = per_feature_read_depth/per_feature_bait_length
				string = range_line.rstrip("\n") + "\t" + str(per_feature_avg_read_depth) + "\t" + str(segment_start) + "\t" + str(segment_end) + "\n"
		
				with open(output_file_name, "a+") as output:
					output.write(string)
			else:
				string = string = range_line.rstrip("\n") + "\t" + str(0) + "\t" + str(0) + "\t" + str(0) + "\n"
				with open(output_file_name, "a+") as output:
                                        output.write(string)
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
				
#Main code

#1. Tumor, exon analysis
create_output(TUMOR_PFX, "T", "exon")
qc_per_feature(EXON_BEDFILE, QC_TUMOR_FILE)

#2. Tumor, gene analysis
create_output(TUMOR_PFX, "T", "gene")
qc_per_feature(GENE_BEDFILE, QC_TUMOR_FILE)

#3. Germline, exon analysis
create_output(GERMLINE_PFX, "G", "exon")
qc_per_feature(EXON_BEDFILE, QC_GERMLINE_FILE)

#4. Germline, gene analysis
create_output(GERMLINE_PFX, "G", "gene")
qc_per_feature(GENE_BEDFILE, QC_GERMLINE_FILE)
