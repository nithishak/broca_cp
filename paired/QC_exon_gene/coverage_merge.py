#The purpose of this script is to merge exon qc results from T and G onto the same excel sheet and likewise for gene qc.
import sys
import pandas as pd
import numpy as np
import os #just for dir check

#Read in args
T_EXON_QC_FILE = sys.argv[1]
G_EXON_QC_FILE = sys.argv[2]

T_GENE_QC_FILE = sys.argv[3]
G_GENE_QC_FILE = sys.argv[4]

TUMOR_PFX = sys.argv[5]
OUTPUT_DIR_PATH = sys.argv[6] #CP/results/TUMOR_PFX/results/

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#Parameter check
if len (sys.argv) != 7 :
    print "CHECK: Not all python args provided "
    sys.exit (1)

#File check
files=[T_EXON_QC_FILE,G_EXON_QC_FILE,T_GENE_QC_FILE,G_GENE_QC_FILE]
for filee in files:
        try:
                with open(filee) as f:
                        pass
        except IOError as e:
                print "ERROR:Unable to open file" #Does not exist OR no read permissions
                sys.exit(1)

#Dir check
        if os.path.exists(OUTPUT_DIR_PATH) == False:
                print "ERROR:One of required directories not present"
                sys.exit(1)
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

#Main code
def qc_exon (T_EXON_QC_FILE,G_EXON_QC_FILE):
	#For QC per exon
	t_exon_qc = pd.read_csv(T_EXON_QC_FILE, sep = "\t", header = 0,index_col= None, dtype ="str")
	g_exon_qc = pd.read_csv(G_EXON_QC_FILE, sep = "\t", header =0,index_col= None, dtype= "str")

	exon_qc = pd.merge (t_exon_qc,g_exon_qc, on=["Chromosome","Start","Stop","Gene","Exon_Number"], how = "outer", suffixes = ["_T","_G"])
	#Let us create a new col called Segment_Start. If either Segment_Start_T or Segment_Start_G is 0, this means there were no reads found for T or G for that exon respectively. The avg_read_depth_T or _G col will correctly show 0 but you want the segment start or end to simply show the range for whatever sample that has reads
	#If both Segment_Start_T and Segment_Start_G are 0, this means there were no reads for both samples, then this will also work anyway.
	conditions = [(exon_qc["Segment_Start_T"] == 0), (exon_qc["Segment_Start_G"] == 0)]
	choices = [exon_qc["Segment_Start_G"], exon_qc["Segment_Start_T"]]
	exon_qc["Segment_Start"] = np.select(conditions,choices,default=exon_qc["Segment_Start_T"])
	
	#Let us create a new col called Segment_End
	conditions = [(exon_qc["Segment_Start_T"] == 0), (exon_qc["Segment_Start_G"] == 0)] #if Segment_Start_T is 0, then obviously Segment_End_T is also 0
        choices = [exon_qc["Segment_End_G"], exon_qc["Segment_End_T"]]
        exon_qc["Segment_End"] = np.select(conditions,choices,default=exon_qc["Segment_End_T"])

	#Let us clean up a little, make the avg_read_deptth an interger and combine the chromosome, start and stop positions
	exon_qc["Position"] = "chr" + exon_qc["Chromosome"] + ":" + exon_qc["Start"] + "-" + exon_qc["Stop"]
	exon_qc["Segment_Position"] = "chr" + exon_qc["Chromosome"] + ":" + exon_qc["Segment_Start"] + "-" + exon_qc["Segment_End"]
	exon_qc["Avg_Read_Depth_T"] = exon_qc["Avg_Read_Depth_T"].astype(float).round().astype(int).astype(str)
	exon_qc["Avg_Read_Depth_G"] = exon_qc["Avg_Read_Depth_G"].astype(float).round().astype(int).astype(str)
		
	exon_qc_rearranged = exon_qc[["Position","Segment_Position","Gene","Exon_Number","Avg_Read_Depth_T","Avg_Read_Depth_G"]]
	
	#Write to file
	output_file_name = OUTPUT_DIR_PATH + TUMOR_PFX + "_TG_" + "exon_qc" + ".txt"
	exon_qc_rearranged.to_csv(output_file_name, sep = "\t", index=None)


def qc_gene (T_GENE_QC_FILE,G_GENE_QC_FILE):
	#For QC per geneG
	t_gene_qc = pd.read_csv(T_GENE_QC_FILE, sep = "\t", header= 0, index_col= None, dtype= "str")
	g_gene_qc = pd.read_csv(G_GENE_QC_FILE, sep = "\t", header =0, index_col= None, dtype= "str")

	gene_qc = pd.merge (t_gene_qc,g_gene_qc, on=["Chromosome","Start","Stop","Gene"], how = "outer", suffixes= ["_T","_G"])
	#Let us create a new col called Segment_Start. If either Segment_Start_T or Segment_Start_G is 0, this means there were no reads found for T or G for that gene respectively. The avg_read_depth_T or _G col will correctly show 0 but you want the segment start or end to simply show the range for whatever sample that has reads
        #If both Segment_Start_T and Segment_Start_G are 0, this means there were no reads for both samples, then this will also work anyway.
        conditions = [(gene_qc["Segment_Start_T"] == 0), (gene_qc["Segment_Start_G"] == 0)]
        choices = [gene_qc["Segment_Start_G"], gene_qc["Segment_Start_T"]]
        gene_qc["Segment_Start"] = np.select(conditions,choices,default=gene_qc["Segment_Start_T"])

        #Let us create a new col called Segment_End
        conditions = [(gene_qc["Segment_Start_T"] == 0), (gene_qc["Segment_Start_G"] == 0)] #if Segment_Start_T is 0, then obviously Segment_End_T is also 0
        choices = [gene_qc["Segment_End_G"], gene_qc["Segment_End_T"]]
        gene_qc["Segment_End"] = np.select(conditions,choices,default=gene_qc["Segment_End_T"])

	#Let us clean up a little, make the avg_read_deptth an interger and combine the chromosome, start and stop positions
	gene_qc["Position"] = "chr" + gene_qc["Chromosome"] + ":" + gene_qc["Start"] + "-" + gene_qc["Stop"]
	gene_qc["Segment_Position"] = "chr" + gene_qc["Chromosome"] + ":" + gene_qc["Segment_Start"] + "-" + gene_qc["Segment_End"]
	gene_qc["Avg_Read_Depth_T"] = gene_qc["Avg_Read_Depth_T"].astype(float).round().astype(int).astype(str)
	gene_qc["Avg_Read_Depth_G"] = gene_qc["Avg_Read_Depth_G"].astype(float).round().astype(int).astype(str)


	gene_qc_rearranged = gene_qc[["Position","Segment_Position","Gene","Avg_Read_Depth_T","Avg_Read_Depth_G"]]

	#Write to file
        output_file_name = OUTPUT_DIR_PATH + TUMOR_PFX + "_TG_" + "gene_qc" + ".txt"
        gene_qc_rearranged.to_csv(output_file_name, sep = "\t", index=None)

#---------------------------------------------------------------------------------------------------------------
qc_exon(T_EXON_QC_FILE,G_EXON_QC_FILE)
qc_gene(T_GENE_QC_FILE,G_GENE_QC_FILE)
