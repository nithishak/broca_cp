#!/bin/bash
RUN=$1
JOBS_IN_BATCH=$2
RUN_TYPE=$3
#Read in a document called paths.sh from the directory paths, that contains information for all necessarly paths that are common for paried/unpaired sample, provide RUN as an arg
source paths/common_paths.sh $RUN $RUN_TYPE

set -x -e

#create a place for the run
mkdir -p $RUN_LOG_DIR

#create a dir for results
mkdir -p $SAVEROOT

#create a dir for final results
mkdir -p $RUN_RESULTS

############################################################################CHECKS#####################################################################################
#Parameter check
if [ "$#" -ne 3 ]; then
        echo "Not all parameters were provided"
        exit 1
fi


#File check
IFS="|"
FILES="$LIST_OF_SAMPLES|$LIST_OF_ORDERED_SAMPLES|$PATH_TO_CP_PAIRED_PIPELINE|$PATH_TO_CP_UNPAIRED_PIPELINE|$COMBINE_FINAL_OUTPUT_SCRIPT"
for file in $FILES
        do
                if [ ! -f $file ];then
                        echo "ERROR:One of the required main files is missing.Please check"
                        exit 1
                fi
done

#Dir check
DIRS="$DATAPATH|$SAVEROOT|$RUN_LOG_DIR|$RUN_RESULTS"
for dir in $DIRS
        do
                if [ ! -d $dir ];then
                        echo "ERROR:One of the required directories is missing.Please check"
                        exit 1
                fi
done
unset IFS

#######################################################################################################################################################################
#Main code

run_once_per_run () {
	#Run once per run script first to generate exon and gene bedfiles as well as txFile needed for AnnotSV
        $RUN_ONCE_PER_RUN_SCRIPT $RUN $RUN_TYPE $SAVEROOT #last arg is output dir

}

gene_number_check () {
	#Check that all the genes in  the broca gene file are present in refseq
	NO_GENES_BROCA_GENES=$(wc -l < $BROCA_GENES)
	NO_GENES_QC_GENES_FILE=$(wc -l < $SAVEROOT/QC_bedfile_genes.bed)
	if [ ! $(($NO_GENES_BROCA_GENES - $NO_GENES_QC_GENES_FILE)) -eq 1 ];then
		echo "ERROR - CHECK YOUR BROCA GENES FILE, ONE OF MORE GENE NAMES NEED TO BE REPLACED WITH WHAT IS IN YOUR REFSEQ FILE!!!!"
	else
		echo "Gene count ok, proceed"
	fi
} 

run_CP_pipeline() {
	#initialize a counter
	count=0
	while IFS='' read -r line
	do
        	TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
        	GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')
		
		#####################PAIRED SAMPLE##########################
		if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
			#Let us run the command
			nohup $PATH_TO_CP_PAIRED_PIPELINE $TUMOR_PFX $GERMLINE_PFX $RUN $RUN_TYPE >>./${RUN}_logs/${TUMOR_PFX}_TG_nohup.log 2>>./${RUN}_logs/${TUMOR_PFX}_TG_nohup.log &
			count=$((count+2))
		
		#####################SINGLE SAMPLE##########################
		else
			SAMPLE_PFX=$(echo $line|tr -d '|')
			#Let us run the command
			nohup $PATH_TO_CP_UNPAIRED_PIPELINE $SAMPLE_PFX $RUN $RUN_TYPE >>./${RUN}_logs/${SAMPLE_PFX}_UN_nohup.log 2>>./${RUN}_logs/${SAMPLE_PFX}_UN_nohup.log &
			count=$((count+1))
			
		fi 
	
	if [ $count -ge $JOBS_IN_BATCH ]
	then
	wait
	count=0
	fi
	done< $LIST_OF_SAMPLES
}

quick_final_bam_file_check () {
        rm -f ${RUN}_missing_bam_files.txt
        #check if bam files are all produced, this is for when we don't run downstream processing and want to pass bam files to cnvkit/purecn
        while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')
                 #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
                        if [ ! -f $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_T.bam ] || [ ! -f $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_G.bam ];then
                                echo "bqsr_${TUMOR_PFX}_T.bam or bqsr_${GERMLINE_PFX}_G.bam missing, please check" >> ${RUN}_missing_bam_files.txt
                        fi
                #####################SINGLE SAMPLE##########################
                else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
                        if [ ! -f $SAVEROOT/$SAMPLE_PFX/recal/bqsr_${SAMPLE_PFX}_UN.bam ];then
                                echo "bqsr_${SAMPLE_PFX}_UN.bam missing, please check" >> ${RUN}_missing_bam_files.txt
                        fi

                fi
        done< $LIST_OF_SAMPLES
}


quick_labv_bam_file_check () {
	rm -f ${RUN}_missing_labv_bam_files.txt
	#check if bam files are all produced, this is for when we don't run downstream processing and want to pass bam files to cnvkit/purecn
	while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')
                 #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
                        if [ ! -f $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_T_labv.bam ] || [ ! -f $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_G_labv.bam ];then
				echo "bqsr_${TUMOR_PFX}_T_labv.bam or bqsr_${GERMLINE_PFX}_G_labv.bam missing, please check" >> ${RUN}_missing_labv_bam_files.txt
			fi
		#####################SINGLE SAMPLE##########################
                else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
                        if [ ! -f $SAVEROOT/$SAMPLE_PFX/recal/bqsr_${SAMPLE_PFX}_UN.bam ];then
				echo "bqsr_${SAMPLE_PFX}_UN_labv.bam missing, please check" >> ${RUN}_missing_labv_bam_files.txt
			fi
	
		fi
	done< $LIST_OF_SAMPLES
}


quick_check_final_output () {
        while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')

                #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
                        if [ ! -f $SAVEROOT/$TUMOR_PFX/$TUMOR_PFX.xlsx ];then
				echo "$SAVEROOT/${RUN}_final_output/$TUMOR_PFX.xlsx not here"
				echo -e "\n" >> ${RUN}_failed_samples.log
                                echo "Pipeline did NOT run for this paired sample - $TUMOR_PFX, $GERMLINE_PFX" >> ${RUN}_failed_samples.log
                                echo "nohup $PATH_TO_CP_PAIRED_PIPELINE $TUMOR_PFX $GERMLINE_PFX $DATAPATH $SAVEROOT $CORES >>./${RUN}_logs/${TUMOR_PFX}_TG_nohup.log 2>>./${RUN}_logs/${TUMOR_PFX}_TG_nohup.log &" >> ${RUN}_failed_samples.log
                                echo "=========================================================================last few lines of log==================================================================" >> ${RUN}_failed_samples.log
                                tail -n 5 ./${RUN}_logs/${TUMOR_PFX}_TG_nohup.log >> ${RUN}_failed_samples.log
                                echo "============================================================================================================================================================" >> ${RUN}_failed_samples.log
                        fi
                #####################SINGLE SAMPLE##########################
                else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
                        if [ ! -f $SAVEROOT/$SAMPLE_PFX/$SAMPLE_PFX.xlsx ];then
                        	echo "$SAVEROOT/${RUN}_final_output/$SAMPLE_PFX.xlsx not here"
				echo -e "\n" >> ${RUN}_failed_samples.log
                                echo "Pipeline did NOT run for this unpaired sample - $SAMPLE_PFX" >> ${RUN}_failed_samples.log
                                echo "nohup $PATH_TO_CP_UNPAIRED_PIPELINE $SAMPLE_PFX $DATAPATH $SAVEROOT $CORES >>./${RUN}_logs/${SAMPLE_PFX}_UN_nohup.log 2>>./${RUN}_logs/${SAMPLE_PFX}_UN_nohup.log &" >> ${RUN}_failed_samples.log
                                echo "=========================================================================last few lines of log==================================================================" >> ${RUN}_failed_samples.log
                                tail -n 5 ./${RUN}_logs/${SAMPLE_PFX}_UN_nohup.log >> ${RUN}_failed_samples.log
                                echo "============================================================================================================================================================" >> ${RUN}_failed_samples.log
                        fi

                fi

        done < $LIST_OF_SAMPLES
}

move_labv_bam_files () {
	#mv instead of copy bam files so that they do not interfere with copy files (step 1 of cnvkit script)
	while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')
		 #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
			if [ ! -f $RUN_RESULTS/bqsr_${TUMOR_PFX}_T_labv.bam ];then
	                	mv $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_T_labv.bam $RUN_RESULTS
        	                mv $SAVEROOT/$TUMOR_PFX/recal/bqsr_${TUMOR_PFX}_T_labv.bai $RUN_RESULTS
			fi

			if [ ! -f $RUN_RESULTS/bqsr_${GERMLINE_PFX}_G_labv.bam ];then
                	        mv $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_G_labv.bam $RUN_RESULTS
                        	mv $SAVEROOT/$TUMOR_PFX/recal/bqsr_${GERMLINE_PFX}_G_labv.bai $RUN_RESULTS
			fi
                #####################SINGLE SAMPLE##########################
                else
                	SAMPLE_PFX=$(echo $line|tr -d '|')
			if [ ! -f $RUN_RESULTS/bqsr_${SAMPLE_PFX}_UN_labv.bam ];then
                        	mv $SAVEROOT/$SAMPLE_PFX/recal/bqsr_${SAMPLE_PFX}_UN_labv.bam $RUN_RESULTS
                        	mv $SAVEROOT/$SAMPLE_PFX/recal/bqsr_${SAMPLE_PFX}_UN_labv.bai $RUN_RESULTS
			fi

                fi

        done < $LIST_OF_SAMPLES
}


move_output_files () {
        while IFS='' read -r line
        do
                TUMOR_PFX=$(echo $line| awk -F '|' '{print $1}')
                GERMLINE_PFX=$(echo $line| awk -F '|' '{print $2}')

                #####################PAIRED SAMPLE##########################
                if [ ! -z $TUMOR_PFX ] && [ ! -z $GERMLINE_PFX ];then
                        if [ ! -f $RUN_RESULTS/$TUMOR_PFX.xlsx ];then
                                cp $SAVEROOT/$TUMOR_PFX/$TUMOR_PFX.xlsx $RUN_RESULTS
                        fi
                #####################SINGLE SAMPLE##########################
                else
                        SAMPLE_PFX=$(echo $line|tr -d '|')
                        if [ ! -f $RUN_RESULTS/$SAMPLE_PFX.xlsx ];then
                                cp $SAVEROOT/$SAMPLE_PFX/$SAMPLE_PFX.xlsx $RUN_RESULTS
                        fi

                fi

        done < $LIST_OF_SAMPLES
}

combine_output() {
	$COMBINE_FINAL_OUTPUT_SCRIPT $RUN $RUN_RESULTS
}


cnvkit() {
	nohup ./cnvkit/cnvkit_main.sh $RUN >>cnvkit.log 2>>cnvkit.log &
}

purecn() {

	./purecn/purecn_main.sh
}



#Main code
run_once_per_run
gene_number_check 

run_CP_pipeline
wait

quick_final_bam_file_check
quick_labv_bam_file_check
quick_check_final_output

move_labv_bam_files
move_output_files

combine_output
wait

cnvkit
wait

##find $DATAPATH -type f -name '*.fastq.gz' -delete ##Medgenome provides fastq.gz, dont delete them
