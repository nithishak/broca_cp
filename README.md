BROCA clinical pipeline README
==============================

This BROCA clinical pipeline is designed to take in either a paired or unpaired tumor/germline sample and generates a comprehensive report that contains analysis on different structural variant anlaysis.
Several Bioinformatic tools have been integrated in this pipeline to achieve this and are geared towards improving sensitivity of results to ensure rare mutations that occur at low variant allele frequencies are not missed.

In a nutshell, here are the tools this pipeline uses and the type of analysis they produce:  

![](https://bitbucket.org/nithishak/broca_cp/downloads/BROCA_CP.png)

Dependencies
-------------
1. Python 2.7
        - pandas 0.24.2
        - numpy 1.16.6
        - xlrd 1.2.0 #allows Python to read Excel files
	- xlsxwriter 2.0.0
        - pysam 0.19.0
        - matplotlib 2.2.5
	- future 0.18.2 #for cnvkit 
2. Python 3.6 (local install in case newer Python version is needed, currently not used for any tool))
3. R 3.4.4
4. R 3.5.1 (local install in case newer R version is needed, currently not used for any tool)
5. BBMap 38.23
6. Cutadapt 1.18
7. BWA 0.7.17
8. Samtools 0.1.9 (htslib 1.15)
9. GATK 4.0.8.1
10. Vardict 1.8.2
11. Varscan 2.3.9
12. Tabix 0.2.6
13. Bcftools 1.9
14. Annovar(2019Oct24)
15. GRIDSS 2.7.3 (remember to add bwa to PATH)
16. AnnotSV 3.0.9
17. Bedtools 2.27.1
18. Pindel 0.3
19. msings
20. CNVKIT 0.9.8
21. Java 8
22. PureCN Docker image markusriester/purecn:latest

(other dependencies: cURL 7.50.3)


Input files
------------
1. A file containing all samples in a run. The file should be named RUN.txt and may contain paired/unpaired samples. Each new unpaired/paired sample should be represented on a new line and the format is tumor_id|germline_id. This would look like:  
	* unpaired_tumor_id|
	* |unpaired_germline_id
	* paired_tumor_id|paired_germline_id 

2. A file containing samples(paired/unpaired) in the order that you want to see them in the final oiutput file. Each line should only hold one sample - this can be an unpaired sample, tumor sample of a tumor/germline pair ir a germline sample of a tumor/germline pair

3. A human reference genome sequence (human_g1k_v37.fasta)  

4. A Bedfile - this file should not have a "chr" prefix and should be zero index based. Depending on whether RUN_TYPE is HR or BROCA, different broca_genes file (input files line 6) and different bedfiles and bait files will be utilized for the analysis.

5. Known variant files for chosen version of human reference genome. In this case, the files used are:
	* 1000G_phase1.indels.b37.vcf & 1000G_phase1.indels.b37.vcf.idx 
	* dbsnp_138.b37.vcf & dbsnp_138.b37.vcf
	* Mills_and_1000G_gold_standard.indels.b37.vcf & Mills_and_1000G_gold_standard.indels.b37.vcf.idx 

6. A tab-demilited list of genes from the lab assay, headers should be  "Gene	Chr	Partition start	partition end	partition refseq". Partition refseq is the preferred transcript and chromosomes should be written without a "chr" prefix.

7. A refseq file downloaded from the genome browser >> table browser >> options: Human/NCBI RefSeq

8. 3 files are required for msings:
	* broca_v8_all_MSI_loci.msi_intervals 
	* broca_v8_all_MSI_loci.sorted.bed
	* brocah_v8_all_MSI_loci_baseline_v2.txt

How to run the pipeline
------------------------
1. Run the CP wrapper (usually, the number of samples is set to 10)  
```CP_wrapper.sh <RUN name> <number of sample to run in each round> <RUN_TYPE, this is either HR or BROCA>```

2. Run cnvkit  
```CP/cnvkit/CNV_main.sh <RUN name>```

Output files
-------------
The user will observe several intermediate files formed in RUN_results/sample_id/ (For paired samples, sample_id will be tumor_id, for unpaired samples, it will be the sample_id)   
The main files of interest will be:  
	
* RUN_results/RUN_final_output  
	* RUN_info.txt (this document contains the reference files that were specific to this run and were used, as well as all versions of packages that were used at this time of run)
	* RUN.xlsx (this is the main excel file that has overall results with all samples in run combined, tabs are QC_Metrics/QC_exons/QC_genes/indelSNP/indelSNP_BROCA/gridss/pindel/msings)
	* sample_id.xlsx (this is the results per sample in the run)
	* bqsr_<unpaired_sample_id>_ UN.bam OR bqsr_<unpaired_sample_id>_ T.bam OR bqsr_<unpaired_sample_id>_ G.bam
	* bqsr_<unpaired_sample_id>_ UN.bai OR bqsr_<unpaired_sample_id>_ T.bai OR bqsr_<unpaired_sample_id>_ G.bai
* RUN_results/CNVKIT_OUTPUT
	* RUN_combined_gene.xlsx (this is the main excel sheet that has all samples in a run combined, shows normalized CNV changes per gene in each sample)
	* RUN_combined_exon.xlsx (this is the main excel sheet that has all samples in a run combined, shows normalized CNV changes per exon per gene in each sample)
	* RUN_T_scatter.pdf (cnvkit output: a pdf that shows each tumor sample per page and a visual representation of CNV changes in that tumor sample across all chromosomes)
	* RUN_G_scatter.pdf
	* RUN_T_diagram.pdf (cnvkit output: a pdf that shows each tumor sample per page and a visual representation of deleted regions in red and gained regions in blue across the span of each chromosome)
	* RUN_G_diagram.pdf
	* <sample_id>_ cnvkit_gene_analysis.xlsx (per sample results for CNV changes per gene)
	* <sample_id>_ cnvkit_exon_analysis.xlsx (per sample results for CNV changes per exon per gene)
	* <sample_id>_ T.pdf (pipeline output: a pdf that contains one gene from the BROCA assay per page in the tumor sample. The graph displays the gene, its introns and exons at the top, and reads per intronic/exonic region as red lines)
	* <sample_id>_ G.pdf
	
Paths to modify
---------------
For the paths to modify, please refer to the paths_to_modify document.
